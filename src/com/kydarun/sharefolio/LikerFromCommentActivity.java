package com.kydarun.sharefolio;

import android.app.Fragment;

import com.kydarun.sharefolio.fragment.LikerFromCommentFragment;
import com.kydarun.sharefolio.obj.Comment;

public class LikerFromCommentActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		// TODO Auto-generated method stub
		Comment currentComment = (Comment) getIntent().getSerializableExtra(LikerFromCommentFragment.KEY_LIKER_FROM_COMMENT);
		
		return LikerFromCommentFragment.newInstance(currentComment);
	}
	
}