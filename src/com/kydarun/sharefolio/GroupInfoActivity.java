package com.kydarun.sharefolio;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.kydarun.sharefolio.adapter.GroupMemberAdapter;
import com.kydarun.sharefolio.api.GroupApi;
import com.kydarun.sharefolio.constant.PageNameConstant;
import com.kydarun.sharefolio.obj.Company;
import com.kydarun.sharefolio.obj.Group;
import com.kydarun.sharefolio.util.Common;

public class GroupInfoActivity extends BaseActivity implements OnClickListener, OnCheckedChangeListener {
	public static final int REQUEST_CODE = 1;
	
	Group group;
	TextView tvGroupName;
	ListView lvGroupMembers;
	ListView lvGroupRequest;
	ImageButton ibGroupEdit;
	GroupMemberAdapter requestAdapter;
	GroupMemberAdapter memberAdapter;
	HorizontalScrollView hsvGroupCompanyList;
	Switch swGroupSetting;
	ImageButton btnGroupAddCompany;
	ToggleGroupTypeAsyncTask task;
	Button btnGroupAddMember;
	Button btnGroupInvite;
	
	int start;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.group_chat_info);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		getFragmentManager().beginTransaction().add(R.id.rlHeader, header).commit();
		
		group = (Group)getIntent().getExtras().getSerializable("Group");
		super.setTitle(group.groupname);
		
		flProgress = (FrameLayout)findViewById(R.id.flProgress);
		
		tvGroupName = (TextView)findViewById(R.id.tvGroupName);
		if (tvGroupName != null) {
			tvGroupName.setTypeface(Common.getFontRegular(context));
			tvGroupName.setText(group.groupname);
		}
		swGroupSetting = (Switch)findViewById(R.id.swGroupSetting);
		if (group.createdby.ID.equals(sm.getCurrentSession().ID)) {
			swGroupSetting.setOnCheckedChangeListener(this);
		}
		else {
			swGroupSetting.setEnabled(false);
		}
		
		lvGroupMembers = (ListView)findViewById(R.id.lvGroupMembers);
		
		lvGroupRequest = (ListView)findViewById(R.id.lvGroupRequest);
		
		hsvGroupCompanyList = (HorizontalScrollView)findViewById(R.id.hsvGroupCompanyList);
		
		ibGroupEdit = (ImageButton)findViewById(R.id.ibGroupEdit);
		if (group.createdby.ID.equals(sm.getCurrentSession().ID)) {
			ibGroupEdit.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent i = new Intent(context, GroupEditActivity.class);
					i.putExtra(GroupEditActivity.CURRENT_GROUP, group);
					startActivityForResult(i, REQUEST_CODE);
				}
			});
		}
		else {
			ibGroupEdit.setVisibility(View.GONE);
		}
		
		new LoadGroupInfoAsyncTask().execute();
		
		btnGroupAddCompany = (ImageButton)findViewById(R.id.btnGroupAddCompany);
		if (btnGroupAddCompany != null) {
			if (group.createdby.ID.equals(sm.getCurrentSession().ID)) {
				btnGroupAddCompany.setOnClickListener(this);
			}
			else {
				btnGroupAddCompany.setVisibility(View.GONE);
			}
		}
		btnGroupAddMember = (Button)findViewById(R.id.btnGroupAddMember);
		if (btnGroupAddMember != null) {
			if (group.createdby.ID.equals(sm.getCurrentSession().ID)) {
				btnGroupAddMember.setOnClickListener(this);
			}
			else {
				btnGroupAddMember.setVisibility(View.GONE);
			}
		}
		btnGroupInvite = (Button)findViewById(R.id.btnGroupInvite);
		if (btnGroupInvite != null) {
			btnGroupInvite.setVisibility(View.GONE);	// Remove this button for the time being
		}
		
		start = 0;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		
		if (resultCode == Activity.RESULT_CANCELED) return; 
		
		if (requestCode == REQUEST_CODE) {
			Group editedGroup = (Group) data.getSerializableExtra("EDITED_GROUP");
			group = editedGroup;
			super.setTitle(group.groupname);
			tvGroupName.setText(group.groupname);
		}
	}

	@SuppressLint("InflateParams")
	class LoadGroupInfoAsyncTask extends AsyncTask<Void, Void, Void> {
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			
			if (group.requests != null) {
				if (lvGroupMembers != null) {
					if (start == 0) {
						memberAdapter = new GroupMemberAdapter(context, group.members, true, group);
						lvGroupMembers.setAdapter(memberAdapter);
					}
					else {
						memberAdapter.members.addAll(group.members);
						memberAdapter.notifyDataSetChanged();
					}
					start += 15;
				}
				if (lvGroupRequest != null) {
					if (group.createdby.ID.equals(sm.getCurrentSession().ID)) {
						requestAdapter = new GroupMemberAdapter(context, false, group, group.requests);
						lvGroupRequest.setAdapter(requestAdapter);
					}
					else {
						lvGroupRequest.setVisibility(View.GONE);
					}
				}
				if (swGroupSetting != null) {
					swGroupSetting.setSwitchTypeface(Common.getFontRegular(context));
					swGroupSetting.setChecked(group.grouptype.equals("C") ? true : false);
				}
				if (hsvGroupCompanyList != null) {
					for (final Company c: group.companies) {
						View v = LayoutInflater.from(context).inflate(R.layout.horizontal_item, null);
						Button btnLink = (Button)v.findViewById(R.id.btnLink);
						btnLink.setTypeface(Common.getFontRegular(context));
						btnLink.setTextColor(R.color.black);
						btnLink.setText(c.companyCode);
						btnLink.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
								Intent i = new Intent(context, CompanyActivity.class);
								Bundle b = new Bundle();
								b.putSerializable("company", c);
								i.putExtras(b);
								startActivity(i);
							}
						});
						((LinearLayout)hsvGroupCompanyList.getChildAt(0)).addView(v);
					}
					if (group.companies.size() == 0) {
						((LinearLayout)hsvGroupCompanyList.getChildAt(0)).addView(LayoutInflater.from(context).inflate(R.layout.empty, null));
					}
				}
 			}
			
			flProgress.setVisibility(View.GONE);
		}

		@Override
		protected Void doInBackground(Void... params) {
			GroupApi api = new GroupApi(context);
			try {
				group = api.getGroupInfo(sm.getCurrentSession().ID, group.groupid);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
	}
	
	class ToggleGroupTypeAsyncTask extends AsyncTask<Void, Void, Boolean> {
		public boolean isFinished;
		public String toggle;
		
		public ToggleGroupTypeAsyncTask(boolean toggle) {
			this.toggle = toggle ? "C" : "S";
			this.isFinished = false;
		}
		
		@Override
		protected Boolean doInBackground(Void... params) {
			GroupApi api = new GroupApi(context);
			group.grouptype = this.toggle;
			try {
				return api.editGroup(sm.getCurrentSession().ID, group);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			swGroupSetting.setEnabled(true);
			task.isFinished = true;
			
			if (!result) {
				Toast.makeText(context, "Failed to toggle group type", Toast.LENGTH_SHORT);
				swGroupSetting.toggle();
			}
		}
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btnGroupAddCompany) {
			Intent intent = new Intent(context, SearchActivity.class);
			Bundle b = new Bundle();
			b.putString("PageName", PageNameConstant.MY_COMPANY);
			b.putSerializable("Group", group);
			intent.putExtras(b);
			startActivity(intent);
		}
		else if (v.getId() == R.id.btnGroupAddMember) {
			Intent intent = new Intent(context, SearchActivity.class);
			Bundle b = new Bundle();
			b.putString("PageName", PageNameConstant.HOME_PAGE);
			b.putSerializable("Group", group);
			intent.putExtras(b);
			startActivity(intent);
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (buttonView.getId() == R.id.swGroupSetting) {
			buttonView.setEnabled(false);
			if (task == null || task.isFinished) {
				task = new ToggleGroupTypeAsyncTask(isChecked);
				task.execute();
			}
		}
	}
}
