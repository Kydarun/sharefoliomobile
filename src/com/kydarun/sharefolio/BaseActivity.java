package com.kydarun.sharefolio;

import com.kydarun.sharefolio.fragment.HeaderFragment;
import com.kydarun.sharefolio.util.SessionManager;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.TextView;
/***
 * 
 * @author Ken
 * Base class for Activity. Initializes SessionManager, HeaderFragment & Title
 * (used in the box)
 */
public abstract class BaseActivity extends Activity {
	Context context;
	SessionManager sm;
	ProgressDialog progressDialog;
	FrameLayout flProgress;
	
	protected TextView tvTitle;
	protected HeaderFragment header;
	
	public String pageName;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.context = this;
		this.sm = new SessionManager(this.context);
		
		header = new HeaderFragment(this);
	}
}
