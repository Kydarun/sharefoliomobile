package com.kydarun.sharefolio;

import java.io.IOException;

import com.amazonaws.services.sns.model.CreatePlatformEndpointResult;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.kydarun.sharefolio.api.UserApi;
import com.kydarun.sharefolio.notification.NotificationReceiverService;
import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.util.Common;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterActivity extends BaseActivity implements OnClickListener {
	EditText etEmail;
	EditText etUsername;
	EditText etPassword;
	
	GoogleCloudMessaging gcm;
	
	boolean isCreated = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register);
		
		gcm = GoogleCloudMessaging.getInstance(this);
		
		etUsername = (EditText)findViewById(R.id.etUsername);
		etEmail = (EditText)findViewById(R.id.etEmail);
		etPassword = (EditText)findViewById(R.id.etPassword);
		
		Button btnRegister = (Button)findViewById(R.id.btnRegister);
		btnRegister.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btnRegister) {
			if (etUsername.getText().toString().equals("") || etPassword.getText().toString().equals("") || etEmail.getText().toString().equals("")) {
				Toast.makeText(context, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
				return;
			}
			progressDialog = ProgressDialog.show(context, context.getResources().getString(R.string.app_name), "Signing Up...");
			new RegisterAsyncTask().execute();
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		if (isCreated) {
			finish();
		}
	}

	class RegisterAsyncTask extends AsyncTask<Void, Void, Account> {

		@Override
		protected Account doInBackground(Void... arg0) {
			UserApi api = new UserApi(context);
			Account account = new Account();
			account.firstname = etUsername.getText().toString();
			account.email = etEmail.getText().toString();
			account.password = etPassword.getText().toString();
			
			try {
				return api.register(account);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return new Account();
		}

		@Override
		protected void onPostExecute(Account result) {
			super.onPostExecute(result);
			
			if (result.ID == null) {
				Toast.makeText(context, "Register failed, please check your input and try again.", Toast.LENGTH_SHORT).show();
				return;
			}
			
			sm.login(result);
			new RegisterNotificationAsyncTask().execute();
			
			isCreated = true;
			
			Intent i = new Intent(context, PhoneNumberActivity.class);
			startActivity(i);
		}
	}
	
	class RegisterNotificationAsyncTask extends AsyncTask<Void, Void, Void> {
		CreatePlatformEndpointResult res;
		String token = null;
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			
			if (token != null & res != null && res.getEndpointArn() != null) {
				startService(new Intent(context, NotificationReceiverService.class));
				Toast.makeText(context, "Registered successfully", Toast.LENGTH_SHORT).show();
			}
			else {
				Toast.makeText(context, "Failed to register", Toast.LENGTH_SHORT).show();
			}
			
			progressDialog.dismiss();
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				token = gcm.register(context.getResources().getString(R.string.project_number));
				if (sm.getCurrentSession().platformEndpointArn.equals("")) {
					res = Common.registerPushNotification(context, gcm, token);
				}
				
				UserApi api = new UserApi(context);
				try {
					if (sm.getCurrentSession().platformEndpointArn.equals("")) {
						api.updateRegistrationID(sm.getCurrentSession().ID, token, res.getEndpointArn());
						Account a = sm.getCurrentSession();
						a.registration_id = token;
						a.platformEndpointArn = res.getEndpointArn();
						sm.setSession(a);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (NotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			return null;
		}	
	}
}
