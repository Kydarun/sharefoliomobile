package com.kydarun.sharefolio.api;

import java.util.List;

import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.obj.Request;
import com.kydarun.sharefolio.obj.User;
import com.kydarun.sharefolio.parser.RequestParser;
import com.kydarun.sharefolio.parser.UserParser;
import com.kydarun.sharefolio.util.Encryption;
import com.kydarun.sharefolio.util.SharefolioJSONObject;

import android.content.Context;
import android.text.TextUtils;

public class UserApi extends ApiBase {
	public UserApi(Context context) {
		super(context);
		this.module = "user.php";
	}

	public User getUserInfo(long accountID, long userID) throws Exception {		
		this.addAccountID(accountID);
		this.setFunction("GET_USER_INFO");
		this.addParameter("userID", Encryption.encrypt(userID));
		
		return UserParser.parseAccount(this.postJSON().getJSONObject("user"));
	}
	
	public User login(User user) throws Exception {
		this.addParameter("email", user.email);
		this.addParameter("password", user.password);
		this.setFunction("LOGIN");
		
		return UserParser.parseAccount(this.postJSON().getJSONObject("user"));
	}
	
	public User fbLogin(String fb_id, String firstname, String lastname, String email, String picture) throws Exception {
		this.setFunction("FB_LOGIN");
		this.addParameter("fb_id", fb_id);
		this.addParameter("firstname", firstname);
		this.addParameter("lastname", lastname);
		this.addParameter("email", email);
		this.addParameter("picture", picture);
		
		return UserParser.parseAccount(this.postJSON().getJSONObject("user"));
	}
	
	public Account register(Account account) throws Exception {
		this.setFunction("REGISTER");
		this.addParameter("firstname", account.firstname);
		this.addParameter("password", account.password);
		this.addParameter("email", account.email);
		
		return UserParser.parseAccount(this.postJSON().getJSONObject("user"));
	}
	
	public List<Account> getFriendsList(long accountID, int start, int numberOfResult) throws Exception {
		this.addAccountID(accountID);
		this.setFunction("GET_FRIENDS_LIST");
		this.addParameter("start", start);
		this.addParameter("numberOfResult", numberOfResult);
		
		return UserParser.parseAccountList(this.postJSON().getJSONArray("friend"));
	}
	
	public List<Account> searchPeople(long accountID, String query, int start, int numberOfResult) throws Exception {
		this.setFunction("SEARCH_PEOPLE");
		this.addAccountID(accountID);
		this.addParameter("query", query);
		this.addParameter("start", start);
		this.addParameter("numberOfResult", numberOfResult);
		
		return UserParser.parseAccountList(this.postJSON().getJSONArray("user"));
	}
	
	public List<Request> getFriendRequest(long accountID, int start, int numberOfResult) throws Exception {
		this.setFunction("GET_REQUEST_LIST");
		this.addAccountID(accountID);
		this.addParameter("start", start);
		this.addParameter("numberOfResult", numberOfResult);
		
		return RequestParser.parseRequestList(this.postJSON().getJSONArray("request"));
	}
	
	public boolean processRequest(long accountID, long requestID, boolean action) throws Exception {
		this.setFunction("PROCESS_REQUEST");
		this.addAccountID(accountID);
		this.addParameter("requestID", Encryption.encrypt(requestID));
		this.addParameter("action", action);
		
		return this.postJSON().isValid();
	}
	
	public boolean updatePhoneNumber(Account account) throws Exception {
		this.setFunction("UPDATE_PHONE_NUMBER");
		this.addAccountID(account.ID);
		this.addParameter("phoneNumber", account.phoneNumber);
		
		return this.postJSON().isValid();
	}
	
	public boolean verifyPhoneNumber(Account account) throws Exception {
		this.setFunction("VERIFY_PHONE_NUMBER");
		this.addAccountID(account.ID);
		this.addParameter("phoneNumber", account.phoneNumber);
		this.addParameter("verificationCode", account.verificationCode);
		
		return this.postJSON().isValid();
	}
	
	public Account editProfile(Account account) throws Exception {
		this.setFunction("EDIT_PROFILE");
		this.addAccountID(account.ID);
		this.addParameter("firstname", account.firstname);
		
		return UserParser.parseAccount(this.postJSON().getJSONObject("user"));
	}
	
	public boolean updateRegistrationID(long accountID, String registrationID, String platformEndpointArn) throws Exception {
		this.setFunction("UPDATE_REGISTRATION_ID");
		this.addAccountID(accountID);
		this.addParameter("registrationID", registrationID);
		this.addParameter("platformEndpointArn", platformEndpointArn);
		
		return this.postJSON().isValid();
	}
	
	public List<Account> getUserViaContact(long accountID, List<String> phoneNumber) throws Exception {
		this.setFunction("GET_USER_VIA_CONTACT");
		this.addAccountID(accountID);
		this.addParameter("phoneNumber", TextUtils.join(",", phoneNumber));
		
		return UserParser.parseAccountList(this.postJSON().getJSONArray("user"));
	}
	
	public boolean sendFriendRequest(long accountID, Account friend) throws Exception {
		this.setFunction("SEND_FRIEND_REQUEST");
		this.addAccountID(accountID);
		this.addParameter("friendID", Encryption.encrypt(friend.ID));
		
		return this.postJSON().isValid();
	}
	
	public boolean deleteFriend(long accountID, long friendID) throws Exception {
		this.setFunction("DELETE_FRIEND");
		this.addAccountID(accountID);
		this.addParameter("friendID", Encryption.encrypt(friendID));
		
		return this.postJSON().isValid();
	}
	
	public int getFriendCount(long accountID) throws Exception {
		this.setFunction("GET_FRIEND_COUNT");
		this.addAccountID(accountID);
		
		SharefolioJSONObject result = this.postJSON();
		if (result.isValid()) {
			return result.getInt("count");
		}
		else {
			return 0;
		}
	}
}
