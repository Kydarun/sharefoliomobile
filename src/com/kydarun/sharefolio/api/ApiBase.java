package com.kydarun.sharefolio.api;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;

import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.constant.EnvironmentalConstant;
import com.kydarun.sharefolio.util.Encryption;
import com.kydarun.sharefolio.util.SharefolioJSONObject;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.util.Log;

public abstract class ApiBase {
	protected Context context;
	protected List<NameValuePair> param;
	protected String urlBase;
	protected String module;
	
	public ApiBase(Context context) {
		this.context = context;
		Resources res = context.getResources();
		this.urlBase = EnvironmentalConstant.DEBUG ? 
				res.getString(R.string.url_debug) : res.getString(R.string.url);
		this.param = new ArrayList<NameValuePair>();
	}
	
	public SharefolioJSONObject getJSON() throws Exception {
		try {
			String paramList = URLEncodedUtils.format(param, "utf-8");
			String url = urlBase + module + "?" + paramList;
			
			DefaultHttpClient client = new DefaultHttpClient();
			HttpGet get = new HttpGet(Uri.parse(url).buildUpon().toString());
			
			get.addHeader("Accept", "application/json");
			
			HttpResponse response = client.execute(get);
			HttpEntity entity = response.getEntity();
			
			String json = EntityUtils.toString(entity);
			
			client.getConnectionManager().shutdown();
			
			SharefolioJSONObject result = new SharefolioJSONObject(json);
			if (!result.isValid()) throw new Exception("Invalid server response."); 
			
			return new SharefolioJSONObject(json);
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		param.clear();
		
		return new SharefolioJSONObject();
	}
	
	public SharefolioJSONObject postJSON() throws Exception {
		try {
			String url = urlBase + module;
			
			DefaultHttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(Uri.parse(url).buildUpon().toString());
			post.setEntity(new UrlEncodedFormEntity(param, HTTP.UTF_8));
			post.addHeader("Accept", "application/json");

			HttpResponse response = client.execute(post);
			HttpEntity entity = response.getEntity();

			String json = EntityUtils.toString(entity);
			Log.d(ApiBase.class.getSimpleName(), json);

			SharefolioJSONObject result = new SharefolioJSONObject(json);
			// Log.d(ApiBase.class.getSimpleName(), result.toString());
			if (!result.isValid()) throw new Exception("Invalid server response."); 
			
			return new SharefolioJSONObject(json);
		}
		catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
			e.printStackTrace();
		}
		
		param.clear();
		
		return new SharefolioJSONObject();
	}
	
	protected void addParameter(String name, String value) {
		param.add(new BasicNameValuePair(name, value));
	}
	
	protected void addParameter(String name, long value) {
		param.add(new BasicNameValuePair(name, "" + value));
	}
	
	protected void addParameter(String name, boolean value) {
		param.add(new BasicNameValuePair(name, value ? "true" : "false"));
	}
	
	protected void addAccountID(long accountID) {
		this.addParameter("accountID", Encryption.encrypt(accountID));
	}
	
	protected void setFunction(String func) {
		this.addParameter("func", func);
	}
	
	public SharefolioJSONObject uploadJSON() {
		// Skeleton
		
		param.clear();
		
		return new SharefolioJSONObject();
	}
}
