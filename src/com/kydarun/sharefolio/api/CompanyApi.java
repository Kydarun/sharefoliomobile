package com.kydarun.sharefolio.api;

import java.util.List;

import com.kydarun.sharefolio.obj.Company;
import com.kydarun.sharefolio.obj.History;
import com.kydarun.sharefolio.parser.HistoryParser;
import com.kydarun.sharefolio.parser.UserParser;
import com.kydarun.sharefolio.util.Encryption;

import android.content.Context;

public class CompanyApi extends ApiBase {

	public CompanyApi(Context context) {
		super(context);
		this.module = "company.php";
	}
	
	public List<Company> searchCompany(long accountID, String query, int start, int numberOfResult) throws Exception {
		this.setFunction("SEARCH_COMPANY");
		this.addAccountID(accountID);
		this.addParameter("query", query);
		this.addParameter("start", start);
		this.addParameter("numberOfResult", numberOfResult);
		
		return UserParser.parseCompanyList(this.postJSON().getJSONArray("company"));
	}
	
	public List<History> getPriceChart(long accountID, String id) throws Exception {
		this.setFunction("GET_PRICE_CHART");
		this.addAccountID(accountID);
		this.addParameter("id", id);
		
		return HistoryParser.parseHistoryList(this.postJSON().getJSONArray("price"));
	}
	
	public List<Company> getMyCompanyList(long accountID) throws Exception {
		this.setFunction("GET_MY_COMPANY_LIST");
		this.addAccountID(accountID);
		
		return UserParser.parseCompanyList(this.postJSON().getJSONArray("company"));
	}
	
	public List<Company> getCompanySuggestion(long accountID) throws Exception {
		this.setFunction("SUGGEST_COMPANIES");
		this.addAccountID(accountID);
		
		return UserParser.parseWallpostCompanyList(this.postJSON().getJSONArray("company"));
	}
	
	public Company getCompany(long accountID, String id, int start, int numberOfResult) throws Exception {
		this.setFunction("GET_COMPANY");
		this.addAccountID(accountID);
		this.addParameter("companyID", id);
		this.addParameter("start", start);
		this.addParameter("numberOfResult", numberOfResult);
		
		return UserParser.parseCompany(this.postJSON().getJSONObject("company"));
	}
	
	public boolean followCompany(long accountID, String companyID) throws Exception {
		this.setFunction("FOLLOW_COMPANY");
		this.addAccountID(accountID);
		this.addParameter("companyID", companyID);
		
		return this.postJSON().isValid();
	}
	
	public boolean unfollowCompany(long accountID, String companyID) throws Exception {
		this.setFunction("UNFOLLOW_COMPANY");
		this.addAccountID(accountID);
		this.addParameter("companyID", companyID);
		
		return this.postJSON().isValid();
	}
	
	public Company getCompanyWidget(long accountID, String companyID) throws Exception {
		this.setFunction("GET_COMPANY_WIDGET");
		this.addAccountID(accountID);
		this.addParameter("id", companyID);
		
		return UserParser.parseCompany(this.postJSON().getJSONObject("company"));
	}
	
	public boolean isUserFollowing(long accountID, long userID, String companyID) throws Exception {
		this.setFunction("IS_USER_FOLLOWING");
		this.addAccountID(accountID);
		this.addParameter("userID", Encryption.encrypt(userID));
		this.addParameter("companyID", companyID);
		
		return this.postJSON().getBoolean("following");
	}
}
