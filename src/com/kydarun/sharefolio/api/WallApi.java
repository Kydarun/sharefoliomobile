package com.kydarun.sharefolio.api;

import java.util.List;

import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.obj.Comment;
import com.kydarun.sharefolio.obj.Wall;
import com.kydarun.sharefolio.parser.CommentParser;
import com.kydarun.sharefolio.parser.WallParser;
import com.kydarun.sharefolio.util.Encryption;

import android.content.Context;
import android.util.Log;

public class WallApi extends ApiBase {

	public WallApi(Context context) {
		super(context);
		this.module = "wall.php";
	}
	
	public List<Wall> loadHome(long accountID, int start, int numberOfResult) throws Exception {
		this.setFunction("LOAD_HOME");
		this.addAccountID(accountID);
		this.addParameter("start", start);
		this.addParameter("numberOfResult", numberOfResult);
		
		return WallParser.parseWallList(this.postJSON().getJSONArray("wallpost"));
	}
	
	public List<Comment> loadComments(long accountID, long wallID, int start, int numberOfResult) throws Exception {
		this.setFunction("LOAD_COMMENTS");
		this.addAccountID(accountID);
		this.addParameter("wallID", Encryption.encrypt(wallID));
		this.addParameter("start", start);
		this.addParameter("numberOfResult", numberOfResult);
		
		return CommentParser.parseCommentList(this.postJSON().getJSONArray("comment"));
	}
	
	public Wall loadSinglePost(long accountID, long wallID) throws Exception {
		this.setFunction("LOAD_SINGLE_POST");
		this.addAccountID(accountID);
		this.addParameter("wallID", Encryption.encrypt(wallID));
		
		return WallParser.parseWall(this.postJSON().getJSONArray("wallpost").getJSONObject(0));
	}
	
	public Wall likePost(long accountID, long wallID) throws Exception {
		this.setFunction("USER_LIKE_POST");
		this.addAccountID(accountID);
		this.addParameter("wallID", Encryption.encrypt(wallID));
		
		return WallParser.parseWall(this.postJSON().getJSONArray("wallpost").getJSONObject(0));
	}
	
	public Wall unlikePost(long accountID, long wallID) throws Exception {
		this.setFunction("USER_UNLIKE_POST");
		this.addAccountID(accountID);
		this.addParameter("wallID", Encryption.encrypt(wallID));
		
		return WallParser.parseWall(this.postJSON().getJSONArray("wallpost").getJSONObject(0));
	}
	
	/* To be implemented using HTTP Uploads in later versions */
	public List<Comment> submitComment(Comment comment) throws Exception {
		this.setFunction("USER_SUBMIT_COMMENT");
		this.addAccountID(((Account)comment.user).ID);
		this.addParameter("wallID", Encryption.encrypt(comment.wallID));
		this.addParameter("userComment", comment.message);
		if (comment.companyTagged != null && comment.companyTagged.size() != 0) {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < comment.companyTagged.size(); i++) {
				sb.append(comment.companyTagged.get(i).id);
				if (i != comment.companyTagged.size() - 1) {
					sb.append(",");
				}
			}
			this.addParameter("companyTagged", sb.toString());
			Log.d("WallApi", sb.toString());
		}
		if (comment.personTagged != null && comment.personTagged.size() != 0) {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < comment.personTagged.size(); i++) {
				sb.append(comment.personTagged.get(i).ID);
				if (i != comment.personTagged.size() - 1) {
					sb.append(",");
				}
			}
			this.addParameter("personTagged", sb.toString());
			Log.d("WallApi", sb.toString());
		}
		Log.d("WallApi", "for testing");
		
		return CommentParser.parseCommentList(this.postJSON().getJSONArray("comment"));
	}
	
	/* To be implemented using HTTP Uploads in later versions */
	public Wall postOnWall(Wall wall) throws Exception {
		this.setFunction("POST_ON_WALL");
		this.addAccountID(((Account)wall.poster).ID);
		this.addParameter("message", wall.wallContent);
		if ((Account)wall.target != null) {
			this.addParameter("friendID", Encryption.encrypt(((Account)wall.target).ID));
		}
		if (wall.targetCompany != null) {
			this.addParameter("companyID", wall.targetCompany.id);
		}
		this.addParameter("linkTitle", wall.linkTitle);
		this.addParameter("linkUrl", wall.linkUrl);
		this.addParameter("linkDescription", wall.linkDescription);
		this.addParameter("linkVideo", wall.linkVideo);
		this.addParameter("linkImage", wall.linkImage);
		if (wall.companyTagged != null) {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < wall.companyTagged.size(); i++) {
				sb.append(wall.companyTagged.get(i).id);
				if (i != wall.companyTagged.size() - 1) {
					sb.append(",");
				}
			}
			this.addParameter("companyTagged", sb.toString());
		}
		if (wall.personTagged != null) {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < wall.personTagged.size(); i++) {
				sb.append(wall.personTagged.get(i).ID);
				if (i != wall.personTagged.size() - 1) {
					sb.append(",");
				}
			}
			this.addParameter("personTagged", sb.toString());
		}
		
		return WallParser.parseWall(this.postJSON().getJSONArray("wallpost").getJSONObject(0));
	}
	
	public Comment likeComment(long accountID, long commentID) throws Exception {
		this.setFunction("LIKE_COMMENT");
		this.addAccountID(accountID);
		this.addParameter("commentID", Encryption.encrypt(commentID));
		
		return CommentParser.parseComment(this.postJSON().getJSONArray("comment").getJSONObject(0));
	}
	
	public Comment unlikeComment(long accountID, long commentID) throws Exception {
		this.setFunction("UNLIKE_COMMENT");
		this.addAccountID(accountID);
		this.addParameter("commentID", Encryption.encrypt(commentID));
		
		return CommentParser.parseComment(this.postJSON().getJSONArray("comment").getJSONObject(0));
	}
	
	public List<Wall> loadUserWall(long accountID, long userID, int start, int numberOfResult) throws Exception {
		this.setFunction("LOAD_USER_WALL");
		this.addAccountID(accountID);
		this.addParameter("userID", Encryption.encrypt(userID));
		this.addParameter("start", start);
		this.addParameter("numberOfResult", numberOfResult);
		
		return WallParser.parseWallList(this.postJSON().getJSONArray("wallpost"));
	}
}
