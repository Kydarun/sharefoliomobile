package com.kydarun.sharefolio.api;

import java.util.List;

import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.obj.Group;
import com.kydarun.sharefolio.obj.GroupMessage;
import com.kydarun.sharefolio.obj.GroupRequest;
import com.kydarun.sharefolio.parser.GroupMessageParser;
import com.kydarun.sharefolio.parser.GroupParser;
import com.kydarun.sharefolio.parser.GroupRequestParser;
import com.kydarun.sharefolio.parser.UserParser;
import com.kydarun.sharefolio.util.Encryption;

import android.content.Context;

public class GroupApi extends ApiBase {

	public GroupApi(Context context) {
		super(context);
		this.module = "group.php";
	}
	
	public Group createGroup(long accountID, Group group) throws Exception {
		this.setFunction("CREATE_GROUP");
		this.addAccountID(accountID);
		this.addParameter("groupname", group.groupname);
		this.addParameter("groupType", group.grouptype);
		
		return GroupParser.parseGroup(this.postJSON().getJSONObject("group"));
	}
	
	public Group addMember(long accountID, long groupID, long newMemberID) throws Exception {
		this.setFunction("ADD_MEMBER");
		this.addAccountID(accountID);
		this.addParameter("groupID", Encryption.encrypt(groupID));
		this.addParameter("members", Encryption.encrypt(newMemberID));
		
		return GroupParser.parseGroup(this.postJSON().getJSONObject("group"));
	}
	
	public boolean requestJoinGroup(long accountID, long groupid) throws Exception {
		this.setFunction("REQUEST_JOIN_GROUP");
		this.addAccountID(accountID);
		this.addParameter("groupid", Encryption.encrypt(groupid));
		
		return this.postJSON().isValid();
	}
	
	public List<GroupRequest> getRequestList(long accountID, long groupid) throws Exception {
		this.setFunction("GET_REQUEST_LIST");
		this.addAccountID(accountID);
		this.addParameter("groupid", Encryption.encrypt(groupid));
		
		return GroupRequestParser.parseGroupRequestList(this.postJSON().getJSONArray("grouprequest"));
	}
	
	public boolean processRequest(long accountID, long groupid, long from, String status) throws Exception {
		this.setFunction("PROCESS_REQUEST");
		this.addAccountID(accountID);
		this.addParameter("groupid", Encryption.encrypt(groupid));
		this.addParameter("from", Encryption.encrypt(from));
		this.addParameter("status", status);
		
		return this.postJSON().isValid();
	}
	
	public List<Account> getMemberList(long accountID, long groupid) throws Exception {
		this.setFunction("GET_MEMBER_LIST");
		this.addAccountID(accountID);
		this.addParameter("groupid", Encryption.encrypt(groupid));

		return UserParser.parseAccountList(this.postJSON().getJSONArray("member"));
	}
	
	public List<Group> searchGroup(long accountID, String query, int start, int numberOfResult) throws Exception {
		this.setFunction("SEARCH_GROUP");
		this.addAccountID(accountID);
		this.addParameter("query", query);
		this.addParameter("start", start);
		this.addParameter("numberOfResult", numberOfResult);
		
		return GroupParser.parseGroupList(this.postJSON().getJSONArray("group"));
	}
	
	public List<Group> getMyGroup(long accountID) throws Exception {
		this.setFunction("GET_MY_GROUP");
		this.addAccountID(accountID);
		
		return GroupParser.parseGroupList(this.postJSON().getJSONArray("group"));
	}
	
	public List<GroupMessage> getGroupChat(long accountID, long groupid, int start, int numberOfResult) throws Exception {
		this.setFunction("GET_GROUP_CHAT");
		this.addAccountID(accountID);
		this.addParameter("groupID", Encryption.encrypt(groupid));
		this.addParameter("start", start);
		this.addParameter("numberOfResult", numberOfResult);
		
		return GroupMessageParser.parseGroupMessageList(this.postJSON().getJSONArray("message"));
	}
	
	public GroupMessage sendMessage(long accountID, long groupid, String message, String uuid) throws Exception {
		this.setFunction("SEND_MESSAGE");
		this.addAccountID(accountID);
		this.addParameter("groupID", Encryption.encrypt(groupid));
		this.addParameter("message", message);
		this.addParameter("uuid", uuid);
		
		return GroupMessageParser.parseGroupMessage(this.postJSON().getJSONArray("message").getJSONObject(0));
	}
	
	public boolean editGroup(long accountID, Group group) throws Exception {
		this.setFunction("EDIT_GROUP");
		this.addAccountID(accountID);
		this.addParameter("groupID", Encryption.encrypt(group.groupid));
		this.addParameter("groupname", group.groupname);
		this.addParameter("groupType", group.grouptype);
		
		return this.postJSON().isValid();
	}
	
	public Group getGroupInfo(long accountID, long groupID) throws Exception {
		this.setFunction("GET_GROUP_INFO");
		this.addAccountID(accountID);
		this.addParameter("groupID", Encryption.encrypt(groupID));
		
		return GroupParser.parseGroup(this.postJSON().getJSONObject("group"));
	}
	
	public boolean removeMember(long accountID, long groupID, long memberID) throws Exception {
		this.setFunction("REMOVE_MEMBER");
		this.addAccountID(accountID);
		this.addParameter("groupid", Encryption.encrypt(groupID));
		this.addParameter("memberID", Encryption.encrypt(memberID));
		
		return this.postJSON().isValid();
	}
	
	public boolean followCompany(long accountID, long groupID, String companyID) throws Exception {
		this.setFunction("FOLLOW_COMPANY");
		this.addAccountID(accountID);
		this.addParameter("groupid", Encryption.encrypt(groupID));
		this.addParameter("companyID", companyID);
		
		return this.postJSON().isValid();
	}
	
	public boolean unfollowCompany(long accountID, long groupID, String companyID) throws Exception {
		this.setFunction("UNFOLLOW_COMPANY");
		this.addAccountID(accountID);
		this.addParameter("groupid", Encryption.encrypt(groupID));
		this.addParameter("companyID", companyID);
		
		return this.postJSON().isValid();
	}
}
