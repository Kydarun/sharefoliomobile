package com.kydarun.sharefolio;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.costum.android.widget.PullAndLoadListView;
import com.costum.android.widget.PullAndLoadListView.OnLoadMoreListener;
import com.costum.android.widget.PullToRefreshListView.OnRefreshListener;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.kydarun.sharefolio.adapter.WallAdapter;
import com.kydarun.sharefolio.api.CompanyApi;
import com.kydarun.sharefolio.chart.ChartOptions;
import com.kydarun.sharefolio.chart.ChartView;
import com.kydarun.sharefolio.chart.GenericChart;
import com.kydarun.sharefolio.obj.Company;
import com.kydarun.sharefolio.obj.History;
import com.kydarun.sharefolio.database.DBHelper;
import com.kydarun.sharefolio.util.Common;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Photo;
import com.sromku.simple.fb.listeners.OnPublishListener;


@SuppressLint("ClickableViewAccessibility")
public class CompanyActivity extends BaseActivity implements OnRefreshListener, OnLoadMoreListener, OnClickListener {
	// ChartView cvChart;
	ImageView ivChart;
	TextView tvCompanyName;
	TextView tvCompanyPrice;
	TextView tvCompanyChange;
	ImageView ivCompanyLogo;
	TextView tvCompanyAbout;
	RelativeLayout rlForScreenShot;
	LinearLayout cvChart2;
	ImageButton bShare;
	Button btnFollowToggle;
	PullAndLoadListView lvCompanyWall;
	WallAdapter adapter;
	SimpleFacebook mSimpleFacebook;
	Company company;
	Bitmap screenShot;
	FrameLayout flCompanyGuide;
	boolean isFollowing;
	
	int start;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.company);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		// getFragmentManager().beginTransaction().add(R.id.rlHeader, header).commit();
		
		Bundle b = this.getIntent().getExtras();
		company = (Company)b.getSerializable("company");
		
		super.setTitle(company.companyCode);
		
		// flProgress = (FrameLayout)findViewById(R.id.flProgress);
		
		start = 0;
		
		View header = getLayoutInflater().inflate(R.layout.company_header, null, false);
		rlForScreenShot = (RelativeLayout)findViewById(R.id.rlCompanyLayout);
		
		// cvChart = (ChartView) header.findViewById(R.id.cvChart);
		ivChart = (ImageView) header.findViewById(R.id.ivChart);
		
		/* android.view.ViewGroup.LayoutParams params = cvChart.getLayoutParams();
		params.height = pixel();
		cvChart.setLayoutParams(params);
		
		if(cvChart != null){
			ChartOptions options;
			String[] columnNames = new String[] { company.companyName };
			cvChart.addGenericChart(columnNames, GenericChart.CHART_TYPE_SPARK_LINE, "chart_div", GenericChart.TITLE_TYPE_EMPTY);
			cvChart.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					
					Bundle b = getIntent().getExtras();
					Intent i = new Intent(context, ChartFullScreenActivity.class);
					Bundle bundle = new Bundle();
					bundle.putSerializable("Company", b.getSerializable("Company"));
					i.putExtras(b);
					startActivity(i);
					
				return (event.getAction() == MotionEvent.ACTION_MOVE);
				}
			});
			
			options = cvChart.getOptions("chart_div");
			options.setTitle("Sparkline");
			options.addColor("#ffffff");
			options.setAreaOpacity(0.0f);
			options.setBackgroundColor("#000000");
			options.setEnableInteractivity(false);	// Enable in future
		} */
		UrlImageViewHelper.setUrlDrawable(ivChart, "http://chart.bursamalaysia.com/cache/charts.pl?type=plc_profile&id=" + company.id + ".MY&duration=365d");
		ivChart.setOnClickListener(this);
		
		tvCompanyName = (TextView) header.findViewById(R.id.tvCompanyName);
		if (tvCompanyName != null) {
			tvCompanyName.setTypeface(Common.getFontLight(context));
			tvCompanyName.setTextColor(context.getResources().getColor(R.color.black));
		}
		tvCompanyPrice = (TextView) header.findViewById(R.id.tvCompanyPrice);
		if (tvCompanyPrice != null) {
			tvCompanyPrice.setTypeface(Common.getFontLight(context));
			tvCompanyPrice.setTextColor(context.getResources().getColor(R.color.black));
		}
		tvCompanyChange = (TextView) header.findViewById(R.id.tvCompanyChange);
		if (tvCompanyChange != null) {
			tvCompanyChange.setTypeface(Common.getFontLight(context));
			tvCompanyChange.setTextColor(context.getResources().getColor(R.color.black));
		}
		
		bShare = (ImageButton) findViewById(R.id.bShare);
		bShare.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/* new AlertDialog.Builder(context)
					.setTitle(context.getResources().getString(R.string.app_name))
					.setMessage("Share to Facebook?")
					.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							progressDialog = ProgressDialog.show(context, context.getResources().getString(R.string.app_name), "Posting to Facebook...");
							// TODO Auto-generated method stub
							rlForScreenShot.setDrawingCacheEnabled(false);
							rlForScreenShot.setDrawingCacheEnabled(true);
							
							screenShot = rlForScreenShot.getDrawingCache();
							
							OnPublishListener onPublishListener = new OnPublishListener() {

								@Override
								public void onComplete(String response) {
									// TODO Auto-generated method stub
									Log.d("OnComplete", "Published successfully. The new post id = " + response);
									progressDialog.dismiss();
								}

								@Override
								public void onException(Throwable throwable) {
									// TODO Auto-generated method stub
									super.onException(throwable);
								}

								@Override
								public void onFail(String reason) {
									// TODO Auto-generated method stub
									super.onFail(reason);
									Log.d("onFail", "Failed because " + reason);
								}

								@Override
								public void onThinking() {
									// TODO Auto-generated method stub
									super.onThinking();
								}
								
							};
							
							String companyAvatarLink = "http://sharefolio.net/avatar/company/" 
									+ company.id
									+ company.avatar;
							
							String companyLink = "http://sharefolio.net/company.php?id=" + company.id;
							
							Log.d("AvatarLink", companyAvatarLink);
							Log.d("link", companyLink);
							
							Feed feed = new Feed.Builder()
						    .setName("ShareFolio.net")
						    .setCaption(company.companyName + "." + company.id)
						    .setDescription("Company history to be shown here")
						    .setPicture(companyAvatarLink)
						    .setLink(companyLink)
						    .build();
							
							
							Photo photo = new Photo.Builder()
						    .setImage(screenShot)
						    .setName("I'm following " + company.companyCode + " in Sharefolio. Check it out!")
						    .build();
							
							
							//mSimpleFacebook.publish(feed, true, onPublishListener);
							mSimpleFacebook.publish(photo, onPublishListener);
						}
					})
					.setNegativeButton("No", null)
					.show(); */
				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_SEND);
				rlForScreenShot.setDrawingCacheEnabled(false);
				rlForScreenShot.setDrawingCacheEnabled(true);
				
				screenShot = rlForScreenShot.getDrawingCache();
				intent.putExtra(Intent.EXTRA_STREAM, Common.generateImageFromBitmap(context, screenShot));
				intent.setType("image/jpeg");
				startActivity(Intent.createChooser(intent, "Share this screenshot to"));
			}
		});
		
		//rlForScreenShot.addView(cvChart, params);
		
		lvCompanyWall = (PullAndLoadListView)findViewById(R.id.lvCompanyWall);
		lvCompanyWall.addHeaderView(header);
		
		if (lvCompanyWall != null) {
			lvCompanyWall.setOnRefreshListener(this);
			lvCompanyWall.setOnLoadMoreListener(this);
		}
		
		btnFollowToggle = (Button)findViewById(R.id.btnFollowToggle);
		if (btnFollowToggle != null) {
			btnFollowToggle.setOnClickListener(this);
		}
		
		new LoadCompanyAsyncTask().execute();
		
		flCompanyGuide = (FrameLayout)findViewById(R.id.flCompanyGuide);
		if (Common.getSetting(context, "first_time_login", true)) {
			Common.setSetting(context, "first_time_login", false);
			flCompanyGuide.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (v.getId() == R.id.flCompanyGuide) {
						v.setVisibility(View.GONE);
					}
				}
			});
		}
		else {
			flCompanyGuide.setVisibility(View.GONE);
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public int pixel() {
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		
		return (int) (dm.heightPixels * 0.2);
	}
	@Override
	protected void onResume() {
		super.onResume();
		mSimpleFacebook = SimpleFacebook.getInstance(this);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		mSimpleFacebook.onActivityResult(this, requestCode, resultCode, data);
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	public void onLoadMore() {
		start += 15;
		new CompanyWallpostAsyncTask().execute();
	}

	@Override
	public void onRefresh() {
		start = 0;
		new CompanyWallpostAsyncTask().execute();
	}

	class LoadCompanyAsyncTask extends AsyncTask<Void, Void, Void> {
		//History history;
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			
			/* for (History history: company.history) {
				cvChart.addDatum("chart_div", company.companyName, Float.parseFloat(history.value));		
			}

			cvChart.drawCharts(); */
					 
			tvCompanyName.setText(company.companyCode);
			tvCompanyPrice.setText(company.price.last);
			String priceChange = company.price.stockChange.equals("0.00") ? "(0.00%)" : 
				Common.percentageChange(Double.parseDouble(company.price.ref), Double.parseDouble(company.price.last));
			tvCompanyChange.setText((company.price.stockChange.equals("0.00") ? "0.000" : company.price.stockChange) + " " + priceChange);
				Common.percentageChange(Double.parseDouble(company.price.ref), Double.parseDouble(company.price.last));
			if (priceChange.contains("+")) {
				tvCompanyPrice.setTextColor(context.getResources().getColor(R.color.green));
				tvCompanyChange.setTextColor(context.getResources().getColor(R.color.green));
			}
			else if (priceChange.contains("-")) {
				tvCompanyPrice.setTextColor(context.getResources().getColor(R.color.red));
				tvCompanyChange.setTextColor(context.getResources().getColor(R.color.red));
			}
			adapter = new WallAdapter(context, company.wallpost);
			lvCompanyWall.setAdapter(adapter);

			if (isFollowing) {
				btnFollowToggle.setText("Following");
			}
			else {
				btnFollowToggle.setText("Follow");
			}
			
			// flProgress.setVisibility(View.GONE);
		
		}

		@Override
		protected Void doInBackground(Void... params) {
			CompanyApi api = new CompanyApi(context);
			try {
				company = api.getCompany(sm.getCurrentSession().ID, company.id, start, 15);
				// company.history = api.getPriceChart(sm.getCurrentSession().ID, company.id);
				
				isFollowing = api.isUserFollowing(sm.getCurrentSession().ID, sm.getCurrentSession().ID, company.id);
			} catch (Exception e) {
				e.printStackTrace();
				company.history = new ArrayList<History>();
			}
			
			return null;
		}
	}
	
	class CompanyWallpostAsyncTask extends AsyncTask<Void, Void, Void> {
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			
			adapter.walls = company.wallpost;
			adapter.notifyDataSetChanged();
			
			if (start == 0) {
				lvCompanyWall.onRefreshComplete();
			}
			else {
				lvCompanyWall.onLoadMoreComplete();
			}
		}

		@Override
		protected Void doInBackground(Void... params) {
			CompanyApi api = new CompanyApi(context);
			try {
				Company co = api.getCompany(sm.getCurrentSession().ID, company.id, start, 15);
				if (start == 0) {
					company.wallpost = co.wallpost;
				}
				else {
					company.wallpost.addAll(co.wallpost);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return null;
		}
		
	}
	
	class FollowCompanyAsyncTask extends AsyncTask<Void, Void, Boolean> {
		
		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			
			if (result) {
				isFollowing = !isFollowing;
				
				if (isFollowing) {
					btnFollowToggle.setText("Following");
				}
				else {
					btnFollowToggle.setText("Follow");
				}
			}
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			CompanyApi api = new CompanyApi(context);
			
			try {
				if (isFollowing) {
					return api.unfollowCompany(sm.getCurrentSession().ID, company.id);
				}
				else {
					return api.followCompany(sm.getCurrentSession().ID, company.id);
				}
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
			
			return false;
		}
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btnFollowToggle) {
			if (isFollowing) {
				new AlertDialog.Builder(context)
					.setTitle(context.getResources().getString(R.string.app_name))
					.setMessage("Unfollow " + company.companyCode + "?")
					.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							new FollowCompanyAsyncTask().execute();
							DBHelper.get(context).deleteCompany(company);
						}
					})
					.setNegativeButton("No", null)
					.show();
			}
			else {
				new FollowCompanyAsyncTask().execute();
				DBHelper.get(context).addCompany(company);
			}
		}
		else if (v.getId() == R.id.ivChart) {
			Intent intent = new Intent(context, FullScreenPictureActivity.class);
			Bundle b = new Bundle();
			b.putString("url", "http://chart.bursamalaysia.com/cache/charts.pl?type=plc_profile&id=" + company.id + ".MY&duration=365d");
			intent.putExtras(b);
			startActivity(intent);
		}
	}
}
