package com.kydarun.sharefolio.util;

import android.content.Context;
import com.kydarun.sharefolio.obj.Account;

public class SessionManager {
	private Context context;
	
	public SessionManager(Context context) {
		this.context = context;
	}
	
	/* Session-related functions */
	public Account getCurrentSession() {
		return isSessionExists() ? readSession() : null;
	}
	
	public boolean login(Account account) {
		if (isSessionExists()) {
			return false;
		}
		else {
			setSession(account);
			return true;
		}
	}
	
	public boolean logout() {
		if (isSessionExists()) {
			initializeSession();
			
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean isSessionExists() {
		return Common.getSetting(context, "ID", -1L) != -1L;
	}
	
	private Account readSession() {
		Account account = new Account();
		account.ID = Common.getSetting(context, "ID", -1L);
		account.firstname = Common.getSetting(context, "firstname");
		account.avatar = Common.getSetting(context, "avatar");
		account.fb_id = Common.getSetting(context, "fb_id");
		account.phoneNumber = Common.getSetting(context, "phoneNumber");
		account.registration_id = Common.getSetting(context, "registration_id");
		account.platformEndpointArn = Common.getSetting(context, "platformEndpointArn");
		
		return account;
	}
	
	public void setSession(Account account) {
		Common.setSetting(context, "ID", account.ID);
		Common.setSetting(context, "firstname", account.firstname);
		Common.setSetting(context, "avatar", account.avatar == null ? "" : account.avatar);
		Common.setSetting(context, "fb_id", account.fb_id == null ? "" : account.fb_id);
		Common.setSetting(context, "phoneNumber", account.phoneNumber == null ? "" : account.phoneNumber);
		Common.setSetting(context, "registration_id", account.registration_id == null ? "" : account.registration_id);
		Common.setSetting(context, "platformEndpointArn", account.platformEndpointArn == null ? "" : account.platformEndpointArn);
	}
	
	private void initializeSession() {
		Common.setSetting(context, "ID", -1L);
		Common.setSetting(context, "firstname", "");
		Common.setSetting(context, "avatar", "");
		Common.setSetting(context, "fb_id", "");
		Common.setSetting(context, "phoneNumber", "");
		Common.setSetting(context, "registration_id", "");
		Common.setSetting(context, "platformEndpointArn", "");
	}
	
	/* PageName related functions */
	public void setCurrentPage(String pageName) {
		Common.setSetting(context, "PageName", pageName);
	}
	
	public String getCurrentPage() {
		return Common.getSetting(context, "PageName", "");
	}
}
