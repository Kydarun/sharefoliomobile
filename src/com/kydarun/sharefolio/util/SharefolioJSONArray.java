package com.kydarun.sharefolio.util;

import org.json.JSONArray;
import org.json.JSONException;

public class SharefolioJSONArray extends JSONArray {
	
	public SharefolioJSONArray(String source) throws JSONException {
		super(source);
	}
	
	public SharefolioJSONArray() {
		super();
	}
	
	@Override
	public SharefolioJSONArray getJSONArray(int index) {
		try {
			return super.isNull(index) ? null : new SharefolioJSONArray(super.getJSONArray(index).toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public SharefolioJSONObject getJSONObject(int index) {
		try {
			return super.isNull(index) ? null : new SharefolioJSONObject(super.getJSONObject(index).toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
