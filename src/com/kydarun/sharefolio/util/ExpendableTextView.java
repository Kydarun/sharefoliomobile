package com.kydarun.sharefolio.util;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.kydarun.sharefolio.R;
/**
 * User: Bazlur Rahman Rokon
 * Date: 9/7/13 - 3:33 AM
 */
public class ExpendableTextView extends TextView {
    private static final int DEFAULT_TRIM_LENGTH = 100;
    private static final String ELLIPSIS = "  ..Continue";

    private CharSequence originalText;
    private CharSequence trimmedText;
    private BufferType bufferType;
    private boolean trim = true;
    private int trimLength;

    public ExpendableTextView(Context context) {
        this(context, null);
    }

    public ExpendableTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ExpendableTextView);
        this.trimLength = typedArray.getInt(R.styleable.ExpendableTextView_trimLength, DEFAULT_TRIM_LENGTH);
        typedArray.recycle();

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                trim = !trim;
                setText();
                
                requestFocus();
               
            }
        });
    }

    private void setText() {
        super.setText(getDisplayableText(), bufferType);
       
    }

    private CharSequence getDisplayableText() {
        return trim ? trimmedText : originalText;
        
        
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        originalText = text;
        trimmedText = getTrimmedText(text);
        bufferType = type;
        setText();
        
    }

    private CharSequence getTrimmedText(CharSequence text) {
        if (originalText != null && originalText.length() > trimLength) {
        	final Spannable elipse = new SpannableString(ELLIPSIS);
        	
        	elipse.setSpan(new RelativeSizeSpan(0.8f), 0, elipse.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        	elipse.setSpan(new ForegroundColorSpan(Color.rgb(51, 102, 255)), 0, elipse.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        	elipse.setSpan(new StyleSpan(Typeface.BOLD), 0, elipse.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            return new SpannableStringBuilder(originalText, 0, trimLength + 1).append(elipse);
            
          
        } else {
            return originalText;
        }
    }

    public CharSequence getOriginalText() {
        return originalText;
       
    }

    public void setTrimLength(int trimLength) {
        this.trimLength = trimLength;
        trimmedText = getTrimmedText(originalText);
        setText();
    }

    public int getTrimLength() {
        return trimLength;
    }
}