package com.kydarun.sharefolio.util;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;

import com.kydarun.sharefolio.LoadUserWallActivity;
import com.kydarun.sharefolio.CompanyActivity;
import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.obj.Company;

public class SharefolioSpannableHandler {

	public static SpannableStringBuilder getSpannableStringBuilder(final Context context, String text, List<Account> persons, List<Company> companies) {
		SpannableStringBuilder ssb = new SpannableStringBuilder(text);

		if (persons != null && persons.size() != 0) {
			for (int i = 0; i < persons.size(); i++) {
				final Account person = persons.get(i);
				final String personName = persons.get(i).firstname;
	
				if (text.contains(personName)) {
					int start = text.indexOf(personName) - 1;
					int end = start + personName.length() + 1;
					ssb.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					ssb.setSpan(new ForegroundColorSpan(Color.BLUE), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					ssb.setSpan(new ClickableSpan() {
	
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							 Intent intent = new Intent(context, LoadUserWallActivity.class);
							 intent.putExtra(LoadUserWallActivity.TAG, person);
							 context.startActivity(intent);
						}
					}, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
				}
			}
		} 
		
		if (companies != null && companies.size() != 0) {
			for (int i = 0 ; i < companies.size() ; i++) {
				final Company company = companies.get(i);
				final String companyCode = companies.get(i).companyCode;
				
				if (text.contains(companyCode)) {
					
					int start = text.indexOf(companyCode) - 1;
					int end = start + companyCode.length() + 1;
					ssb.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					ssb.setSpan(new ForegroundColorSpan(Color.BLUE), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					ssb.setSpan(new ClickableSpan() {
	
						@Override
						public void onClick(View v) {
							Intent intent = new Intent(context, CompanyActivity.class);
							Bundle b = new Bundle();
							b.putSerializable("company", company);
							intent.putExtras(b);
							context.startActivity(intent);
						}
					}, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
				}
			}
		}
		return ssb;
	}

}
