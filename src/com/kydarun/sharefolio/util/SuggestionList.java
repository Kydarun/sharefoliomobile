package com.kydarun.sharefolio.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Pattern;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListPopupWindow;

import com.kydarun.sharefolio.adapter.CompaniesDropDownAdapter;
import com.kydarun.sharefolio.adapter.FriendsDropDownAdapter;
import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.obj.Company;

public class SuggestionList {
	static boolean isCompany;
	static int anchor;
	static String subString;
	static List<Account> list;
	static List<Company> list2;
	static EditText postCommentText;
	static ListPopupWindow popupWindow;
	static HashSet<Company> hsCompanyTagged = new HashSet<Company>();
	static HashSet<Account> hsPersonTagged = new HashSet<Account>();
	static List<Account> initialFriendsTag = new ArrayList<Account>();
	static List<Company> initialCompaniesTag = new ArrayList<Company>();

	public static void loadSuggestionList(Context c, ListPopupWindow popupWindow1, CharSequence s,
			List<Account> friends, List<Company> companies, EditText postCommentText1) {
		
		postCommentText = postCommentText1;
		popupWindow = popupWindow1;
		
		String string = s.toString();
		if ((string.lastIndexOf("@") != -1) && (string.lastIndexOf("@") > string.lastIndexOf("#"))) {
			isCompany = false;
			anchor = string.lastIndexOf("@");
			subString = string.substring(anchor + 1); 
			list = new ArrayList<Account>();
			Pattern p = Pattern.compile("(.*)(" + Pattern.quote(subString) + ")(.*)", Pattern.CASE_INSENSITIVE);
			for (int i = 0 ; i < friends.size() ; i++) {
				String friendsName = friends.get(i).firstname;
				if (p.matcher(friendsName).matches()) {
					list.add(friends.get(i));
				}
			}
			if (list.isEmpty()) {
				popupWindow.dismiss();
			} else {
				popupWindow.setAdapter(new FriendsDropDownAdapter(c, list));
				popupWindow.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						// TODO Auto-generated method stub
						String newString = postCommentText.getText().toString();
						StringBuffer sb = new StringBuffer(newString);
						
						if (isCompany) {
							hsCompanyTagged.add(list2.get(position));
							initialCompaniesTag = new ArrayList<Company>();
							initialCompaniesTag.addAll(hsCompanyTagged);
							
							if (subString.length() == 0) {
								postCommentText.append(list2.get(position).companyCode);
							} else {
								String latestString = sb.replace(anchor + 1, postCommentText.length(), list2.get(position).companyCode).toString();
								postCommentText.setText(latestString);
								postCommentText.setSelection(postCommentText.length());
							}
						} else {
							hsPersonTagged.add(list.get(position));
							initialFriendsTag = new ArrayList<Account>();
							initialFriendsTag.addAll(hsPersonTagged);
							
							if (subString.length() == 0) {
								postCommentText.append(list.get(position).firstname);
							} else {
								String latestString = sb.replace(anchor + 1, postCommentText.length(), list.get(position).firstname).toString();
								postCommentText.setText(latestString);
								postCommentText.setSelection(postCommentText.length());
							}
						}
						popupWindow.dismiss();
					}
				});
				popupWindow.setInputMethodMode(ListPopupWindow.INPUT_METHOD_NEEDED);
				popupWindow.show();
			}
		} else if ((string.lastIndexOf("#") != -1) && (string.lastIndexOf("#") > string.lastIndexOf("@"))) {
			isCompany = true;
			anchor = string.lastIndexOf("#");
			subString = string.substring(anchor + 1);
			list2 = new ArrayList<Company>();
			Pattern p = Pattern.compile("(.*)(" + Pattern.quote(subString) + ")(.*)", Pattern.CASE_INSENSITIVE);
			for (int i = 0 ; i < companies.size() ; i++) {
				String companyName = companies.get(i).companyCode;
				if (p.matcher(companyName).matches()) {
					list2.add(companies.get(i));
				}
			}
			if (list2.isEmpty()) {
				popupWindow.dismiss();
			} else {
				popupWindow.setAdapter(new CompaniesDropDownAdapter(c, list2));
				popupWindow.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						// TODO Auto-generated method stub
						String newString = postCommentText.getText().toString();
						StringBuffer sb = new StringBuffer(newString);
						
						if (isCompany) {
							hsCompanyTagged.add(list2.get(position));
							initialCompaniesTag = new ArrayList<Company>();
							initialCompaniesTag.addAll(hsCompanyTagged);
							
							if (subString.length() == 0) {
								postCommentText.append(list2.get(position).companyCode);
							} else {
								String latestString = sb.replace(anchor + 1, postCommentText.length(), list2.get(position).companyCode).toString();
								postCommentText.setText(latestString);
								postCommentText.setSelection(postCommentText.length());
							}
						} else {
							hsPersonTagged.add(list.get(position));
							initialFriendsTag = new ArrayList<Account>();
							initialFriendsTag.addAll(hsPersonTagged);
							
							if (subString.length() == 0) {
								postCommentText.append(list.get(position).firstname);
							} else {
								String latestString = sb.replace(anchor + 1, postCommentText.length(), list.get(position).firstname).toString();
								postCommentText.setText(latestString);
								postCommentText.setSelection(postCommentText.length());
							}
						}
						popupWindow.dismiss();
					}
				});
				popupWindow.setInputMethodMode(ListPopupWindow.INPUT_METHOD_NEEDED);
				popupWindow.show();
			}
		} else {
			popupWindow.dismiss();
		}
	}
	
	public static List<Account> loadFinalFriendsTag(String commentPosted) {
		List<Account> finalFriendsTag = new ArrayList<Account>();
		if (!initialFriendsTag.isEmpty()) {
			for (int i = 0 ; i < initialFriendsTag.size() ; i++) {
				if (commentPosted.matches("(.*)" + "@(" + initialFriendsTag.get(i).firstname + ")(.*)")) {
					finalFriendsTag.add(initialFriendsTag.get(i));
				}
			}
		}
		return finalFriendsTag;
	}

	public static List<Company> loadFinalCompaniesTag(String commentPosted) {
		List<Company> finalCompaniesTag = new ArrayList<Company>();
		if (!initialCompaniesTag.isEmpty()) {
			for (int i = 0 ; i < initialCompaniesTag.size() ; i++) {
				if (commentPosted.matches("(.*)" + "#(" + initialCompaniesTag.get(i).companyCode + ")(.*)")) {
					finalCompaniesTag.add(initialCompaniesTag.get(i));
				}
			}
		}
		return finalCompaniesTag;
	}
}
