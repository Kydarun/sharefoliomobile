package com.kydarun.sharefolio.util;

import java.io.UnsupportedEncodingException;
import java.util.Random;

import android.util.Base64;

public class Encryption {
	public static String encrypt(long plainText) {
		String cipherText = "";
		
		String firstValue;
		try {
			firstValue = new String(Base64.encode((generateRandomNumber() + "").getBytes("ISO-8859-1"), Base64.NO_WRAP));
			String secondValue = new String(Base64.encode((generateRandomNumber() + "").getBytes("ISO-8859-1"), Base64.NO_WRAP));
			String accountID = new String(Base64.encode((plainText + "").getBytes("ISO-8859-1"), Base64.NO_WRAP));
			
			cipherText = firstValue + accountID + secondValue;
			
			return cipherText;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		return "";
	}
	
	public static long decrypt(String cipherText) {
		if (cipherText.equals("")) return 0;
		
		int plainText = 0;
		
		String accountID = cipherText.substring(12, cipherText.length());
		accountID = accountID.substring(0, accountID.length() - 12);
		try {
			plainText = Integer.parseInt(new String(Base64.decode(accountID.getBytes("ISO-8859-1"), Base64.NO_WRAP)));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		return plainText;
	}
	
	private static int generateRandomNumber() {
		Random random = new Random();
		return random.nextInt(88888888) + 11111111;
	}
}
