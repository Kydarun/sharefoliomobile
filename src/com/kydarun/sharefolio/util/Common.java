package com.kydarun.sharefolio.util;

import java.io.ByteArrayOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.Calendar;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Seconds;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.CreatePlatformEndpointRequest;
import com.amazonaws.services.sns.model.CreatePlatformEndpointResult;
import com.amazonaws.services.sns.model.DeleteEndpointRequest;
import com.amazonaws.services.sns.model.ListPlatformApplicationsRequest;
import com.amazonaws.services.sns.model.ListPlatformApplicationsResult;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.obj.Account;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources.NotFoundException;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.MediaStore.Images;

public class Common {
	
	public static String getSetting(Context context, String key) {
		return getSetting(context, key, "");
	}
	
	public static String getSetting(Context context, String key, String defaultValue) {
        // Get the app's shared preferences
        SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String setting = app_preferences.getString(key, defaultValue);
        
        return setting;
	}
	
	
	public static long getSetting(Context context, String key, long defaultValue) {
        // Get the app's shared preferences
        SharedPreferences app_preferences = 
        	PreferenceManager.getDefaultSharedPreferences(context);
       
        long setting = app_preferences.getLong(key, defaultValue);
        
        return setting;
	}	
	
	public static boolean getSetting(Context context, String key, boolean defaultValue) {
        // Get the app's shared preferences
        SharedPreferences app_preferences = 
        	PreferenceManager.getDefaultSharedPreferences(context);
       
        boolean setting = app_preferences.getBoolean(key, defaultValue);
        
        return setting;
	}
	
	public static void setSetting(Context context, String key, String value) {
        // Get the app's shared preferences
        SharedPreferences app_preferences = 
        	PreferenceManager.getDefaultSharedPreferences(context);
        
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putString(key, value);
        editor.commit(); // Very important
	}
	
	public static void setSetting(Context context, String key, long value) {
        // Get the app's shared preferences
        SharedPreferences app_preferences = 
        	PreferenceManager.getDefaultSharedPreferences(context);
        
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putLong(key, value);
        editor.commit(); // Very important
	}
	
	public static void setSetting(Context context, String key, boolean value) {
        // Get the app's shared preferences
        SharedPreferences app_preferences = 
        	PreferenceManager.getDefaultSharedPreferences(context);
        
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putBoolean(key, value);
        editor.commit(); // Very important
	}
	
	public static boolean imageExist(String imageUrl) {
		try {
			HttpURLConnection.setFollowRedirects(false);
			HttpURLConnection con = (HttpURLConnection)new URL(imageUrl).openConnection();
			con.setRequestMethod("HEAD");
			return con.getResponseCode() == HttpURLConnection.HTTP_OK;
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static String trimText(String text, int length) {
		return text == null ? "" : (text.length() < length ? text : text.substring(0, length) + "..");
	}
	
	public static Typeface getFontBold(Context context) {
		return Typeface.createFromAsset(context.getAssets(), "nunito_bold.ttf");
	}
	
	public static Typeface getFontRegular(Context context) {
		return Typeface.createFromAsset(context.getAssets(), "nunito_regular.ttf");
	}
	
	public static Typeface getFontLight(Context context) {
		return Typeface.createFromAsset(context.getAssets(), "nunito_light.ttf");
	}
	
	public static String formatDateDiff(long gap) {
		if (gap == 0) {
			return "Just now";
		}
		
		DateTime stamp = new DateTime(System.currentTimeMillis() - (gap * 1000));
		DateTime now = new DateTime();
		
		Seconds seconds = Seconds.secondsBetween(stamp, now);
		if (seconds.getSeconds() < 60) {
			return doPlural(seconds.getSeconds(), "second");
		}
		
		Minutes minutes = Minutes.minutesBetween(stamp, now);
		if (minutes.getMinutes() < 60) {
			return doPlural(minutes.getMinutes(), "minute");
		}
		
		Hours hours = Hours.hoursBetween(stamp, now);
		if (hours.getHours() < 24) {
			return doPlural(hours.getHours(), "hour");
		}
		
		Days days = Days.daysBetween(stamp, now);
		if (days.getDays() < 4) {
			return doPlural(days.getDays(), "day");
		}
		else {
			return stamp.toString("EEE, dd MMM YYYY, hh:mm:ss aa ");
		}
	}
	
	public static String getChatStamp(long gap) {
		DateTime stamp = new DateTime(System.currentTimeMillis() - (gap * 1000));
		DateTime now = new DateTime();
		
		if (Days.daysBetween(stamp, now).getDays() == 0) {
			return stamp.toString("hh:mm aa");
		}
		else {
			return stamp.toString("EEE, dd MMM YYYY, hh:mm aa");
		}
	}
	
	public static String percentageChange(double before, double after) {
		DecimalFormat df=new DecimalFormat("0.00");
		double result = (after - before) / before * 100;
		return "(" + (result > 0d ? "+" : "") + df.format(result) + "%)";
	}
	
	public static CreatePlatformEndpointResult registerPushNotification(Context context, GoogleCloudMessaging gcm, String token) {
		try {
			AWSCredentials credentials = new BasicAWSCredentials(context.getResources().getString(R.string.aws_access_key_id), 
					context.getResources().getString(R.string.aws_secret_access_key));
			
			AmazonSNSClient client = new AmazonSNSClient(credentials);
			ListPlatformApplicationsRequest platformRequest = new ListPlatformApplicationsRequest();
			platformRequest.setRequestCredentials(credentials);
			ListPlatformApplicationsResult platformResult = client.listPlatformApplications(platformRequest);
			
			CreatePlatformEndpointRequest request = new CreatePlatformEndpointRequest();
			request.setPlatformApplicationArn(platformResult.getPlatformApplications().get(0).getPlatformApplicationArn());
			request.setToken(token);
			
			return client.createPlatformEndpoint(request);
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static CreatePlatformEndpointResult reRegisterEndpoint(Context context, GoogleCloudMessaging gcm, Account session, String token) {
		AWSCredentials credentials = new BasicAWSCredentials(context.getResources().getString(R.string.aws_access_key_id), 
				context.getResources().getString(R.string.aws_secret_access_key));
		AmazonSNSClient client = new AmazonSNSClient(credentials);
		DeleteEndpointRequest deleteRequest = new DeleteEndpointRequest();
		deleteRequest.setRequestCredentials(credentials);
		deleteRequest.setEndpointArn(session.platformEndpointArn);
		client.deleteEndpoint(deleteRequest);
		
		return registerPushNotification(context, gcm, token);
	}
	
	public static Uri generateImageFromBitmap(Context context, Bitmap bitmap) {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = Images.Media.insertImage(context.getContentResolver(), bitmap, "Sharefolio_" + Calendar.getInstance().getTimeInMillis() + ".jpg", null);
        return Uri.parse(path);
	}
	
	private static String doPlural(int number, String str) {
		return number > 1 ? number + " " + str + "s ago" : number + " " + str + " ago";
	}
	
	public static int getNotificationCount(Context context) {
		return Integer.parseInt(Common.getSetting(context, "notification_count", "0"));
	}
	
	public static void setNotificationCount(Context context, int count) {
		Common.setSetting(context, "notification_count", "" + count);
	}
	
	public static boolean isPhoneNumberVerified(SessionManager sm) {
		Account account = sm.getCurrentSession();
		return !account.phoneNumber.equals("");
	}
}
