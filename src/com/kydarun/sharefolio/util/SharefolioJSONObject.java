package com.kydarun.sharefolio.util;

import org.json.JSONException;
import org.json.JSONObject;

public class SharefolioJSONObject extends JSONObject {

	public SharefolioJSONObject(String string) throws JSONException {
		super(string);
	}
	
	public SharefolioJSONObject() {
		super();
	}

	@Override
	public String getString(String name) {
		try {
			return this.has(name) && !this.isNull(name) ? super.getString(name) : "";
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return "";
	}
	
	@Override
	public long getLong(String name) {
		try {
			return this.has(name) && !this.isNull(name) ? super.getLong(name) : -1;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return -1;
	}

	@Override
	public Object get(String name) {
		try {
			return this.has(name) && !this.isNull(name) ? super.get(name) : null;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public boolean getBoolean(String name) {
		try {
			return this.has(name) && !this.isNull(name) ? super.getBoolean(name) : false;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public double getDouble(String name) {
		try {
			return this.has(name) && !this.isNull(name) ? super.getDouble(name) : -1d;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return -1d;
	}

	@Override
	public int getInt(String name) {
		try {
			return this.has(name) && !this.isNull(name) ? super.getInt(name) : -1;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return -1;
	}

	@Override
	public SharefolioJSONArray getJSONArray(String name) {
		try {
			return super.has(name) && !this.isNull(name) ? new SharefolioJSONArray(super.getJSONArray(name).toString()) : new SharefolioJSONArray();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return new SharefolioJSONArray();
	}

	@Override
	public SharefolioJSONObject getJSONObject(String name) {
		try {
			return super.has(name) && !this.isNull(name) ? new SharefolioJSONObject(super.getJSONObject(name).toString()) : new SharefolioJSONObject();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return new SharefolioJSONObject();
	}
	
	public boolean isValid() {
		try {
			return super.getString("status").equals("ok");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return false;
	}
}
