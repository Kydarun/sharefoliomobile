package com.kydarun.sharefolio;

import com.kydarun.sharefolio.fragment.CommentFragment;

import android.app.Fragment;

public class CommentActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		// TODO Auto-generated method stub
		long wallID = getIntent().getLongExtra(CommentFragment.KEY_WALLID, 0);
		long accountID = getIntent().getLongExtra(CommentFragment.KEY_ACCOUNTID, 0);
		
		return CommentFragment.newInstance(wallID, accountID);
	}

}
