package com.kydarun.sharefolio.constant;

public interface EnvironmentalConstant {
	public static final boolean DEBUG = false;
	public static final long FETCH_PRICE_INTERVAL = 20000;	// Milliseconds interval for fetching price
}
