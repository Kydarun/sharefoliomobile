package com.kydarun.sharefolio.constant;

public interface NotificationConstant {
	public static final String COMPANY = "Company";
	public static final String WALL = "Wall";
	public static final String COMMENT = "Comment";
	public static final String GROUP = "Group";
	public static final String FRIEND_REQUEST = "FriendRequest";
	public static final String ACCOUNT = "Account";
	
	public static final String SMS_NOTIFICATION = "com.kydarun.sharefolio.action.SMS";
	public static final String CHAT_NOTIFICATION = "com.kydarun.sharefolio.action.CHAT";
}
