package com.kydarun.sharefolio.constant;

public interface PageNameConstant {
	public static final String HOME_PAGE = "Home";
	public static final String MY_CHAT = "Groups";
	public static final String MY_COMPANY = "Companies";
	public static final String COMPANY_INFO = "Company Info";
	public static final String SETTING = "Settings";
	public static final String SEARCH = "Search";
	public static final String CHAT = "Chat";
	public static final String PROFILE = "Profile";
	public static final String EDIT_GROUP_INFO = "Edit Group Info";
	public static final String WALL = "Wall";
}
