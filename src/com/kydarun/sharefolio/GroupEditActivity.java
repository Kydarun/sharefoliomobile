package com.kydarun.sharefolio;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.kydarun.sharefolio.api.GroupApi;
import com.kydarun.sharefolio.constant.PageNameConstant;
import com.kydarun.sharefolio.obj.Group;

public class GroupEditActivity extends BaseActivity {
	public static final String CURRENT_GROUP = "current group";
	Group group;
	ImageView ivGroupCoverPhoto;
	EditText etGroupName;
	Button bEditGroupInfo;
	Button bSubmitEditedGroupInfo;
	GroupApi ga;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_group_edit);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		group = (Group) getIntent().getSerializableExtra(CURRENT_GROUP);
		
		getFragmentManager().beginTransaction().add(R.id.rlHeader, header).commit();
		super.setTitle(PageNameConstant.EDIT_GROUP_INFO);
		
		ivGroupCoverPhoto = (ImageView) findViewById(R.id.ivGroupCoverPhoto);
		
		etGroupName = (EditText) findViewById(R.id.etGroupName);
		etGroupName.setText(group.groupname);
		
		bEditGroupInfo = (Button) findViewById(R.id.bEditGroupInfo);
		bEditGroupInfo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				etGroupName.setEnabled(true);
			}
		});
		
		bSubmitEditedGroupInfo = (Button) findViewById(R.id.bSubmitEditedGroupInfo);
		bSubmitEditedGroupInfo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new EditGroupTask().execute();
			}
		});
		
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private class EditGroupTask extends AsyncTask<Void, Void, Void> {
		Boolean isEditGroupSuccess;

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			ga = new GroupApi(context);
			String groupName = etGroupName.getText().toString();
			group.groupname = groupName;
			
			try {
				isEditGroupSuccess = ga.editGroup(sm.getCurrentSession().ID, group);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			if (isEditGroupSuccess) {
				Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
				etGroupName.setEnabled(false);
				Intent i = new Intent();
				i.putExtra("EDITED_GROUP", group);
				setResult(GroupInfoActivity.REQUEST_CODE, i);
				finish();
			} else {
				Toast.makeText(context, "Fail", Toast.LENGTH_SHORT).show();
			}
		}
	}
	
}
