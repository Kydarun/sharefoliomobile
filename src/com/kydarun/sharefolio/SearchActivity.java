package com.kydarun.sharefolio;

import java.util.List;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;

import com.costum.android.widget.PullAndLoadListView;
import com.costum.android.widget.PullAndLoadListView.OnLoadMoreListener;
import com.costum.android.widget.PullToRefreshListView.OnRefreshListener;
import com.kydarun.sharefolio.adapter.AdapterBase;
import com.kydarun.sharefolio.adapter.CompanySearchAdapter;
import com.kydarun.sharefolio.adapter.GroupSearchAdapter;
import com.kydarun.sharefolio.adapter.PeopleSearchAdapter;
import com.kydarun.sharefolio.api.ApiBase;
import com.kydarun.sharefolio.api.CompanyApi;
import com.kydarun.sharefolio.api.GroupApi;
import com.kydarun.sharefolio.api.UserApi;
import com.kydarun.sharefolio.constant.PageNameConstant;
import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.obj.Company;
import com.kydarun.sharefolio.obj.Group;

public class SearchActivity extends BaseActivity implements OnLoadMoreListener, OnRefreshListener, OnItemClickListener, OnClickListener {
	protected String sourcePageName;
	PullAndLoadListView lvSearch;
	AdapterBase adapter;
	Group group;
	FrameLayout flDone;
	Button btnDone;
	
	String query;
	
	int start;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		this.sourcePageName = getIntent().getExtras().getString("PageName");
		
		flProgress = (FrameLayout)findViewById(R.id.flProgress);
		flProgress.setVisibility(View.GONE);
		
		if (sourcePageName.equals(PageNameConstant.HOME_PAGE)) {
			super.setTitle(PageNameConstant.SEARCH + " People");
		}
		else if (sourcePageName.equals(PageNameConstant.MY_COMPANY)) {
			super.setTitle(PageNameConstant.SEARCH + " Companies");
		}
		else if (sourcePageName.equals(PageNameConstant.MY_CHAT)) {
			super.setTitle(PageNameConstant.SEARCH + " Groups");
		}
		
		lvSearch = (PullAndLoadListView)findViewById(R.id.lvSearch);
		if (lvSearch != null) {
			lvSearch.setOnLoadMoreListener(this);
			lvSearch.setOnRefreshListener(this);
			lvSearch.setOnItemClickListener(this);
			lvSearch.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);
		}
		
		flDone = (FrameLayout)findViewById(R.id.flDone);
		btnDone = (Button)findViewById(R.id.btnDone);
		Bundle b = getIntent().getExtras();
		if (b.containsKey("Group")) {
			group = (Group)getIntent().getExtras().getSerializable("Group");
			flDone.setVisibility(View.VISIBLE);
			btnDone.setOnClickListener(this);
		}
		else {
			group = null;
			flDone.setVisibility(View.GONE);
		}
		
		start = 0;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.search, menu);
		
		MenuItem searchItem = menu.findItem(R.id.menu_item_search);
		final SearchView searchView = (SearchView) searchItem.getActionView();
		searchView.setQueryHint(getTitle());
		searchView.setOnQueryTextListener(new OnQueryTextListener() {
			
			@Override
			public boolean onQueryTextSubmit(String searchQuery) {
				// TODO Auto-generated method stub
				query = searchQuery;
				search(query);
				searchView.clearFocus();
				return true;
			}
			
			@Override
			public boolean onQueryTextChange(String newText) {
				// TODO Auto-generated method stub
				return true;
			}
		});
		
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}	
	}

	@Override
	public void onRefresh() {
		start = 0;
		flProgress.setVisibility(View.VISIBLE);
		new SearchAsyncTask(query).execute();
	}

	@Override
	public void onLoadMore() {
		new SearchAsyncTask(query).execute();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if (sourcePageName.equals(PageNameConstant.HOME_PAGE)) {
			Account account = ((PeopleSearchAdapter)adapter).peoples.get(position - 1);
			Intent i = new Intent(this, ProfileActivity.class);
			Bundle b = new Bundle();
			b.putSerializable("Account", account);
			i.putExtras(b);
			startActivity(i);
		}
		else if (sourcePageName.equals(PageNameConstant.MY_COMPANY)) {
			Company company = ((CompanySearchAdapter)adapter).companies.get(position - 1);
			Intent intent = new Intent(this, CompanyActivity.class);
			Bundle b = new Bundle();
			b.putSerializable("company", company);
			intent.putExtras(b);
			startActivity(intent);
		}
		else if (sourcePageName.equals(PageNameConstant.MY_CHAT)) {
			Group group = ((GroupSearchAdapter)adapter).groups.get(position - 1);
			Intent intent = new Intent(this, GroupInfoActivity.class);
			Bundle b = new Bundle();
			b.putSerializable("Group", group);
			intent.putExtras(b);
			startActivity(intent);
		}
	}
	
	public void search(String query) {
		start = 0;
		flProgress.setVisibility(View.VISIBLE);
		new SearchAsyncTask(query).execute();
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btnDone:
				finish();
				break;
		}
	}

	class SearchAsyncTask extends AsyncTask<Void, Void, Void> {
		String query;
		
		public SearchAsyncTask(String query) {
			this.query = query;
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			ApiBase api;
			if (sourcePageName.equals(PageNameConstant.HOME_PAGE)) {
				api = new UserApi(context);
				try {
					List<Account> peoples = ((UserApi)api).searchPeople(sm.getCurrentSession().ID, query, start, 15);
					if (start == 0) {
						if (group != null) {
							adapter = new PeopleSearchAdapter(context, peoples, group);
						}
						else {
							adapter = new PeopleSearchAdapter(context, peoples);
						}
					}
					else {
						((PeopleSearchAdapter)adapter).peoples.addAll(peoples);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else if (sourcePageName.equals(PageNameConstant.MY_COMPANY)) {
				api = new CompanyApi(context);
				try {
					List<Company> companies = ((CompanyApi)api).searchCompany(sm.getCurrentSession().ID, query, start, 15);
					
					if (group != null) {		
						// Reorganize adapter items to check companies that are already following
						for (int i = 0; i < companies.size(); i++) {
							Company c = companies.get(i);
							companies.get(i).isFollowing = false;
							boolean isFound = false;
							for (int j = 0; j < group.companies.size(); j++) {
								Company co = group.companies.get(j);
								if (c.id.equals(co.id)) {
									isFound = true;
									companies.get(i).isFollowing = true;
								}
								
								if (!isFound) {
									companies.get(i).isFollowing = false;
								}
							}
						}
					}
					if (start == 0) {
						if (group != null) {
							adapter = new CompanySearchAdapter(context, companies, group);
						}
						else {
							adapter = new CompanySearchAdapter(context, companies);
						}
					}
					else {
						((CompanySearchAdapter)adapter).companies.addAll(companies);
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else if (sourcePageName.equals(PageNameConstant.MY_CHAT)) {
				api = new GroupApi(context);
				try {
					List<Group> groups = ((GroupApi)api).searchGroup(sm.getCurrentSession().ID, query, start, 15);
					if (start == 0) {
						adapter = new GroupSearchAdapter(context, groups);
					}
					else {
						((GroupSearchAdapter)adapter).groups.addAll(groups);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (adapter == null) {
				flProgress.setVisibility(View.GONE);
				return;
			}
			
			if (start == 0) {
				lvSearch.setAdapter(adapter);
				lvSearch.onRefreshComplete();
			}
			else {
				adapter.notifyDataSetChanged();
				lvSearch.onLoadMoreComplete();
				
			}
			
			flProgress.setVisibility(View.GONE);
			
			start += 15;
		}
	}
}
