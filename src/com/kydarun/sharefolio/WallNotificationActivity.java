package com.kydarun.sharefolio;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListPopupWindow;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.costum.android.widget.PullAndLoadListView;
import com.costum.android.widget.PullToRefreshListView.OnRefreshListener;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.kydarun.sharefolio.adapter.CommentAdapter;
import com.kydarun.sharefolio.api.WallApi;
import com.kydarun.sharefolio.constant.PageNameConstant;
import com.kydarun.sharefolio.database.DBHelper;
import com.kydarun.sharefolio.fragment.HeaderFragment;
import com.kydarun.sharefolio.fragment.LikerFromWallFragment;
import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.obj.Comment;
import com.kydarun.sharefolio.obj.CommentBank;
import com.kydarun.sharefolio.obj.Company;
import com.kydarun.sharefolio.obj.Wall;
import com.kydarun.sharefolio.util.Common;
import com.kydarun.sharefolio.util.SharefolioSpannableHandler;
import com.kydarun.sharefolio.util.SuggestionList;

public class WallNotificationActivity extends BaseActivity {

	List<Account> friends;
	List<Company> companies;

	CommentBank cb;
	CommentAdapter adapter;
	ArrayList<Comment> comments = new ArrayList<Comment>();
	ArrayList<Comment> latestComments = new ArrayList<Comment>();

	Wall currentWall;
	Wall singleWall;
	Wall latestReturnedWall;
	long currentWallID;
	PullAndLoadListView lvWallAndComment;

	RelativeLayout rlForAvatar;
	ImageView imageView;
	TextView wallPoster;
	TextView wallPost;
	WebView wv;
	TextView posterCompanyAnnouncementTitle;
	TextView posterCompanyAnnouncementLink;
	LinearLayout ll;
	TextView targetName;
	Button like;
	Button unlike;
	Button comment;
	TextView gap;
	LinearLayout llLike;
	TextView numberOfLike;

	ListPopupWindow popupWindow;
	EditText etComment;
	Button bSubmitComment;
	Comment newComment;
	String oldMessage;
	FrameLayout flProgress;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wall_notification);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		flProgress = (FrameLayout) findViewById(R.id.flProgress);

		friends = DBHelper.get(context).getAllFriends();
		companies = DBHelper.get(context).getAllCompanies();

		header = new HeaderFragment(this);
		header.pageName = PageNameConstant.WALL;

		getFragmentManager().beginTransaction().add(R.id.rlHeader, header)
				.commit();
		super.setTitle(PageNameConstant.WALL);

		currentWall = (Wall) getIntent().getSerializableExtra("Wall");
		currentWallID = currentWall.wallID;

		new LoadSinglePostTask().execute();

		LayoutInflater inflater = getLayoutInflater();
		View wallView = inflater.inflate(R.layout.homepage_item_view, null, false);

		rlForAvatar = (RelativeLayout) wallView.findViewById(R.id.rlForAvatar);
		imageView = (ImageView) wallView.findViewById(R.id.item_ivProfilePicture);
		wallPoster = (TextView) wallView.findViewById(R.id.item_tvUserName);
		wallPost = (TextView) wallView.findViewById(R.id.item_tvPost);
		wv = (WebView) wallView.findViewById(R.id.item_webview);
		posterCompanyAnnouncementTitle = (TextView) wallView.findViewById(R.id.item_tvAnnouncementTitle);
		posterCompanyAnnouncementLink = (TextView) wallView.findViewById(R.id.item_tvAnnouncementLink);
		ll = (LinearLayout) wallView.findViewById(R.id.linear);
		targetName = (TextView) wallView.findViewById(R.id.item_tvTarget);
		like = (Button) wallView.findViewById(R.id.item_bLikeButton);
		unlike = (Button) wallView.findViewById(R.id.item_bUnlikeButton);
		comment = (Button) wallView.findViewById(R.id.item_bCommentButton);
		gap = (TextView) wallView.findViewById(R.id.item_tvGap);
		llLike = (LinearLayout) wallView.findViewById(R.id.item_llLike);
		numberOfLike = (TextView) wallView.findViewById(R.id.numberOfLike);

		like.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new LikePostTask().execute(currentWallID);
			}
		});

		unlike.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new UnlikePostTask().execute(currentWallID);
			}
		});

		llLike.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i = new Intent(context, LikerFromWallActivity.class);
				i.putExtra(LikerFromWallFragment.EXTRA_CURRENT_WALL, latestReturnedWall);
				context.startActivity(i);
			}
		});

		comment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/**
				 * Intent intent = new Intent(context, CommentActivity2.class);
				 * intent.putExtra(CommentFragment.KEY_WALLID,
				 * currentWall.wallID);
				 * intent.putExtra(CommentFragment.KEY_ACCOUNTID,
				 * sm.getCurrentSession().ID); context.startActivity(intent);
				 **/
			}
		});

		String gapString = Common.formatDateDiff(currentWall.gap);
		gap.setText(gapString);

		cb = new CommentBank(context, currentWallID, flProgress);
		adapter = new CommentAdapter(context, cb.getComments());

		lvWallAndComment = (PullAndLoadListView) findViewById(R.id.lvWallAndComment);
		lvWallAndComment.addHeaderView(wallView);
		lvWallAndComment.setAdapter(adapter);
		lvWallAndComment.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);

		cb.setupCommentBank(sm.getCurrentSession().ID, adapter, lvWallAndComment);

		lvWallAndComment.setOnRefreshListener(new OnRefreshListener() {

			public void onRefresh() {
				// Do work to refresh the list here.
				cb.onLoadMoreComment();
			}
		});

		etComment = (EditText) findViewById(R.id.etComment);

		popupWindow = new ListPopupWindow(context);
		popupWindow.setAnchorView(etComment);
		popupWindow.setInputMethodMode(ListPopupWindow.INPUT_METHOD_NEEDED);

		etComment.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				SuggestionList.loadSuggestionList(context, popupWindow, s, friends, companies, etComment);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		bSubmitComment = (Button) findViewById(R.id.bComment);
		bSubmitComment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String commentPosted = etComment.getText().toString();
				oldMessage = commentPosted;

				if (!isEmpty(commentPosted)) {
					bSubmitComment.setEnabled(false);
					etComment.setEnabled(false);

					newComment = new Comment();
					newComment.user = new Account();
					((Account) newComment.user).ID = sm.getCurrentSession().ID;

					newComment.personTagged = SuggestionList.loadFinalFriendsTag(commentPosted);
					newComment.companyTagged = SuggestionList.loadFinalCompaniesTag(commentPosted);
					
					if (newComment.personTagged.size() != 0) {
						for (int i = 0 ; i < newComment.personTagged.size() ; i++) {
							if (commentPosted.contains(newComment.personTagged.get(i).firstname)) {
								commentPosted = commentPosted.replace(newComment.personTagged.get(i).firstname, newComment.personTagged.get(i).ID.toString());
							}
						}
					}
					
					if (newComment.companyTagged.size() != 0) {
						for (int i = 0 ; i < newComment.companyTagged.size() ; i++) {
							if (commentPosted.contains(newComment.companyTagged.get(i).companyCode)) {
								commentPosted = commentPosted.replace(newComment.companyTagged.get(i).companyCode, newComment.companyTagged.get(i).id);
							}
						}
					}

					newComment.wallID = currentWallID;
					newComment.message = commentPosted;
					new SubmitCommentTask().execute();
				} else {
					Toast.makeText(context, "Please enter your comment...", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public static boolean isEmpty(String commentPosted) {
		if (commentPosted.trim().length() != 0) {
			return false;
		} else {
			return true;
		}
	}

	private class LoadSinglePostTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			WallApi wa = new WallApi(context);
			try {
				singleWall = wa.loadSinglePost(sm.getCurrentSession().ID, currentWallID);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			if (singleWall.posterCompany != null) {
				ll.setVisibility(LinearLayout.GONE);
				
				rlForAvatar.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(context, CompanyActivity.class);
						Bundle bundle = new Bundle();
						bundle.putSerializable("company", singleWall.posterCompany);
						intent.putExtras(bundle);
						context.startActivity(intent);
					}
				});

				String posterCompanyAvatarLink = "http://sharefolio.net/avatar/company/" + singleWall.posterCompany.id + singleWall.posterCompany.avatar;
				UrlImageViewHelper.setUrlDrawable(imageView, posterCompanyAvatarLink);

				wallPoster.setText(singleWall.posterCompany.id + "." + singleWall.posterCompany.companyCode);

				wallPost.setVisibility(TextView.GONE);

				wv.loadData(singleWall.wallContent, "text/html", "UTF-8");
				wv.setVisibility(WebView.VISIBLE);

				posterCompanyAnnouncementTitle.setVisibility(TextView.VISIBLE);
				posterCompanyAnnouncementTitle.setText(singleWall.announcementTitle);

				posterCompanyAnnouncementLink.setVisibility(TextView.VISIBLE);
				posterCompanyAnnouncementLink.setText(singleWall.announcementLink);
			} else {
				wv.setVisibility(WebView.GONE);
				posterCompanyAnnouncementTitle.setVisibility(TextView.GONE);
				posterCompanyAnnouncementLink.setVisibility(TextView.GONE);
				wallPost.setVisibility(TextView.VISIBLE);
				ll.setVisibility(LinearLayout.GONE);
				
				rlForAvatar.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(context, LoadUserWallActivity.class);
						intent.putExtra(LoadUserWallActivity.TAG, singleWall.poster);
						context.startActivity(intent);
					}
				});

				String posterAvatarLink = "http://sharefolio.net/avatar/" 
						+ singleWall.poster.ID 
						+ singleWall.poster.avatar;
				UrlImageViewHelper.setUrlDrawable(imageView, posterAvatarLink);

				wallPoster.setText(singleWall.poster.firstname);
				
				String wallContent = Html.fromHtml(singleWall.wallContent).toString();

				SpannableStringBuilder ssb = SharefolioSpannableHandler.getSpannableStringBuilder(context, wallContent, currentWall.personTagged, currentWall.companyTagged);
				wallPost.setMovementMethod(LinkMovementMethod.getInstance());
				wallPost.setText(ssb);

				if (singleWall.targetCompany != null) {
					targetName.setText(currentWall.targetCompany.id 
							+ "." 
							+ currentWall.targetCompany.companyCode);
					ll.setVisibility(LinearLayout.VISIBLE);
				} else if (singleWall.target != null) {
					targetName.setText(currentWall.target.firstname);
					ll.setVisibility(LinearLayout.VISIBLE);
				}
			}
			updateWall(singleWall, currentWallID);
		}
	}

	private class LikePostTask extends AsyncTask<Long, Void, Void> {
		long wID;
		Wall insideWall;
		boolean isLikePostSuccess;

		@Override
		protected Void doInBackground(Long... params) {
			// TODO Auto-generated method stub
			WallApi wa = new WallApi(context);
			wID = params[0];
			try {
				insideWall = wa.likePost(sm.getCurrentSession().ID, wID);
				isLikePostSuccess = insideWall.liked;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			if (isLikePostSuccess) {
				singleWall = insideWall;
				updateWall(insideWall, wID);
			}
		}
	}

	private class UnlikePostTask extends AsyncTask<Long, Void, Void> {
		long wID;
		Wall insideWall;
		boolean isLikePostSuccess;

		@Override
		protected Void doInBackground(Long... params) {
			// TODO Auto-generated method stub
			WallApi wa = new WallApi(context);
			wID = params[0];
			try {
				insideWall = wa.unlikePost(sm.getCurrentSession().ID, wID);
				isLikePostSuccess = insideWall.liked;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			if (isLikePostSuccess == false) {
				singleWall = insideWall;
				updateWall(insideWall, wID);
			}
		}
	}

	public void updateWall(Wall wall, long wallID) {
		latestReturnedWall = wall;
		if (wall.like.size() != 0) {
			llLike.setVisibility(LinearLayout.VISIBLE);
			numberOfLike.setText(String.valueOf(wall.like.size()));
		} else {
			llLike.setVisibility(LinearLayout.GONE);
		}

		if (wall.liked) {
			unlike.setVisibility(Button.VISIBLE);
			like.setVisibility(Button.GONE);
		} else {
			like.setVisibility(Button.VISIBLE);
			unlike.setVisibility(Button.GONE);
		}
	}

	public class SubmitCommentTask extends AsyncTask<Void, Void, Void> {
		List<Comment> checkLatestComment = new ArrayList<Comment>();

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			checkLatestComment.clear();
			WallApi waComment = new WallApi(context);
			try {
				checkLatestComment.addAll(waComment.submitComment(newComment));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			Comment latestComment = checkLatestComment.get(0);

			if (isCommentSubmitted(latestComment)) {
				cb.getComments().add(latestComment);
			}
			lvWallAndComment.setSelection(adapter.getCount() - 1);
			Toast.makeText(context, "Comment is submitted", Toast.LENGTH_SHORT).show();
			bSubmitComment.setEnabled(true);
			etComment.setEnabled(true);
			etComment.setText("");
		}

		private boolean isCommentSubmitted(Comment latestComment) {
			// TODO Auto-generated method stub
			if (((Account) latestComment.user).ID.equals(((Account) newComment.user).ID) && latestComment.message.equals(oldMessage)) {
				return true;
			} else {
				return false;
			}
		}
	}

}
