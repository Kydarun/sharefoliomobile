package com.kydarun.sharefolio;

import java.util.ArrayList;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;

import com.costum.android.widget.PullAndLoadListView;
import com.costum.android.widget.PullAndLoadListView.OnLoadMoreListener;
import com.costum.android.widget.PullToRefreshListView.OnRefreshListener;

import com.kydarun.sharefolio.adapter.WallAdapter;
import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.obj.Wall;
import com.kydarun.sharefolio.api.WallApi;

public class LoadUserWallActivity extends BaseActivity {
	public static final String TAG = "com.kydarun.sharefolio.LoadUserWallActivity_Tag";

	PullAndLoadListView lvWall;
	WallAdapter adapter;
	ArrayList<Wall> walls = new ArrayList<Wall>();
	Account person;
	
	int starting = 0;
	final int NUMBER_OF_RESULT = 10;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_load_user_wall);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		person = (Account) getIntent().getSerializableExtra(TAG);

		getFragmentManager().beginTransaction().add(R.id.rlHeader, header).commit();
		super.setTitle(person.firstname);

		adapter = new WallAdapter(context, walls);
		lvWall = (PullAndLoadListView) findViewById(R.id.lvWall);
		lvWall.setAdapter(adapter);

		new LoadMoreWallsTask().execute();

		lvWall.setOnRefreshListener(new OnRefreshListener() {
			// Do work to refresh the list here.
			public void onRefresh() {
				starting = 0;
				new RefreshWallTask().execute();
			}
		});

		lvWall.setOnLoadMoreListener(new OnLoadMoreListener() {
			// Do the work to load more items at the end of list here.
			public void onLoadMore() {
				starting += NUMBER_OF_RESULT;
				new LoadMoreWallsTask().execute();
			}
		});

	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private class LoadMoreWallsTask extends AsyncTask<Void, Void, Void> {
		WallApi wa = new WallApi(context);

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			if (isCancelled()) {
				return null;
			}
			try {
				walls.addAll(wa.loadUserWall(sm.getCurrentSession().ID, person.ID, starting, NUMBER_OF_RESULT));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			adapter.notifyDataSetChanged();
			lvWall.onLoadMoreComplete();
		}

		protected void onCancelled() {
			// Notify the loading more operation has finished
			lvWall.onLoadMoreComplete();
		}
	}

	private class RefreshWallTask extends AsyncTask<Void, Void, Void> {
		WallApi wa = new WallApi(context);

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			if (isCancelled()) {
				return null;
			}
			walls.clear();
			try {
				walls.addAll(wa.loadUserWall(sm.getCurrentSession().ID, person.ID, starting, NUMBER_OF_RESULT));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			adapter.notifyDataSetChanged();
			lvWall.onRefreshComplete();
		}

		protected void onCancelled() {
			// Notify the loading more operation has finished
			lvWall.onLoadMoreComplete();
		}
	}

}
