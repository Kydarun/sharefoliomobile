package com.kydarun.sharefolio;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

public class WelcomeActivity extends BaseActivity {
	boolean isCreated = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.welcome);
		
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				Intent intent;
				if (sm.isSessionExists()) {
					intent = new Intent(WelcomeActivity.this, MainActivity.class);
				}
				else {
					intent = new Intent(WelcomeActivity.this, LoginActivity.class);
				}
				startActivity(intent);
				isCreated = true;
			}
			
		}, 2000);
		
		Tracker tracker = GoogleAnalytics.getInstance(context).newTracker(R.xml.tracker_config);
		tracker.setScreenName("WelcomeActivity");
		tracker.send(new HitBuilders.AppViewBuilder().build());
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		if (isCreated) {
			finish();
		}
	}
}
