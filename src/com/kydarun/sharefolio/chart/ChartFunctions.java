package com.kydarun.sharefolio.chart;

import java.util.Date;

public class ChartFunctions {
	@SuppressWarnings("deprecation")
	public static String convertDateToString(Date date) {
		StringImploder sI = new StringImploder();
		sI.addNewString("" + date.getYear());
		sI.addNewString("" + date.getMonth());
		sI.addNewString("" + date.getDate());

		String returnString = "new Date(" + sI + ")";
		
		return returnString;
	}
}
