package com.kydarun.sharefolio.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.obj.Comment;
import com.kydarun.sharefolio.obj.Company;

import com.kydarun.sharefolio.obj.Wall;
import com.kydarun.sharefolio.util.ExpendableTextView;

import com.kydarun.sharefolio.obj.Price;


public class DBHelper extends SQLiteOpenHelper {
	private static final int DATABASE_VERSION = 2;
	private static final String DATABASE_NAME = "sharefoliomobile";
	private static final String TABLE_FRIEND = "friend";
	private static final String TABLE_COMPANY = "company";
	private static final String TABLE_ACCOUNTWALLPOST = "accountwallpost";
	
	private static final String KEY_FRIEND_ID = "friend_id";
	private static final String KEY_FRIEND_FIRSTNAME = "friend_firstname";
	private static final String KEY_FRIEND_AVATAR = "friend_avatar";
	
	private static final String KEY_COMPANY_ID = "company_id";
	private static final String KEY_COMPANY_NAME = "company_name";
	private static final String KEY_COMPANY_CODE = "company_code";
	private static final String KEY_COMPANY_AVATAR = "company_avatar";
	private static final String KEY_COMPANY_PRICE_REF = "price_ref";
	private static final String KEY_COMPANY_PRICE_LAST = "price_last";
	private static final String KEY_COMPANY_PRICE_CHANGE = "price_change";
	private static final String KEY_COMPANY_PRICE_VOLUME = "price_volume";
	private static final String KEY_COMPANY_WALLPOST_GAP = "wallpost_gap";
	private static final String KEY_COMPANY_WALLPOST_WALL_CONTENT = "wallpost_wallContent";
	private static final String KEY_COMPANY_WALLPOST_ANNOUNCEMENT_TITLE = "wallpost_announcementTitle";
	
	private static final String KEY_WALL_ID = "wall_id";
	private static final String KEY_WALL_POSTER = "wall_Poster";
	private static final String KEY_WALL_POST = "wall_Post";
	private static final String KEY_WALL_POSTER_COMPANY_ANNOUNCE_TITLE = "wall_posterCompanyAnnouncementTitle";
	private static final String KEY_WALL_POSTER_COMPANY_ANNOUNCE_LINK = "wall_posterCompanyAnnouncementLink";
	private static final String KEY_WALL_TARGET_NAME = "wall_targetName";
	private static final String KEY_WALL_NUMBER_LIKES = "wall_numberOfLike";
	private static final String KEY_WALL_GAP = "wall_gap";
	
	private static final String[] FRIEND_COLUMNS = {KEY_FRIEND_ID, KEY_FRIEND_FIRSTNAME, KEY_FRIEND_AVATAR};

	private static final String[] WALL_POST_COLUMNS = {KEY_WALL_ID, KEY_WALL_POSTER, KEY_WALL_POST, KEY_WALL_POSTER_COMPANY_ANNOUNCE_TITLE, 
														KEY_WALL_POSTER_COMPANY_ANNOUNCE_LINK, KEY_WALL_TARGET_NAME, KEY_WALL_NUMBER_LIKES, KEY_WALL_GAP};   
	

	private static final String[] COMPANY_COLUMNS = {KEY_COMPANY_ID, KEY_COMPANY_NAME, KEY_COMPANY_CODE, KEY_COMPANY_AVATAR, 
		KEY_COMPANY_PRICE_REF, KEY_COMPANY_PRICE_LAST, KEY_COMPANY_PRICE_CHANGE, KEY_COMPANY_PRICE_VOLUME,
		KEY_COMPANY_WALLPOST_GAP, KEY_COMPANY_WALLPOST_WALL_CONTENT, KEY_COMPANY_WALLPOST_ANNOUNCEMENT_TITLE};

	
	private static DBHelper sDBHelper;
	
	public static DBHelper get(Context context) {
		if (sDBHelper == null) {
			sDBHelper = new DBHelper(context);
		}
		return sDBHelper;
	}
	
	private DBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		String CREATE_FRIEND_TABLE = "CREATE TABLE " + TABLE_FRIEND + " ( "
				+ KEY_FRIEND_ID + " INTEGER PRIMARY KEY, "
				+ KEY_FRIEND_FIRSTNAME + " TEXT, "
				+ KEY_FRIEND_AVATAR + " TEXT )";
		
		String CREATE_COMPANY_TABLE = "CREATE TABLE " + TABLE_COMPANY + " ( "
				+ KEY_COMPANY_ID + " TEXT PRIMARY KEY, "
				+ KEY_COMPANY_NAME + " TEXT, "
				+ KEY_COMPANY_CODE + " TEXT, "
				+ KEY_COMPANY_AVATAR + " TEXT, "
				+ KEY_COMPANY_PRICE_REF + " TEXT, "
				+ KEY_COMPANY_PRICE_LAST + " TEXT, "
				+ KEY_COMPANY_PRICE_CHANGE + " TEXT, "
				+ KEY_COMPANY_PRICE_VOLUME + " TEXT, "
				+ KEY_COMPANY_WALLPOST_GAP + " TEXT, "
				+ KEY_COMPANY_WALLPOST_WALL_CONTENT + " TEXT, "
				+ KEY_COMPANY_WALLPOST_ANNOUNCEMENT_TITLE + " TEXT )";
		
		String CREATE_ACCOUNTWALLPOST_TABLE = "CREATE TABLE " + TABLE_ACCOUNTWALLPOST+ " ( "
				+ KEY_WALL_ID  + " TEXT PRIMARY KEY, "
				+ KEY_WALL_POSTER + " TEXT, "
				+ KEY_WALL_POST + " TEXT, "
				+ KEY_WALL_POSTER_COMPANY_ANNOUNCE_TITLE + " TEXT, "
				+ KEY_WALL_POSTER_COMPANY_ANNOUNCE_LINK + " TEXT, "
				+ KEY_WALL_TARGET_NAME + " TEXT, " 
				+ KEY_WALL_NUMBER_LIKES + " TEXT, "
				+ KEY_WALL_GAP + " TEXT )";
		
		db.execSQL(CREATE_FRIEND_TABLE);
		db.execSQL(CREATE_COMPANY_TABLE);
		db.execSQL(CREATE_ACCOUNTWALLPOST_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("ALTER TABLE " + TABLE_COMPANY + " ADD COLUMN " + KEY_COMPANY_PRICE_REF + " TEXT");
		db.execSQL("ALTER TABLE " + TABLE_COMPANY + " ADD COLUMN " + KEY_COMPANY_PRICE_LAST + " TEXT");
		db.execSQL("ALTER TABLE " + TABLE_COMPANY + " ADD COLUMN " + KEY_COMPANY_PRICE_CHANGE + " TEXT");
		db.execSQL("ALTER TABLE " + TABLE_COMPANY + " ADD COLUMN " + KEY_COMPANY_PRICE_VOLUME + " TEXT");
		db.execSQL("ALTER TABLE " + TABLE_COMPANY + " ADD COLUMN " + KEY_COMPANY_WALLPOST_GAP + " TEXT");
		db.execSQL("ALTER TABLE " + TABLE_COMPANY + " ADD COLUMN " + KEY_COMPANY_WALLPOST_WALL_CONTENT + " TEXT");
		db.execSQL("ALTER TABLE " + TABLE_COMPANY + " ADD COLUMN " + KEY_COMPANY_WALLPOST_ANNOUNCEMENT_TITLE + " TEXT");
		
		this.onCreate(db);
	}
	
	public void addFriend(Account friend) {
		Log.d("addFriend", friend.toString());
		
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_FRIEND_ID, friend.ID);
		values.put(KEY_FRIEND_FIRSTNAME, friend.firstname);
		values.put(KEY_FRIEND_AVATAR, friend.avatar);
		
		db.insert(TABLE_FRIEND, null, values);
		db.close();
	}
	
	public void addCompany(Company company) {
		Log.d("addCompany", company.toString());
		
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_COMPANY_ID, company.id);
		values.put(KEY_COMPANY_NAME, company.companyName);
		values.put(KEY_COMPANY_CODE, company.companyCode);
		values.put(KEY_COMPANY_AVATAR, company.avatar);
		values.put(KEY_COMPANY_PRICE_REF, company.price.ref);
		values.put(KEY_COMPANY_PRICE_LAST, company.price.last);
		values.put(KEY_COMPANY_PRICE_CHANGE, company.price.stockChange);
		values.put(KEY_COMPANY_PRICE_VOLUME, company.price.volume);
		values.put(KEY_COMPANY_WALLPOST_GAP, company.wallpost == null || company.wallpost.size() == 0 ? "0" : company.wallpost.get(0).gap + "");
		values.put(KEY_COMPANY_WALLPOST_WALL_CONTENT, company.wallpost == null || company.wallpost.size() == 0 ? "" : company.wallpost.get(0).wallContent);
		values.put(KEY_COMPANY_WALLPOST_ANNOUNCEMENT_TITLE, company.wallpost == null || company.wallpost.size() == 0 ? "" : company.wallpost.get(0).announcementTitle);
		
		db.insert(TABLE_COMPANY, null, values);
		db.close();
	}
	
/*	public void addWallpost(Wall accountwallpost) {
		Log.d("addWallPost", accountwallpost.toString());
		
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		
		values.put(KEY_WALL_ID, accountwallpost.wallID);
		values.put(KEY_WALL_POSTER, (accountwallpost.poster).toString());
		values.put(KEY_WALL_POST, accountwallpost.wallContent);
		values.put(KEY_WALL_POSTER_COMPANY_ANNOUNCE_TITLE, accountwallpost.announcementTitle);
		values.put(KEY_WALL_POSTER_COMPANY_ANNOUNCE_LINK, accountwallpost.announcementLink);
		values.put(KEY_WALL_TARGET_NAME, (accountwallpost.target).toString());
		values.put(KEY_WALL_NUMBER_LIKES, accountwallpost.userLike);
		values.put(KEY_WALL_GAP, accountwallpost.gap +"");
		
		db.insert(TABLE_ACCOUNTWALLPOST, null, values);
		db.close();
		
	}*/
	public Account getFriend(int id) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(TABLE_FRIEND, 
				FRIEND_COLUMNS,
				KEY_FRIEND_ID + " = ?",	//maybe something wrong here
				new String[] {String.valueOf(id)}, 
				null,
				null,
				null);
		
		if (cursor != null) {
			cursor.moveToFirst();
		}
		Account friend = new Account();
		friend.ID = cursor.getLong(0);
		friend.firstname = cursor.getString(1);
		friend.avatar = cursor.getString(2);
		
		Log.d("getFriend:", friend.toString());
		
		return friend;
	}
	
	public Company getCompany(String id) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(TABLE_COMPANY, 
				COMPANY_COLUMNS, 
				KEY_COMPANY_ID + " = ?",	//maybe something wrong here 
				new String[] {id},
				null,
				null, 
				null);
		
		if (cursor != null) {
			cursor.moveToFirst();
		}
		Company company = new Company();
		company.id = cursor.getString(0);
		company.companyName = cursor.getString(1);
		company.companyCode = cursor.getString(2);
		company.avatar = cursor.getString(3);
		company.price = new Price();
		company.price.ref = cursor.getString(4);
		company.price.last = cursor.getString(5);
		company.price.stockChange = cursor.getString(6);
		company.price.volume = cursor.getString(7);
		company.wallpost = new ArrayList<Wall>();
		Wall post = new Wall();
		post.gap = Long.parseLong(cursor.getString(8));
		post.wallContent = cursor.getString(9);
		post.announcementTitle = cursor.getString(10);
		company.wallpost.add(post);
		
		Log.d("getCompany:", company.toString());
		
		return company;
	}
	
/*	public Wall getWall(int id) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(TABLE_ACCOUNTWALLPOST, 
				WALL_POST_COLUMNS, 
				KEY_WALL_ID + " = ?",	//maybe something wrong here 
				new String[] {String.valueOf(id)}, 
				null,
				null, 
				null,
				null);
		
		if (cursor != null) {
			cursor.moveToFirst();
		}
		Wall wallpost = new Wall();
		wallpost.wallID = Long.parseLong(cursor.getString(0));
		wallpost.poster = (Account)(Object) cursor.getString(1);
		wallpost.wallContent= cursor.getString(2);
		wallpost.announcementTitle = cursor.getString(3);
		wallpost.announcementLink = cursor.getString(4);
		wallpost.target = (Account) (Object) cursor.getString(5);
		wallpost.userLike = cursor.getString(6);
		wallpost.gap = Long.parseLong(cursor.getString(7));
		wallpost.personTagged = new ArrayList<Account>();
		wallpost.companyTagged = new ArrayList<Company>();
		Company company = new Company();
		company.id = cursor.getString(8);
		company.companyCode = cursor.getString(9);
		company.companyName = cursor.getString(10);
		wallpost.companyTagged.add(company);
		
		Log.d("getWall:", wallpost.toString());
		 	
		return wallpost;
	}
	*/
	public void deleteCompany(Company company) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_COMPANY,
			KEY_COMPANY_ID + " = ?",
			new String[] {company.id});
	}
	
	public List<Account> getAllFriends() {
		List<Account> friends = new ArrayList<Account>();
		
		String query = "SELECT * FROM " + TABLE_FRIEND;
		
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(query, null);
		
		Account friend = null;
		if (cursor.moveToFirst()) {
			do {
				friend = new Account();
				friend.ID = cursor.getLong(0);
				friend.firstname = cursor.getString(1);
				friend.avatar = cursor.getString(2);
				friends.add(friend);
			} while(cursor.moveToNext());
		}
		
		Log.d("getAllFriends", friends.toString());
		return friends;
	}
	
	public List<Company> getAllCompanies() {
		List<Company> companies = new ArrayList<Company>();
		
		String query = "SELECT * FROM " + TABLE_COMPANY;
		
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(query, null);
		
		Company company = null;	
		if (cursor.moveToFirst()) {
			do {
				company = new Company();
				company.id = cursor.getString(0);
				company.companyName = cursor.getString(1);
				company.companyCode = cursor.getString(2);
				company.avatar = cursor.getString(3);
				company.price = new Price();
				company.price.ref = cursor.getString(4);
				company.price.last = cursor.getString(5);
				company.price.stockChange = cursor.getString(6);
				company.price.volume = cursor.getString(7);
				company.wallpost = new ArrayList<Wall>();
				Wall post = new Wall();
				post.gap = Long.parseLong(cursor.getString(8));
				post.wallContent = cursor.getString(9);
				post.announcementTitle = cursor.getString(10);
				company.wallpost.add(post);
				companies.add(company);
			} while(cursor.moveToNext());
		}
		
		Log.d("getAllCompanies" , companies.toString());
		return companies;
	}

	/*public List<Wall> getAllWallPosts() {
		List<Wall> wallposts = new ArrayList<Wall>();
		
		String query = "SELECT * FROM " + TABLE_ACCOUNTWALLPOST;
		
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(query, null);
		
		Wall wallpost = null;	
		if (cursor.moveToFirst()) {
			do {
				wallpost = new Wall();
				wallpost.wallID = Long.parseLong(cursor.getString(0));
				wallpost.poster = (Account)(Object) cursor.getString(1);
				wallpost.wallContent= cursor.getString(2);
				wallpost.announcementTitle = cursor.getString(3);
				wallpost.announcementLink = cursor.getString(4);
				wallpost.target = (Account) (Object) cursor.getString(5);
				wallpost.userLike = cursor.getString(6);
				wallpost.gap = Long.parseLong(cursor.getString(7));
				wallpost.personTagged = new ArrayList<Account>();
				wallpost.companyTagged = new ArrayList<Company>();
				Company company = new Company();
				company.id = cursor.getString(8);
				company.companyCode = cursor.getString(9);
				company.companyName = cursor.getString(10);
				wallpost.companyTagged.add(company);
				wallposts.add(wallpost);
			} while(cursor.moveToNext());
		}
		
		Log.d("getAllWallPosts" , wallposts.toString());
		return wallposts;
	}*/
}
