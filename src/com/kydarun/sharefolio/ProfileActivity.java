package com.kydarun.sharefolio;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.kydarun.sharefolio.adapter.GroupSearchAdapter;
import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.api.UserApi;
import com.kydarun.sharefolio.database.DBHelper;
import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.obj.Company;
import com.kydarun.sharefolio.util.Common;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("InflateParams")
public class ProfileActivity extends BaseActivity implements OnClickListener {
	Account account;
	
	TextView tvProfileName;
	ImageView ivProfilePicture;
	HorizontalScrollView hsvCompanyList;
	HorizontalScrollView hsvFriends;
	ListView lvProfileGroupList;
	GroupSearchAdapter adapter;
	Button btnFriendToggle;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profile);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getFragmentManager().beginTransaction().add(R.id.rlHeader, header).commit();
		
		account = (Account)getIntent().getExtras().getSerializable("Account");
		DBHelper.get(context).addFriend(account);
		
		if (account != null) {
			super.setTitle(account.firstname);
		}
		
		tvProfileName = (TextView)findViewById(R.id.tvProfileName);
		if (tvProfileName != null) {
			tvProfileName.setText(account.firstname);
		}
		ivProfilePicture = (ImageView)findViewById(R.id.ivProfilePicture);
		if (ivProfilePicture != null) {
			UrlImageViewHelper.setUrlDrawable(ivProfilePicture, 
					context.getResources().getString(R.string.url_avatar) + account.ID + account.firstname, 
					R.drawable.ic_launcher);
		}
		hsvCompanyList = (HorizontalScrollView)findViewById(R.id.hsvCompanyList);
		hsvFriends = (HorizontalScrollView)findViewById(R.id.hsvFriends);
		lvProfileGroupList = (ListView)findViewById(R.id.lvProfileGroupList);
		
		btnFriendToggle = (Button)findViewById(R.id.btnFriendToggle);
		if (btnFriendToggle != null) {
			if (account.ID.equals(sm.getCurrentSession().ID)) {
				btnFriendToggle.setVisibility(View.GONE);
			}
			else if (account.isFriend) {
				btnFriendToggle.setText("Unfriend " + account.firstname);
				btnFriendToggle.setOnClickListener(this);
			}
			else if  (account.requestPending){
				btnFriendToggle.setText("Request Pending");
				btnFriendToggle.setEnabled(false);
			}
			else if  (account.requestSent){
				btnFriendToggle.setText("Request Sent");
				btnFriendToggle.setEnabled(false);
			}
			else {
				btnFriendToggle.setText("Add Friend");
				btnFriendToggle.setOnClickListener(this);
			}
		}
		
		new LoadProfileAsyncTask().execute();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
		
	}

	class LoadProfileAsyncTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (account != null) {
				if (hsvCompanyList != null) {
					for (final Company c : account.companies) {
						View v = LayoutInflater.from(context).inflate(R.layout.horizontal_item, null);
						Button btnLink = (Button)v.findViewById(R.id.btnLink);
						btnLink.setTypeface(Common.getFontRegular(context));
						btnLink.setTextColor(R.color.black);
						btnLink.setText(c.companyCode);
						btnLink.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
								Intent i = new Intent(context, CompanyActivity.class);
								Bundle b = new Bundle();
								b.putSerializable("company", c);
								i.putExtras(b);
								startActivity(i);
							}
						});
						((LinearLayout)hsvCompanyList.getChildAt(0)).addView(v);
					}
				}
				if (hsvFriends != null) {
					for (final Account a : account.friends) {
						View v = LayoutInflater.from(context).inflate(R.layout.horizontal_friend_item, null);
						ImageView ivFriendPhoto = (ImageView)v.findViewById(R.id.ivFriendPhoto);
						UrlImageViewHelper.setUrlDrawable(ivFriendPhoto, 
								context.getResources().getString(R.string.url_avatar) + a.ID + a.avatar);
						TextView tvFriendName = (TextView)v.findViewById(R.id.tvFriendName);
						tvFriendName.setTypeface(Common.getFontRegular(context));
						tvFriendName.setTextColor(R.color.black);
						tvFriendName.setText(a.firstname);
						v.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
								Intent i = new Intent(context, ProfileActivity.class);
								Bundle b = new Bundle();
								b.putSerializable("Account", a);
								i.putExtras(b);
								startActivity(i);
							}
						});
						((LinearLayout)hsvFriends.getChildAt(0)).addView(v);
					}
				}
				if (lvProfileGroupList != null) {
					adapter = new GroupSearchAdapter(context, account.groups);
					lvProfileGroupList.setAdapter(adapter);
				}
			}
			else {
				Toast.makeText(context, "Profile failed to load", Toast.LENGTH_SHORT).show();
			}
		}

		@Override
		protected Void doInBackground(Void... params) {
			UserApi api = new UserApi(context);
			try {
				account = (Account)api.getUserInfo(sm.getCurrentSession().ID, account.ID);
				account.friends = api.getFriendsList(account.ID, 0, 10);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
	}

	@Override
	public void onClick(View v) {
			
			if(btnFriendToggle.getText().toString().equals("Add Friend"))
			{
				new AddFriendAsyncTask().execute();
			}
			
			if(btnFriendToggle.getText().toString().equals("Unfriend " + account.firstname))
			{
				new UnFriendAsyncTask().execute();
				
			}
	
		}
	
	
	class AddFriendAsyncTask extends AsyncTask<Void, Void, Boolean> {
	
		@Override
		protected Boolean doInBackground(Void... arg0) {
			
			UserApi api = new UserApi(context);
			try {
				return api.sendFriendRequest(sm.getCurrentSession().ID, account);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return false;
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			
			if (result) {
				btnFriendToggle.setText("Request Sent");
				btnFriendToggle.setEnabled(false);
			}
		}
	}
	
	class UnFriendAsyncTask extends AsyncTask<Void, Void, Boolean> {
		
		@Override
		protected Boolean doInBackground(Void... arg0) {
			
			UserApi api = new UserApi(context);
			try {
				return api.deleteFriend(sm.getCurrentSession().ID, account.ID);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return false;
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			
			if (result) {
				
				btnFriendToggle.setText("Add Friend");
				btnFriendToggle.setOnClickListener((OnClickListener) this);
			}
		}
	}

}
