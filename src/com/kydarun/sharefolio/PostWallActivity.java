package com.kydarun.sharefolio;

import java.util.ArrayList;
import java.util.List;


import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListPopupWindow;
import android.widget.Toast;

import com.kydarun.sharefolio.api.WallApi;
import com.kydarun.sharefolio.database.DBHelper;
import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.obj.Company;
import com.kydarun.sharefolio.obj.Wall;
import com.kydarun.sharefolio.util.SuggestionList;

public class PostWallActivity  extends BaseActivity {
	EditText wallContent_editText;
	Button post_button;
	String wallContent;
	WallApi wa;
	Wall wall;
	ListPopupWindow popupWindow;
	
	List<Account> friends = new ArrayList<Account>();
	List<Company> companies = new ArrayList<Company>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		friends = DBHelper.get(context).getAllFriends();
		companies = DBHelper.get(context).getAllCompanies();
		
		setContentView(R.layout.activity_post_wall);
		wallContent_editText = (EditText) findViewById(R.id.etPostWall);
		post_button = (Button) findViewById(R.id.bPostWall);
		
		popupWindow = new ListPopupWindow(context);
		popupWindow.setAnchorView(wallContent_editText);
		popupWindow.setInputMethodMode(ListPopupWindow.INPUT_METHOD_NEEDED);
		
		wallContent_editText.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				SuggestionList.loadSuggestionList(context, popupWindow, s, friends, companies, wallContent_editText);
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		post_button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String wallContent = wallContent_editText.getText().toString();
				
				if (!isEmpty(wallContent)) {
					post_button.setEnabled(false);
					wallContent_editText.setEnabled(false);
					
					wall = new Wall();
					wall.wallContent = wallContent;
					wall.poster = sm.getCurrentSession();
					wall.personTagged = SuggestionList.loadFinalFriendsTag(wallContent);
					wall.companyTagged = SuggestionList.loadFinalCompaniesTag(wallContent);
					
					if (wall.personTagged.size() != 0) {
						for (int i = 0 ; i < wall.personTagged.size() ; i++) {
							if (wallContent.contains(wall.personTagged.get(i).firstname)) {
								wallContent = wallContent.replace(wall.personTagged.get(i).firstname, wall.personTagged.get(i).ID.toString());
							}
						}
					}
					
					if (wall.companyTagged.size() != 0) {
						for (int i = 0 ; i < wall.companyTagged.size() ; i++) {
							if (wallContent.contains(wall.companyTagged.get(i).companyCode)) {
								wallContent = wallContent.replace(wall.companyTagged.get(i).companyCode, wall.companyTagged.get(i).id);
							}
						}
					}
					
					wall.wallContent = wallContent;
					
					wa = new WallApi(PostWallActivity.this);
					new PostOnWallTask().execute();	
				} else {
					Toast.makeText(context, "Please enter your wall content", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	public static boolean isEmpty(String wallContent) {
		if (wallContent.trim().length() != 0) {
			return false;
		} else {
			return true;
		}
	}
	
	public class PostOnWallTask extends AsyncTask<Void, Void, Void> {
		Wall insideWall;

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				insideWall = wa.postOnWall(wall);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			post_button.setEnabled(true);
			wallContent_editText.setEnabled(true);
			finish();
		}
	}
}
