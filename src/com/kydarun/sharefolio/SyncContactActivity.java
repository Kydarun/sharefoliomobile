package com.kydarun.sharefolio;

import java.util.ArrayList;
import java.util.List;

import com.kydarun.sharefolio.adapter.SyncContactAdapter;
import com.kydarun.sharefolio.api.UserApi;
import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.util.Common;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class SyncContactActivity extends BaseActivity implements OnClickListener {
	List<String> phoneNumber = new ArrayList<String>();
	ListView lvContactListUser;
	SyncContactAdapter adapter;
	List<Account> accounts;
	
	boolean isCreated = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sync_contact);
		TextView tvFindPeople = (TextView)findViewById(R.id.tvFindPeople);
		tvFindPeople.setTypeface(Common.getFontRegular(context));
		
		try {
			ContentResolver cr = context.getContentResolver();
		    Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
		    if(cursor.moveToFirst())
		    {
		        do
		        {
		            String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));

		            if(Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0)
		            {
		                Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",new String[]{ id }, null);
		                while (pCur.moveToNext()) 
		                {
		                    String contactNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
		                    phoneNumber.add(contactNumber);
		                    break;
		                }
		                pCur.close();
		            }

		        } while (cursor.moveToNext()) ;
		    }
	        
	        if (cursor != null) {
	        	cursor.close();
	        }
	        
	        new GetUserViaContactAsyncTask().execute();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		lvContactListUser = (ListView)findViewById(R.id.lvContactListUser);
		
		Button btnSkipToHome = (Button)findViewById(R.id.btnToHome);
		if (btnSkipToHome != null) {
			btnSkipToHome.setOnClickListener(this);
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		if (isCreated) {
			finish();
		}
	}

	class GetUserViaContactAsyncTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			
			adapter = new SyncContactAdapter(context, accounts);
			lvContactListUser.setAdapter(adapter);
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			UserApi api = new UserApi(context);
			try {
				accounts = api.getUserViaContact(sm.getCurrentSession().ID, phoneNumber);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
		
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btnToHome) {
			Intent intent = new Intent(context, MainActivity.class);
			startActivity(intent);
			
			isCreated = true;
		}
	}
}