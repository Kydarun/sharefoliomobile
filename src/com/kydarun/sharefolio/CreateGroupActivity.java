package com.kydarun.sharefolio;

import com.kydarun.sharefolio.adapter.GroupSearchPeopleAdapter;
import com.kydarun.sharefolio.api.GroupApi;
import com.kydarun.sharefolio.constant.PageNameConstant;
import com.kydarun.sharefolio.obj.Group;
import com.kydarun.sharefolio.util.Common;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class CreateGroupActivity extends BaseActivity implements OnClickListener {
	EditText etNewGroupName;
	EditText etSearchPeople;
	Switch swGroupSetting;
	Button btnCreateGroup;
	Button btnSearchPeople;
	Group group;
	RelativeLayout rlAddPeople;
	GroupSearchPeopleAdapter adapter;
	ListView lvSearchResult;
	
	boolean isCreated = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.create_group);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		getFragmentManager().beginTransaction().add(R.id.rlHeader, header).commit();
		
		super.setTitle("Create Group");
		
		TextView tvCreateNewGroup = (TextView)findViewById(R.id.tvCreateNewGroup);
		if (tvCreateNewGroup != null) {
			tvCreateNewGroup.setTypeface(Common.getFontRegular(context));
		}
		
		TextView tvEnterGroupName = (TextView)findViewById(R.id.tvEnterGroupName);
		if (tvEnterGroupName != null) {
			tvEnterGroupName.setTypeface(Common.getFontRegular(context));
		}
		
		etNewGroupName = (EditText)findViewById(R.id.etNewGroupName);
		etSearchPeople = (EditText)findViewById(R.id.etSeachPeople);
		swGroupSetting = (Switch)findViewById(R.id.swGroupSetting);
		btnCreateGroup = (Button)findViewById(R.id.btnCreateGroup);
		btnSearchPeople = (Button)findViewById(R.id.btnSearchPeople);
		rlAddPeople = (RelativeLayout)findViewById(R.id.rlAddPeople);
		lvSearchResult = (ListView)findViewById(R.id.lvSearchResult);
		rlAddPeople.setVisibility(View.INVISIBLE);
		
		btnCreateGroup.setOnClickListener(this);
		btnSearchPeople.setOnClickListener(this);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		if (isCreated) {
			finish();
		}
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btnCreateGroup) {
			if (etNewGroupName.getText().toString().equals("")) {
				Toast.makeText(context, "Please enter group name", Toast.LENGTH_SHORT).show();
			}
			else {
				progressDialog = ProgressDialog.show(context, context.getResources().getText(R.string.app_name), "Creating group...");
				new CreateGroupAsyncTask().execute();
			}
		}
	}
	
	class CreateGroupAsyncTask extends AsyncTask<Void, Void, Boolean> {

		// check if group has been created, if has been created, return to group list wt notification, else create group and return.
		@Override
		protected Boolean doInBackground(Void... arg0) {
			GroupApi api = new GroupApi(context);
			Group g = new Group();
			g.groupname = etNewGroupName.getText().toString();
			g.createdby = sm.getCurrentSession();
			g.grouptype = swGroupSetting.isChecked() ? "C" : "S";
			try {
				group = api.createGroup(sm.getCurrentSession().ID, g);
				return true;
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			if (result)
			{
				Intent in = new Intent(context, SearchActivity.class);
				Bundle b = new Bundle();
				b.putSerializable("Group", group);
				b.putString("PageName", PageNameConstant.HOME_PAGE);
				in.putExtras(b);
				isCreated = true;
				startActivity(in);
			}
			else {
				Toast.makeText(context, "Failed to create group", Toast.LENGTH_SHORT);
			}
			
			progressDialog.dismiss();
		}
	}
}
