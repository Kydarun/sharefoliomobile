package com.kydarun.sharefolio;

import java.util.ArrayList;
import java.util.List;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.kydarun.sharefolio.adapter.FriendRequestAdapter;
import com.kydarun.sharefolio.api.UserApi;
import com.kydarun.sharefolio.obj.Request;

public class FriendRequestActivity extends BaseActivity {
	ListView lvFriendRequest;
	FriendRequestAdapter adapter;
	List<Request> requests = new ArrayList<Request>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_friend_request);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		adapter = new FriendRequestAdapter(context, requests);
		flProgress = (FrameLayout)findViewById(R.id.flProgress);
		new GetFriendRequestTask().execute();
		
		lvFriendRequest = (ListView) findViewById(R.id.lvFriendRequest);
		lvFriendRequest.setAdapter(adapter);
		
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private class GetFriendRequestTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			UserApi ua = new UserApi(FriendRequestActivity.this);
			try {
				requests.addAll(requests = ua.getFriendRequest(sm.getCurrentSession().ID, 0, 20));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			adapter.notifyDataSetChanged();
			flProgress.setVisibility(View.GONE);
		}
	}
}
