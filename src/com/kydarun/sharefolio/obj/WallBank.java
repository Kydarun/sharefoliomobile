package com.kydarun.sharefolio.obj;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.costum.android.widget.PullAndLoadListView;
import com.kydarun.sharefolio.adapter.WallAdapter;
import com.kydarun.sharefolio.api.WallApi;

public class WallBank {
	private static WallBank sWallBank;
	private Context mContext;
	private ArrayList<Wall> mWalls;
	private WallApi wa;
	private Account mMyAccount;
	private final int numberOfResult = 10;
	private int mStarting = 0;
	private WallAdapter mAdapter;
	PullAndLoadListView mHomePageListView;
	
	private WallBank(Context context) {
		mContext = context;
		mWalls = new ArrayList<Wall>();
		wa = new WallApi(mContext);
	}
	
	public static WallBank get(Context c) {
		if (sWallBank == null) {
			sWallBank = new WallBank(c);
		}
		return sWallBank;
	}
	
	public ArrayList<Wall> getWalls() {
		return mWalls;
	}
	
	public List<Account> getLikersFromWall(long wallID) {
		for (Wall wall : mWalls) {
			if (wall.wallID.equals(wallID)) {
				return wall.like;
			}
		}
		return null;
	}
	
	public void updateWall(Wall wall, long wallID) {
		for (int i = 0 ; i < mWalls.size() ; i++) {
			if (mWalls.get(i).wallID.equals(wallID)) {
				mWalls.set(i, wall);
				break;
			}
		}
		mAdapter.notifyDataSetChanged();
	}
	
	public void setupWallBank(Account myAccount, WallAdapter adapter, PullAndLoadListView homePageListView) {
		mMyAccount = myAccount;
		mAdapter = adapter;
		mHomePageListView = homePageListView;
		new LoadMoreWallTask().execute();
	}
	
	public void onLoadMoreWall() {
		mStarting += 10;
		new LoadMoreWallTask().execute();
	}
	
	public void onRefreshWall() {
		mStarting = 0;
		new RefreshWallTask().execute();
	}
	
	private class LoadMoreWallTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub	
			if (isCancelled()) {
				return null;
			}
			try {
				mWalls.addAll(wa.loadHome(mMyAccount.ID, mStarting, numberOfResult));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			mAdapter.notifyDataSetChanged();
			Log.d("mWalls size: ",  "" + mWalls.size());
			mHomePageListView.onLoadMoreComplete();
		}
		
		protected void onCancelled() {
			// Notify the loading more operation has finished
			mHomePageListView.onLoadMoreComplete();
		}	
	}
	
	private class RefreshWallTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			if (isCancelled()) {
				return null;
			}
			mWalls.clear();
			try {
				mWalls.addAll(wa.loadHome(mMyAccount.ID, mStarting, numberOfResult));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			mAdapter.notifyDataSetChanged();
			mHomePageListView.onRefreshComplete();
		}
		
		protected void onCancelled() {
			// Notify the loading more operation has finished
			mHomePageListView.onLoadMoreComplete();
		}	
	}
	
}
