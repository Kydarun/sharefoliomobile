package com.kydarun.sharefolio.obj;

import java.io.Serializable;
import java.util.List;

public class Comment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3819169935365815694L;

	public User user;
	public Long commentID;
	public String datePosted;
	public Long gap;
	public String message;
	public List<Account> like;
	public String lastEditedDate;
	public String attachment;
	public String userLike;
	public Boolean liked;
	public List<Account> personTagged;
	public List<Company> companyTagged;
	
	public Long wallID;
}
