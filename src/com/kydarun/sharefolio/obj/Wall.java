package com.kydarun.sharefolio.obj;

import java.io.Serializable;
import java.util.List;

public class Wall implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8295945334235562536L;

	public Long wallID;
	public String wallStamp;
	public String wallContent;
	public String userLike;
	public List<Account> like;
	public Integer wallType;
	public String announcementLink;
	public String announcementTitle;
	public String announcementDesc;
	public String linkTitle;
	public String linkUrl;
	public String linkDescription;
	public String linkVideo;
	public String linkImage;
	public String lastEditedDate;
	public String attachment;
	public Boolean featured;
	public String featuredDate;
	public Long gap;
	public Account poster;
	public Account target;
	public Company posterCompany;
	public Company targetCompany;
	public Boolean liked;
	public List<Account> personTagged;
	public List<Company> companyTagged;
	
	public List<Comment> comments;
}
