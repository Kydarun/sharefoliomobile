package com.kydarun.sharefolio.obj;

import java.io.Serializable;
import java.util.List;

public class Account extends User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3143472139906104192L;
	
	public Long ID;
	public String firstname;
	public String avatar;
	public String fb_id;
	public String phoneNumber;
	public String verificationCode;
	public String registration_id;
	public String platformEndpointArn;
	
	public List<Account> friends;
	public List<Company> companies;
	public List<Group> groups;
	
	public boolean isFriend;
	public boolean requestSent;
	public boolean requestPending;
	
	public String toString(){
		return "Friend [id=" + ID + ", firstname=" + firstname + ", avatar=" + avatar + "]";
	}
}
