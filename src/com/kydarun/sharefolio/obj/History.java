package com.kydarun.sharefolio.obj;

import java.io.Serializable;

public class History implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4206001245242233763L;

	public String value;
	public String volume;
	public String date;
	
	public String id;
}
