package com.kydarun.sharefolio.obj;

import java.io.Serializable;
import java.util.List;

public class GroupMessage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4984152608583532278L;

	public Long groupchatid;
	public Account fromUser;
	public String timestamp;
	public Long gap;
	public String attachment;
	public String message;
	public List<Company> companyTagged;
	public Long groupid;
	public String uuid;
}
