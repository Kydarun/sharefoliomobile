package com.kydarun.sharefolio.obj;

import java.io.Serializable;
import java.util.List;

public class Group implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2418108275930687577L;

	public Long groupid;
	public String groupname;
	public String createddate;
	public String coverphoto;
	public String membercount;
	public Account createdby;
	public String grouptype;
	public boolean requestSent;
	
	public List<GroupMessage> messages;
	public List<Account> members;
	public List<GroupRequest> requests;
	public List<Company> companies;
}
