package com.kydarun.sharefolio.obj;

import java.io.Serializable;

public class Request implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5838694275893681111L;
	public Long id;
	public Account from;
	public String status;
}
