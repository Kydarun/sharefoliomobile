package com.kydarun.sharefolio.obj;

import java.io.Serializable;

public class Price implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3681341931428149552L;

	public String ref;
	public String high;
	public String low;
	public String last;
	public String stockChange;
	public String volume;
	
	public String id;
}
