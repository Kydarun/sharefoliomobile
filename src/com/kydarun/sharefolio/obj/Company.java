package com.kydarun.sharefolio.obj;

import java.io.Serializable;
import java.util.List;

public class Company extends User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8598447488057067508L;

	public String id;
	public String companyName;
	public String companyCode;
	public String avatar;
	
	public Price price;
	public List<History> history;
	public List<Wall> wallpost;
	
	public boolean isFollowing;
	
	public String toString() {
		return "Company [id=" + id 
				+ ", companyName=" + companyName 
				+ ", companyCode=" + companyCode 
				+ ", avatar=" + avatar + "]";
	}
}
