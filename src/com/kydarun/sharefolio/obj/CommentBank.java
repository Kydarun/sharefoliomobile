package com.kydarun.sharefolio.obj;

import java.util.ArrayList;
import java.util.Collections;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.costum.android.widget.PullAndLoadListView;
import com.kydarun.sharefolio.adapter.CommentAdapter;
import com.kydarun.sharefolio.api.WallApi;


public class CommentBank {
	private Context mContext;
	private ArrayList<Comment> mComments;
	private ArrayList<Comment> mLatestComments;
	private WallApi wa;
	private long mMyAccount;
	private long mWallID;
	public int starting = 0;
	private final int numberOfComment = 6;
	private CommentAdapter mAdapter;
	PullAndLoadListView mCommentListView;
	private FrameLayout flProgress;
	
	public CommentBank(Context context, long wallID, FrameLayout flProgress) {
		mContext = context;
		mWallID = wallID;
		mComments = new ArrayList<Comment>();
		mLatestComments = new ArrayList<Comment>();
		wa = new WallApi(mContext);
		this.flProgress = flProgress;
	}
	
	public void setupCommentBank(long myAccount, CommentAdapter adapter, PullAndLoadListView commentListView) {
		mMyAccount = myAccount;
		mAdapter = adapter;
		mCommentListView = commentListView;
		new LoadMoreCommentTask().execute();
	}
	
	public ArrayList<Comment> getComments() {
		return mLatestComments;
	}
	
	public void addComment(Comment latestComment) {
		mLatestComments.add(latestComment);
		mAdapter.notifyDataSetChanged();
	}
	
	public void onLoadMoreComment() {
		starting += 6;
		new LoadMoreCommentTask().execute();
		//mAdapter.notifyDataSetChanged();
	}
	
	private class LoadMoreCommentTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			if (isCancelled()) {
				return null;
			}
			mComments.clear();
			try {
				mComments.addAll(wa.loadComments(mMyAccount, mWallID, starting, numberOfComment));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Log.i("Exception", "Inside LoadMoreCommentTask: " + e.toString());
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub	
			Collections.reverse(mComments);
			mLatestComments.addAll(0, mComments);
			mAdapter.notifyDataSetChanged();
			mCommentListView.onRefreshComplete();
			flProgress.setVisibility(View.GONE);
		}	
		
		protected void onCancelled() {
			// Notify the loading more operation has finished
			mCommentListView.onLoadMoreComplete();
			flProgress.setVisibility(View.GONE);
		}
	}
	
	

}
