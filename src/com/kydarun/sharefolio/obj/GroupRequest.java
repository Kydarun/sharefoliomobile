package com.kydarun.sharefolio.obj;

import java.io.Serializable;

public class GroupRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2661651207492953901L;

	public long grouprequestid;
	public String requestdate;
	public Account from;
	
	public String groupid;
}
