package com.kydarun.sharefolio;

import com.kydarun.sharefolio.api.UserApi;
import com.kydarun.sharefolio.constant.NotificationConstant;
import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.util.Common;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class VerifyPhoneActivity extends BaseActivity implements OnClickListener {
	EditText etVerificationCode;
	Button btnSubmit;
	String phoneNumber;
	
	boolean isCreated = false;
	
	private BroadcastReceiver onEvent = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			Bundle b = intent.getExtras();
			
			if (b != null) {
				String code = b.getString("Code");
				if (etVerificationCode != null) {
					etVerificationCode.setText(code);
					new VerifyPhoneNumberAsyncTask().execute();
				}
			}
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.verify_phone);
		
		phoneNumber = getIntent().getExtras().getString("PhoneNumber");
		
		TextView txtEnterVerificationCode = (TextView)findViewById(R.id.txtEnterVerificationCode);
		if (txtEnterVerificationCode != null) {
			txtEnterVerificationCode.setTypeface(Common.getFontRegular(context));
		}
		
		etVerificationCode = (EditText)findViewById(R.id.etVerificationCode);
		btnSubmit = (Button)findViewById(R.id.btnPhoneSubmit);
		if (btnSubmit != null) {
			btnSubmit.setOnClickListener(this);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		IntentFilter f = new IntentFilter(NotificationConstant.SMS_NOTIFICATION);
		LocalBroadcastManager.getInstance(context).registerReceiver(onEvent, f);
		
		if (isCreated) {
			finish();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		
		LocalBroadcastManager.getInstance(context).unregisterReceiver(onEvent);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btnPhoneSubmit) {
			new VerifyPhoneNumberAsyncTask().execute();
		}
	}
	
	class VerifyPhoneNumberAsyncTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			progressDialog = ProgressDialog.show(context, context.getResources().getString(R.string.app_name), "Verifying...");
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			progressDialog.dismiss();
			if (result) {
				Account me = sm.getCurrentSession();
				me.phoneNumber = phoneNumber;
				me.verificationCode = etVerificationCode.getText().toString();
				sm.setSession(me);
				Intent intent = new Intent(VerifyPhoneActivity.this, SyncContactActivity.class);
				startActivity(intent);
				isCreated = true;
			}
			else {
				Toast.makeText(context, "Phone verification failed.", Toast.LENGTH_SHORT).show();
			}
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			UserApi api = new UserApi(context);
			Account me = sm.getCurrentSession();
			me.phoneNumber = phoneNumber;
			me.verificationCode = etVerificationCode.getText().toString();
			try {
				return api.verifyPhoneNumber(me);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return false;
		}
		
	}
}
