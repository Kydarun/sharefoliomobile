package com.kydarun.sharefolio;

import android.app.Fragment;

import com.kydarun.sharefolio.fragment.LikerFromWallFragment;
import com.kydarun.sharefolio.obj.Wall;

public class LikerFromWallActivity extends SingleFragmentActivity{

	@Override
	protected Fragment createFragment() {
		// TODO Auto-generated method stub
		Wall currentWall = (Wall) getIntent().getSerializableExtra(LikerFromWallFragment.EXTRA_CURRENT_WALL);	
		//Wall wallID = getIntent().getExtra(LikerFromWallFragment.EXTRA_CURRENT_WALL);
		
		return LikerFromWallFragment.newInstance(currentWall);
	}

}
