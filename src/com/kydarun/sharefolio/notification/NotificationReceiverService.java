package com.kydarun.sharefolio.notification;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.kydarun.sharefolio.MainActivity;
import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.constant.NotificationConstant;
import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.obj.Company;
import com.kydarun.sharefolio.obj.Group;
import com.kydarun.sharefolio.obj.Wall;
import com.kydarun.sharefolio.parser.GroupParser;
import com.kydarun.sharefolio.parser.UserParser;
import com.kydarun.sharefolio.parser.WallParser;
import com.kydarun.sharefolio.util.SharefolioJSONObject;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class NotificationReceiverService extends IntentService {
	GoogleCloudMessaging gcm;
	Context context;
	NotificationManager manager;
	
	public NotificationReceiverService() {
		super("NotificationReceiverService");
	}
	
	public static void postNotification(Intent intent, Context context) {
		NotificationManager manager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification n = new Notification.Builder(context).setSmallIcon(R.drawable.ic_launcher)
				.setContentTitle(context.getResources().getString(R.string.app_name))
				.build();
		manager.notify(context.getResources().getString(R.string.notification_tag), 1, n);
	}

	@Override
	public void onCreate() {
		super.onCreate();
		this.context = this;
		gcm = GoogleCloudMessaging.getInstance(this);
		manager = (NotificationManager)this.getSystemService(Context.NOTIFICATION_SERVICE);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle b = intent.getExtras();
		String messageType = gcm.getMessageType(intent);
		
		if (b != null && !b.isEmpty()) {
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
				sendNotification(intent, "Message error: " + b.getString("default"));
			}
			else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
				sendNotification(intent, "Message deleted: " + b.getString("default"));
			}
			else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
				Log.d(NotificationReceiverService.class.getSimpleName(), b.getString("default"));
				sendNotification(intent, b.getString("default"));
			}
		}
		
		NotificationReceiver.completeWakefulIntent(intent);
	}
	
	private void sendNotification(Intent intent, String message) {
		try {
			JSONObject obj = new JSONObject(message).getJSONObject("GCM").getJSONObject("data").getJSONObject("message");
			Notification.Builder builder = new Notification.Builder(this)
				.setSmallIcon(R.drawable.ic_launcher)
				.setContentTitle(context.getResources().getString(R.string.app_name))
				.setAutoCancel(true)
				.setContentText(obj.getString("notification"));
			Bundle b = new Bundle();
			b.putString("type",obj.getString("type"));
			if (obj.getString("type").equals(NotificationConstant.COMPANY)) {
				Company c = UserParser.parseWallpostCompany(new SharefolioJSONObject(obj.getJSONObject("response").toString()));
				b.putSerializable("company", c);
			}
			else if (obj.getString("type").equals(NotificationConstant.WALL) || obj.getString("type").equals(NotificationConstant.COMMENT)) {
				Wall w = WallParser.parseWall(new SharefolioJSONObject(obj.getJSONObject("response").toString()));
				b.putSerializable("Wall", w);
			}
			else if (obj.getString("type").equals(NotificationConstant.GROUP)) {
				Group g = GroupParser.parseGroup(new SharefolioJSONObject(obj.getJSONObject("response").toString()));
				if (g.messages != null && g.messages.size() != 0) {
					ActivityManager activity = (ActivityManager)this.getSystemService(ACTIVITY_SERVICE);
					RunningTaskInfo app = activity.getRunningTasks(1).get(0);
					PackageInfo info = getPackageManager().getPackageInfo(app.topActivity.getPackageName(), 0);
					Log.d(NotificationReceiverService.class.getSimpleName(), app.topActivity.getClassName());
					if (info.applicationInfo.packageName.toString().equals(getApplicationContext().getPackageName())
							&& app.topActivity.getClassName().equals("com.kydarun.sharefolio.ChatActivity")) {
						
						Intent broadcastIntent = new Intent(NotificationConstant.CHAT_NOTIFICATION);
						Bundle bundle = new Bundle();
						bundle.putString("default", intent.getExtras().getString("default"));
						broadcastIntent.putExtras(bundle);
						LocalBroadcastManager.getInstance(context).sendBroadcast(broadcastIntent);
						
						return;
					}
					else {
						b.putSerializable("Group", g);
					}
				}
				else {
					b.putSerializable("Group", g);
				}
			}
			else if (obj.getString("type").equals(NotificationConstant.ACCOUNT)) {
				Account a = UserParser.parseAccount(new SharefolioJSONObject(obj.getJSONObject("response").toString()));
				b.putSerializable("Account", a);
			}
			
			Intent i = new Intent(this, MainActivity.class);
			i.putExtras(b);
			PendingIntent pendintIntent = PendingIntent.getActivity(this, 0, i, PendingIntent.FLAG_CANCEL_CURRENT);
			builder.setContentIntent(pendintIntent);
			Notification notification = builder.build();
			notification.defaults |= Notification.DEFAULT_SOUND;
			notification.defaults |= Notification.DEFAULT_VIBRATE;
			notification.tickerText = obj.getString("notification");
			/* int count = Common.getNotificationCount(context);
			count++;
			Common.setNotificationCount(context, count);
			manager.notify(context.getResources().getString(R.string.app_name), count, notification); */
			manager.notify(context.getResources().getString(R.string.app_name), 1, notification);
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
	}
}
