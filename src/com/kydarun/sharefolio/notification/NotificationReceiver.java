package com.kydarun.sharefolio.notification;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

public class NotificationReceiver extends WakefulBroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent != null) {
			ComponentName comp = new ComponentName(context.getPackageName(), NotificationReceiverService.class.getName());
			startWakefulService(context, intent.setComponent(comp));
			
			setResultCode(Activity.RESULT_OK);
		}
	}
}
