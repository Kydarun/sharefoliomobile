package com.kydarun.sharefolio.notification;

import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.constant.NotificationConstant;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsMessage;
import android.util.Log;

public class SMSReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
			Bundle bundle = intent.getExtras();
			SmsMessage[] messages = null;
			String messageFrom = "";
			
			if (bundle != null) {
				try {
					Object[] pdus = (Object[])bundle.get("pdus");
					messages = new SmsMessage[pdus.length];
					for (int i = 0; i < messages.length; i++) {
						messages[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
						messageFrom = messages[i].getOriginatingAddress();
						String messageBody = messages[i].getMessageBody();
						
						if (messageBody.contains(context.getResources().getString(R.string.txtSMS))) {
							String code = messageBody.substring(messageBody.indexOf("Your PIN number is ") + "Your PIN number is ".length(), messageBody.length() - 1);
							ActivityManager activity = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
							RunningTaskInfo app = activity.getRunningTasks(1).get(0);
							PackageInfo info = context.getPackageManager().getPackageInfo(app.topActivity.getPackageName(), 0);
							Log.d(NotificationReceiverService.class.getSimpleName(), app.topActivity.getClassName());
							if (info.applicationInfo.packageName.toString().equals(context.getPackageName())
									&& app.topActivity.getClassName().equals("com.kydarun.sharefolio.VerifyPhoneActivity")) {
								Intent broadcastIntent = new Intent(NotificationConstant.SMS_NOTIFICATION);
								Bundle b = new Bundle();
								bundle.putString("Code", code);
								broadcastIntent.putExtras(bundle);
								LocalBroadcastManager.getInstance(context).sendBroadcast(broadcastIntent);
							}
						}
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}
