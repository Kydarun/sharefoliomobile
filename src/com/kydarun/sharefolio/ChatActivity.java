package com.kydarun.sharefolio;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.UUID;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.kydarun.sharefolio.adapter.ChatAdapter;
import com.kydarun.sharefolio.api.GroupApi;
import com.kydarun.sharefolio.constant.NotificationConstant;
import com.kydarun.sharefolio.obj.Group;
import com.kydarun.sharefolio.obj.GroupMessage;
import com.kydarun.sharefolio.parser.GroupMessageParser;
import com.kydarun.sharefolio.parser.GroupParser;
import com.kydarun.sharefolio.util.Encryption;
import com.kydarun.sharefolio.util.SharefolioJSONObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class ChatActivity extends BaseActivity implements OnScrollListener, OnClickListener {
	ListView lvGroupChat;
	ChatAdapter adapter;
	EditText etMessage;
	ImageButton btnSendMessage;
	ImageButton ibSendPicture;
	ChatAsyncTask chatTask;
	
	static final int RESULT_LOAD_IMAGE = 11;
	
	private BroadcastReceiver onEvent = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Bundle b = intent.getExtras();
			
			if (b != null && !b.isEmpty()) {
				if (adapter != null) {
					JSONObject obj;
					try {
						obj = new JSONObject(b.getString("default")).getJSONObject("GCM").getJSONObject("data").getJSONObject("message");
						Group g = GroupParser.parseGroup(new SharefolioJSONObject(obj.getJSONObject("response").toString()));
						if (g.groupid.equals(group.groupid)) {
							adapter.chats.add(g.messages.get(0));
							adapter.notifyDataSetChanged();
							lvGroupChat.setSelection(adapter.chats.size() - 1);
						}
						else {
							NotificationManager manager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
							Notification.Builder builder = new Notification.Builder(context)
								.setSmallIcon(R.drawable.ic_launcher)
								.setContentTitle(context.getResources().getString(R.string.app_name))
								.setAutoCancel(true)
								.setContentText(obj.getString("notification"));
							Bundle bundle = new Bundle();
							bundle.putString("type", NotificationConstant.GROUP);
							bundle.putSerializable("Group", g);
							
							Intent i = new Intent(context, MainActivity.class);
							i.putExtras(bundle);
							PendingIntent pendintIntent = PendingIntent.getActivity(context, 0, i, PendingIntent.FLAG_CANCEL_CURRENT);
							builder.setContentIntent(pendintIntent);
							Notification notification = builder.build();
							notification.defaults |= Notification.DEFAULT_SOUND;
							notification.defaults |= Notification.DEFAULT_VIBRATE;
							notification.tickerText = obj.getString("notification");
							/* int count = Common.getNotificationCount(context);
							count++;
							Common.setNotificationCount(context, count);
							manager.notify(context.getResources().getString(R.string.app_name), count, notification); */
							manager.notify(context.getResources().getString(R.string.app_name), 1, notification);
							
						}
					} catch (JSONException e) {
						e.printStackTrace();
						Toast.makeText(context, "Failed to parse message: " + b.getString("default"), Toast.LENGTH_SHORT).show();
					}
				}
			}
			else {
				Log.d(ChatActivity.class.getSimpleName(), "Can't receive message");
			}
		}
	};
	
	Group group;
	int start;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.group_chat);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		getFragmentManager().beginTransaction().add(R.id.rlHeader, header).commit();
		
		flProgress = (FrameLayout)findViewById(R.id.flProgress);
		
		Bundle b = this.getIntent().getExtras();
		if (b.containsKey("group")) {
			group = (Group)b.getSerializable("group");
			super.setTitle(group.groupname);
		}
		else {
			group = new Group();
			super.setTitle("Untitled");
		}
		
		lvGroupChat = (ListView)findViewById(R.id.lvGroupChat);
		if (lvGroupChat != null) {
			View vEmpty = findViewById(R.id.vEmpty);
			lvGroupChat.setEmptyView(vEmpty);
			vEmpty.setVisibility(View.GONE);
			lvGroupChat.setOnScrollListener(this);
			lvGroupChat.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);
			chatTask = new ChatAsyncTask();
			chatTask.execute();
		}
		etMessage = (EditText)findViewById(R.id.etMessage);
		etMessage.addTextChangedListener(new TextWatcher() {

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				String message = s.toString().trim();
				
				if (message.equals("")) {
					ibSendPicture.setVisibility(View.VISIBLE);
				}
				else {
					ibSendPicture.setVisibility(View.GONE);
				}
			}
		});
		btnSendMessage = (ImageButton)findViewById(R.id.btnSendMessage);
		if (btnSendMessage != null) {
			btnSendMessage.setOnClickListener(this);
		}
		ibSendPicture = (ImageButton)findViewById(R.id.ibSendPicture);
		if (ibSendPicture != null) {
			ibSendPicture.setOnClickListener(this);
		}
		start = 0;
		
		final int abTitleId = getResources().getIdentifier("action_bar_title", "id", "android");
		findViewById(abTitleId).setOnClickListener(new View.OnClickListener() {

		    @Override
		    public void onClick(View v) {
		        Intent intent = new Intent(context, GroupInfoActivity.class);
		        Bundle b = new Bundle();
		        b.putSerializable("Group", group);
		        intent.putExtras(b);
		        startActivity(intent);
		    }
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null) {
			try {
				Uri image = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };
				Cursor cursor = context.getContentResolver().query(image, filePathColumn, null, null, null);
				cursor.moveToFirst();
				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				String picturePath = cursor.getString(columnIndex);
				Log.d(ChatActivity.class.getSimpleName(), picturePath);
				File photo = new File(picturePath);
				
				GroupMessage gm = new GroupMessage();
				gm.message = "";
				gm.groupchatid = (long) 0;
				gm.fromUser = sm.getCurrentSession();
				gm.gap = 0L;
				gm.uuid = UUID.randomUUID().toString();
				gm.attachment = picturePath;
				
				AsyncHttpClient client = new AsyncHttpClient();
				RequestParams param = new RequestParams();
				
				param.put("accountID", Encryption.encrypt(gm.fromUser.ID));
				param.put("groupID", Encryption.encrypt(group.groupid));
				param.put("message", gm.message);
				param.put("attachment", photo);
				param.put("uuid", gm.uuid);
				param.put("func", "SEND_MESSAGE");
				
				adapter.chats.add(gm);
				adapter.notifyDataSetChanged();
				lvGroupChat.setSelection(adapter.chats.size() - 1);
				
				client.post(context.getResources().getString(R.string.url) + "group.php", param, new AsyncHttpResponseHandler() {

					@Override
					public void onFailure(int statusCode, Header[] headers, byte[] responseBody,
							Throwable error) {
						Toast.makeText(context, "Upload failed", Toast.LENGTH_SHORT);
						Log.d(ChatActivity.class.getSimpleName(), new String(responseBody));
					}

					@Override
					public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
						try {
							Log.d(ChatActivity.class.getSimpleName(), new String(responseBody));
							SharefolioJSONObject object = new SharefolioJSONObject(new String(responseBody));
							if (object.isValid()) {
								GroupMessage result = GroupMessageParser.parseGroupMessage(object.getJSONArray("message").getJSONObject(0));
								for (int i = 0 ; i < adapter.chats.size() ; i++) {
									if (adapter.chats.get(i).uuid.equals(result.uuid)) {
										adapter.chats.set(i, result);
										adapter.notifyDataSetChanged();
										lvGroupChat.setSelection(adapter.chats.size() - 1);
									}
								}
							}
							else {
								Toast.makeText(context, "Upload failed", Toast.LENGTH_SHORT);
							}
						}
						catch (Exception ex) {
							ex.printStackTrace();
						}
					}
				});
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		IntentFilter f = new IntentFilter(NotificationConstant.CHAT_NOTIFICATION);
		LocalBroadcastManager.getInstance(context).registerReceiver(onEvent, f);
		
		new GetGroupInfoTask().execute();
	}

	@Override
	protected void onPause() {
		super.onPause();
		
		LocalBroadcastManager.getInstance(context).unregisterReceiver(onEvent);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btnSendMessage:
				String msg = etMessage.getText().toString().trim();
				if (msg.equals("")) {
					Toast.makeText(context, "Message cannot be empty.", Toast.LENGTH_SHORT).show();
					return;
				}
				GroupMessage gm = new GroupMessage();
				gm.message = msg;
				gm.groupchatid = (long) 0;
				gm.fromUser = sm.getCurrentSession();
				gm.gap = 0L;
				gm.uuid = UUID.randomUUID().toString();
				
				adapter.chats.add(gm);
				adapter.notifyDataSetChanged();
				lvGroupChat.setSelection(adapter.chats.size() - 1);
				
				new SendChatAsyncTask().execute(gm.uuid);
				break;
			case R.id.ibSendPicture:
				Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				startActivityForResult(intent, RESULT_LOAD_IMAGE);
				break;
		}
	}
	
	class GetGroupInfoTask extends AsyncTask<Void, Void, Group> {

		@Override
		protected Group doInBackground(Void... params) {
			// TODO Auto-generated method stub
			GroupApi ga = new GroupApi(context);
			try {
				return ga.getGroupInfo(sm.getCurrentSession().ID, group.groupid);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Group result) {
			// TODO Auto-generated method stub
			setTitle(result.groupname);
			group = result;
		}
		
	}
	
	class ChatAsyncTask extends AsyncTask<Void, Void, List<GroupMessage>> {
		boolean isFinished;
		boolean isEOF;
		
		public ChatAsyncTask() {
			super();
			this.isFinished = false;
			this.isEOF = false;
		}
		
		@Override
		protected List<GroupMessage> doInBackground(Void... params) {
			GroupApi api = new GroupApi(context);
			try {
				return api.getGroupChat(sm.getCurrentSession().ID, group.groupid, start, 20);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return null;
		}

		@Override
		protected void onPostExecute(List<GroupMessage> result) {
			super.onPostExecute(result);
			if (result != null) {
				if (result.size() != 0) {
					if (adapter == null) {
						adapter = new ChatAdapter(context, result);
						lvGroupChat.setAdapter(adapter);
						lvGroupChat.setSelection(adapter.chats.size() - 1);
					}
					else {
						int initialIndex = lvGroupChat.getFirstVisiblePosition();
						adapter.chats.addAll(0, result);
						adapter.notifyDataSetChanged();
						lvGroupChat.setSelection(initialIndex + result.size() - 1);
					}
					start += 20;
				}
				else {
					this.isEOF = true;
					
					if (adapter == null) {
						adapter = new ChatAdapter(context, result);
						lvGroupChat.setAdapter(adapter);
					}
				}
				
				flProgress.setVisibility(View.GONE);
			}
			this.isFinished = true;
		}
	}
	
	class SendChatAsyncTask extends AsyncTask<String, Void, GroupMessage> {
		String message = "";
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			message = etMessage.getText().toString();
			etMessage.setText("");
		}

		@Override
		protected GroupMessage doInBackground(String... params) {
			GroupApi api = new GroupApi(context);
			try {
				return api.sendMessage(sm.getCurrentSession().ID, group.groupid, message, params[0]);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(GroupMessage result) {
			super.onPostExecute(result);
			
			if (result != null) {
				for (int i = 0 ; i < adapter.chats.size() ; i++) {
					if (adapter.chats.get(i).uuid.equals(result.uuid)) {
						adapter.chats.set(i, result);
						adapter.notifyDataSetChanged();
						lvGroupChat.setSelection(adapter.chats.size() - 1);
					}
				}
			}
			else {
				// Send failed
				Toast.makeText(context, "Message failed to send.", Toast.LENGTH_SHORT);
			}
		}
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		if (visibleItemCount < totalItemCount) {
			if (firstVisibleItem < 3 && !chatTask.isEOF && chatTask.isFinished) {
				chatTask = new ChatAsyncTask();
				chatTask.execute();
			}
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		
	}
}
