package com.kydarun.sharefolio;

import java.util.ArrayList;
import java.util.List;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Menu;
import android.view.MenuItem;

import com.astuetz.PagerSlidingTabStrip;
import com.imbryk.viewPager.LoopViewPager;
import com.kydarun.sharefolio.constant.NotificationConstant;
import com.kydarun.sharefolio.constant.PageNameConstant;
import com.kydarun.sharefolio.fragment.BaseFragment;
import com.kydarun.sharefolio.fragment.GroupListFragment;
import com.kydarun.sharefolio.fragment.HomeFragment;
import com.kydarun.sharefolio.fragment.MyCompanyFragment;
import com.kydarun.sharefolio.util.Common;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.listeners.OnLogoutListener;

public class MainActivity extends BaseActivity implements OnPageChangeListener {
	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a {@link FragmentPagerAdapter}
	 * derivative, which will keep every loaded fragment in memory. If this
	 * becomes too memory intensive, it may be best to switch to a
	 * {@link android.support.v13.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;
	SimpleFacebook fb;
	
	PagerSlidingTabStrip tabs;
	
	int currentPage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		header.pageName = PageNameConstant.HOME_PAGE;
		
		// getFragmentManager().beginTransaction().add(R.id.rlHeader, header).commit();
		mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());
		
		Common.setNotificationCount(context, 0);

		// Set up the ViewPager with the sections adapter.
		/* mViewPager = (ViewPager)findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);
		mViewPager.setOffscreenPageLimit(100);
		mViewPager.setCurrentItem(1, false);
		
		tabs = (PagerSlidingTabStrip)findViewById(R.id.tabs);
		tabs.setShouldExpand(true);
		tabs.setIndicatorColorResource(R.color.dark_green);
		tabs.setUnderlineColorResource(R.color.dark_green);
		tabs.setTypeface(Common.getFontRegular(context), Typeface.NORMAL);
		tabs.setTextColorResource(R.color.black);
		tabs.setOnPageChangeListener(this);
		tabs.setViewPager(mViewPager);
		tabs.setTextSize(36); */
		
		super.pageName = PageNameConstant.HOME_PAGE;
		
		Bundle b = getIntent().getExtras();
		if (b != null) {
			if (b.getString("type").equals(NotificationConstant.COMPANY)) {
				Intent i = new Intent(context, CompanyActivity.class);
				Bundle bundle = new Bundle();
				bundle.putSerializable("company", b.getSerializable("company"));
				i.putExtras(bundle);
				startActivity(i);
			}
			else if (b.getString("type").equals(NotificationConstant.WALL) || b.getString("type").equals(NotificationConstant.COMMENT)) {
				Intent i = new Intent(context, WallNotificationActivity.class);
				Bundle bundle = new Bundle();
				bundle.putSerializable("Wall", b.getSerializable("Wall"));
				i.putExtras(bundle);
				startActivity(i);
			}
			else if (b.getString("type").equals(NotificationConstant.GROUP)) {
				Intent i = new Intent(context, ChatActivity.class);
				Bundle bundle = new Bundle();
				bundle.putSerializable("group", b.getSerializable("Group"));
				i.putExtras(bundle);
				startActivity(i);
			}
			else if (b.getString("type").equals(NotificationConstant.FRIEND_REQUEST)) {
				Intent i = new Intent(context, FriendRequestActivity.class);
				startActivity(i);
			}
			else if (b.getString("type").equals(NotificationConstant.ACCOUNT)) {
				Intent i = new Intent(context, ProfileActivity.class);
				Bundle bundle = new Bundle();
				bundle.putSerializable("Account", b.getSerializable("Account"));
				i.putExtras(b);
				startActivity(i);
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		else if (id == R.id.action_logout) {
			sm.logout();
			if (fb.isLogin()) {
				fb.logout(new OnLogoutListener() {
	
					@Override
					public void onThinking() {
	
					}
	
					@Override
					public void onException(Throwable throwable) {
	
					}
	
					@Override
					public void onFail(String reason) {
	
					}
	
					@Override
					public void onLogout() {
						Intent i = new Intent(context, LoginActivity.class);
						startActivity(i);
						finish();
					}
				});
			}
			else {
				Intent i = new Intent(context, LoginActivity.class);
				startActivity(i);
				finish();
			}
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		super.onResume();
		fb = SimpleFacebook.getInstance(this);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		fb.onActivityResult(this, requestCode, resultCode, data);
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	static class SectionsPagerAdapter extends FragmentPagerAdapter {

		List<BaseFragment> frags = new ArrayList<BaseFragment>();

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
			frags.add(new HomeFragment());
			frags.add(new MyCompanyFragment());
			frags.add(new GroupListFragment());
		}

		@Override
		public Fragment getItem(int position) {
			position = LoopViewPager.toRealPosition(position, getCount());
			switch (position % frags.size()) {
				case 0:
					return new HomeFragment();
				case 1:
					return new MyCompanyFragment();
				case 2:
					return new GroupListFragment();
				default:
					return null;
			}
		}

		@Override
		public int getCount() {
			return frags.size();
		}

		@Override
		public CharSequence getPageTitle(int position) {
			switch (position) {
				case 0: return PageNameConstant.MY_CHAT;
				case 1: return PageNameConstant.HOME_PAGE;
				case 2: return PageNameConstant.MY_COMPANY;
				default: return "";
			}
		}
	}

	@Override
	public void onPageScrollStateChanged(int state) {
		
	}

	@Override
	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
		if (header.isTaskFinished()) {
			header.getLatestPrice();
		}
		currentPage = position;
		
		switch (currentPage) {
			case 0: 
				super.pageName = PageNameConstant.MY_CHAT;
				break;
			case 1: 
				super.pageName = PageNameConstant.HOME_PAGE;
				break;
			case 2: 
				super.pageName = PageNameConstant.MY_COMPANY;
				break;
		}
	}

	@Override
	public void onPageSelected(int position) {
		
	}
}
