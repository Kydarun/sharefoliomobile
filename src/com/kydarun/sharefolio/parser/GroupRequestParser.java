package com.kydarun.sharefolio.parser;

import java.util.ArrayList;
import java.util.List;

import com.kydarun.sharefolio.obj.GroupRequest;
import com.kydarun.sharefolio.util.Encryption;
import com.kydarun.sharefolio.util.SharefolioJSONArray;
import com.kydarun.sharefolio.util.SharefolioJSONObject;

public class GroupRequestParser {
	public static GroupRequest parseGroupRequest(SharefolioJSONObject object) {
		GroupRequest request = new GroupRequest();
		
		request.grouprequestid = Encryption.decrypt(object.getString("grouprequestid"));
		request.requestdate = object.getString("requestdate");
		request.from = UserParser.parseAccount(object.getJSONObject("from"));
		
		return request;
	}
	
	public static List<GroupRequest> parseGroupRequestList(SharefolioJSONArray array) {
		List<GroupRequest> requests = new ArrayList<GroupRequest>();
		for (int i = 0; i < array.length(); i++) {
			requests.add(GroupRequestParser.parseGroupRequest(array.getJSONObject(i)));
		}
		
		return requests;
	}
}
