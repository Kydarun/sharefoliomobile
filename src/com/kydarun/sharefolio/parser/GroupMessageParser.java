package com.kydarun.sharefolio.parser;

import java.util.ArrayList;
import java.util.List;

import com.kydarun.sharefolio.obj.GroupMessage;
import com.kydarun.sharefolio.util.Encryption;
import com.kydarun.sharefolio.util.SharefolioJSONArray;
import com.kydarun.sharefolio.util.SharefolioJSONObject;

public class GroupMessageParser {
	public static GroupMessage parseGroupMessage(SharefolioJSONObject object) {
		GroupMessage groupMessage = new GroupMessage();
		groupMessage.groupchatid = Encryption.decrypt(object.getString("groupchatid"));
		groupMessage.fromUser = UserParser.parseAccount(object.getJSONObject("fromUser"));
		groupMessage.timestamp = object.getString("timestamp");
		groupMessage.gap = object.getLong("gap");
		groupMessage.attachment = object.getString("attachment");
		groupMessage.message = object.getString("message");
		groupMessage.uuid = object.getString("uuid");
		groupMessage.companyTagged = UserParser.parseCompanyList(object.getJSONArray("companyTagged"));
		
		return groupMessage;
	}
	
	public static List<GroupMessage> parseGroupMessageList(SharefolioJSONArray array) {
		List<GroupMessage> groupMessages = new ArrayList<GroupMessage>();
		for (int i = 0; i < array.length(); i++) {
			groupMessages.add(GroupMessageParser.parseGroupMessage(array.getJSONObject(i)));
		}
		return groupMessages;
	}
}
