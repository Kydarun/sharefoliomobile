package com.kydarun.sharefolio.parser;

import java.util.ArrayList;
import java.util.List;

import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.obj.Company;
import com.kydarun.sharefolio.util.Encryption;
import com.kydarun.sharefolio.util.SharefolioJSONArray;
import com.kydarun.sharefolio.util.SharefolioJSONObject;

public class UserParser {
	public static Account parseAccount(SharefolioJSONObject object) {
		Account account = new Account();
		
		account.ID = Encryption.decrypt(object.getString("ID"));
		account.firstname = object.getString("firstname");
		account.avatar = object.getString("avatar");
		account.email = object.getString("email");
		account.fb_id = object.getString("fb_id");
		account.friends = UserParser.parseAccountList(object.getJSONArray("friend"));
		account.password = object.getString("password");
		account.phoneNumber = object.getString("phoneNumber");
		account.verificationCode = object.getString("verificationCode");
		account.registration_id = object.getString("registration_id");
		account.platformEndpointArn = object.getString("platformEndpointArn");
		account.companies = UserParser.parseWallpostCompanyList(object.getJSONArray("companyList"));
		account.friends = UserParser.parseAccountList(object.getJSONArray("friends"));
		account.groups = GroupParser.parseGroupList(object.getJSONArray("groupList"));
		if (object.getString("isFriend").equals("1")) {
			account.isFriend = true;
		}
		else {
			account.isFriend = false;
		}
		account.requestSent = object.getBoolean("requestSent");
		account.requestPending = object.getBoolean("requestPending");
		return account;
	}
	
	public static List<Account> parseAccountList(SharefolioJSONArray array) {
		List<Account> accounts = new ArrayList<Account>();
		for (int i = 0; i < array.length(); i++) {
			accounts.add(UserParser.parseAccount(array.getJSONObject(i)));
		}
		return accounts;
	}
	
	public static Company parseCompany(SharefolioJSONObject object) {
		Company company = new Company();
		SharefolioJSONObject companyInfo = object.getJSONObject("companyinfo");
		company.id = companyInfo.getString("id");
		company.companyCode = companyInfo.getString("companyCode");
		company.companyName = companyInfo.getString("companyName");
		company.avatar = companyInfo.getString("avatar");
		company.password = object.getString("password");
		company.price = PriceParser.parsePrice(object.getJSONObject("price"));
		company.history = HistoryParser.parseHistoryList(object.getJSONArray("history"));
		company.wallpost = WallParser.parseWallList(object.getJSONArray("wallpost"));
		company.isFollowing = object.getBoolean("isFollowing");
		
		return company;
	}
	
	public static Company parseWallpostCompany(SharefolioJSONObject object) {
		Company company = new Company();
		company.id = object.getString("id");
		company.companyCode = object.getString("companyCode");
		company.companyName = object.getString("companyName");
		company.avatar = object.getString("avatar");
		company.password = object.getString("password");
		company.price = PriceParser.parsePrice(object.getJSONObject("price"));
		company.history = HistoryParser.parseHistoryList(object.getJSONArray("history"));
		company.wallpost = WallParser.parseWallList(object.getJSONArray("wallpost"));
		company.isFollowing = object.getBoolean("isFollowing");
		
		return company;
	}
	
	public static List<Company> parseWallpostCompanyList(SharefolioJSONArray array) {
		List<Company> companies = new ArrayList<Company>();
		for (int i = 0; i < array.length(); i++) {
			companies.add(UserParser.parseWallpostCompany(array.getJSONObject(i)));
		}
		return companies;
	}
	
	public static List<Company> parseCompanyList(SharefolioJSONArray array) {
		List<Company> companies = new ArrayList<Company>();
		for (int i = 0; i < array.length(); i++) {
			companies.add(UserParser.parseCompany(array.getJSONObject(i)));
		}
		return companies;
	}
}
