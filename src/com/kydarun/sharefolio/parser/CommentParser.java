package com.kydarun.sharefolio.parser;

import java.util.ArrayList;
import java.util.List;

import com.kydarun.sharefolio.obj.Comment;
import com.kydarun.sharefolio.util.Encryption;
import com.kydarun.sharefolio.util.SharefolioJSONArray;
import com.kydarun.sharefolio.util.SharefolioJSONObject;

public class CommentParser {
	public static Comment parseComment(SharefolioJSONObject object) {
		Comment comment = new Comment();
		comment.commentID = Encryption.decrypt(object.getString("commentID"));
		comment.datePosted = object.getString("datePosted");
		comment.liked = object.getBoolean("liked");
		comment.message = object.getString("message");
		comment.like = UserParser.parseAccountList(object.getJSONArray("like"));
		comment.gap = object.getLong("gap");
		comment.user = UserParser.parseAccount(object.getJSONObject("user"));
		comment.userLike = object.getString("userLike");
		comment.attachment = object.getString("attachment");
		comment.personTagged = UserParser.parseAccountList(object.getJSONArray("personTagged"));
		comment.companyTagged = UserParser.parseWallpostCompanyList(object.getJSONArray("companyTagged"));
		
		return comment;
	}
	
	public static List<Comment> parseCommentList(SharefolioJSONArray array) {
		List<Comment> comments = new ArrayList<Comment>();
		for (int i = 0; i < array.length(); i++) {
			comments.add(CommentParser.parseComment(array.getJSONObject(i)));
		}
		
		return comments;
	}
}
