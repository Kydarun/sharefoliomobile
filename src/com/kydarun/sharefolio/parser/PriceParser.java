package com.kydarun.sharefolio.parser;

import java.util.ArrayList;
import java.util.List;

import com.kydarun.sharefolio.obj.Price;
import com.kydarun.sharefolio.util.SharefolioJSONArray;
import com.kydarun.sharefolio.util.SharefolioJSONObject;

public class PriceParser {
	public static Price parsePrice(SharefolioJSONObject object) {
		Price price = new Price();
		price.ref = object.getString("ref");
		price.high = object.getString("high");
		price.low = object.getString("low");
		price.last = object.getString("last");
		price.stockChange = object.getString("stockChange");
		price.volume = object.getString("volume");
		
		return price;
	}
	
	public static List<Price> parsePriceList(SharefolioJSONArray array) {
		List<Price> prices = new ArrayList<Price>();
		for (int i = 0; i < array.length(); i++) {
			prices.add(PriceParser.parsePrice(array.getJSONObject(i)));
		}
		
		return prices;
	}
}
