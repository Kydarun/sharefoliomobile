package com.kydarun.sharefolio.parser;

import java.util.ArrayList;
import java.util.List;

import com.kydarun.sharefolio.obj.History;
import com.kydarun.sharefolio.util.SharefolioJSONArray;
import com.kydarun.sharefolio.util.SharefolioJSONObject;

public class HistoryParser {
	public static History parseHistory(SharefolioJSONObject object) {
		History history = new History();
		
		history.date = object.getString("date");
		history.value = object.getString("value");
		history.volume = object.getString("volume");
		
		return history;
	}
	
	public static List<History> parseHistoryList(SharefolioJSONArray array) {
		List<History> histories = new ArrayList<History>();
		for (int i = 0; i < array.length(); i++) {
			histories.add(HistoryParser.parseHistory(array.getJSONObject(i)));
		}
		return histories;
	}
}
