package com.kydarun.sharefolio.parser;

import java.util.ArrayList;
import java.util.List;

import com.kydarun.sharefolio.obj.Request;
import com.kydarun.sharefolio.util.Encryption;
import com.kydarun.sharefolio.util.SharefolioJSONArray;
import com.kydarun.sharefolio.util.SharefolioJSONObject;

public class RequestParser {
	public static Request parseRequest(SharefolioJSONObject object) {
		Request request = new Request();
		request.id = Encryption.decrypt(object.getString("id"));
		request.from = UserParser.parseAccount(object.getJSONObject("from"));
		request.status = object.getString("status");
		
		return request;
	}
	
	public static List<Request> parseRequestList(SharefolioJSONArray array) {
		List<Request> requests = new ArrayList<Request>();
		for (int i = 0; i < array.length(); i++) {
			requests.add(RequestParser.parseRequest(array.getJSONObject(i)));
		}
		
		return requests;
	}
}
