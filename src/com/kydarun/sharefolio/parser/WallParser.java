package com.kydarun.sharefolio.parser;

import java.util.ArrayList;
import java.util.List;

import com.kydarun.sharefolio.obj.Wall;
import com.kydarun.sharefolio.util.Encryption;
import com.kydarun.sharefolio.util.SharefolioJSONArray;
import com.kydarun.sharefolio.util.SharefolioJSONObject;

public class WallParser {
	public static Wall parseWall(SharefolioJSONObject object) {
		Wall wall = new Wall();
		wall.wallID = Encryption.decrypt(object.getString("wallID"));
		wall.wallStamp = object.getString("wallStamp");
		wall.wallType = object.getInt("wallType");
		wall.wallContent = object.getString("wallContent");
		wall.gap = object.getLong("gap");
		wall.poster = object.has("poster") ? UserParser.parseAccount(object.getJSONObject("poster")) : null;
		wall.target = object.has("target") ? UserParser.parseAccount(object.getJSONObject("target")) : null;
		wall.posterCompany = object.has("posterCompany") ? UserParser.parseWallpostCompany(object.getJSONObject("posterCompany")) : null;
		wall.targetCompany = object.has("targetCompany") ? UserParser.parseWallpostCompany(object.getJSONObject("targetCompany")) : null;
		wall.attachment = object.getString("attachment");
		wall.featured = object.getInt("featured") == 1 ? true : false;
		wall.userLike = object.getString("userLike");
		wall.featuredDate = object.getString("featuredDate");
		wall.lastEditedDate = object.getString("lastEditedDate");
		wall.like = UserParser.parseAccountList(object.getJSONArray("like"));
		wall.announcementTitle = object.getString("announcementTitle");
		wall.announcementLink = object.getString("announcementLink");
		wall.announcementDesc = object.getString("announcementDesc");
		wall.linkDescription = object.getString("linkDescription");
		wall.linkTitle = object.getString("linkTitle");
		wall.linkUrl = object.getString("linkUrl");
		wall.linkImage = object.getString("linkImage");
		wall.linkVideo = object.getString("linkVideo");
		wall.liked = object.getBoolean("liked");
		wall.personTagged = UserParser.parseAccountList(object.getJSONArray("personTagged"));
		wall.companyTagged = UserParser.parseWallpostCompanyList(object.getJSONArray("companyTagged"));
		wall.comments = CommentParser.parseCommentList(object.getJSONArray("comment"));
		
		return wall;
	}
	
	public static List<Wall> parseWallList(SharefolioJSONArray array) {
		List<Wall> walls = new ArrayList<Wall>();
		for (int i = 0; i < array.length(); i++) {
			walls.add(WallParser.parseWall(array.getJSONObject(i)));
		}
		
		return walls;
	}
}
