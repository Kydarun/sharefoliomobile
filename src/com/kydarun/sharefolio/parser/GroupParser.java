package com.kydarun.sharefolio.parser;

import java.util.ArrayList;
import java.util.List;

import com.kydarun.sharefolio.obj.Group;
import com.kydarun.sharefolio.util.Encryption;
import com.kydarun.sharefolio.util.SharefolioJSONArray;
import com.kydarun.sharefolio.util.SharefolioJSONObject;

public class GroupParser {
	public static Group parseGroup(SharefolioJSONObject object) {
		Group group = new Group();
		
		group.groupid = Encryption.decrypt(object.getString("groupid"));
		group.groupname = object.getString("groupname");
		group.createdby = UserParser.parseAccount(object.getJSONObject("createdby"));
		group.createddate = object.getString("createddate");
		group.membercount = object.getString("membercount");
		group.coverphoto = object.getString("coverphoto");
		group.grouptype = object.getString("grouptype");
		group.messages = GroupMessageParser.parseGroupMessageList(object.getJSONArray("message"));
		group.members = UserParser.parseAccountList(object.getJSONArray("members"));
		group.requests = GroupRequestParser.parseGroupRequestList(object.getJSONArray("grouprequest"));
		group.grouptype = object.getString("grouptype");
		group.companies = UserParser.parseWallpostCompanyList(object.getJSONArray("companyList"));
		group.requestSent = object.getBoolean("requestSent");
		
		return group;
	}
	
	public static List<Group> parseGroupList(SharefolioJSONArray array) {
		List<Group> groups = new ArrayList<Group>();
		for (int i = 0; i < array.length(); i++) {
			groups.add(GroupParser.parseGroup(array.getJSONObject(i)));
		}
		
		return groups;
	}
}
