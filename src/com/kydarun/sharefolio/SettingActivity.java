package com.kydarun.sharefolio;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kydarun.sharefolio.api.UserApi;
import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.util.Common;

public class SettingActivity extends BaseActivity {
	Button btnLogout;
	EditText etUsername;
	UserApi ua;
	String username;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		btnLogout = (Button) findViewById(R.id.bLogout);
		etUsername = (EditText) findViewById(R.id.etUsername);
		
		etUsername.setText(sm.getCurrentSession().firstname);
		etUsername.setOnKeyListener(new OnKeyListener() {
			
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
					username = etUsername.getText().toString();
					new EditProfileTask().execute();
				}
				return false;
			}
		});
		
		btnLogout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (sm.logout()) {
					context.deleteDatabase("sharefoliomobile");
					Common.setSetting(context, "last_updated", 0L);
					Toast.makeText(context, "Logout successfully", Toast.LENGTH_SHORT).show();
					Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
					i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(i);
				} else {
					Toast.makeText(context, "Logout failed!", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
		
	}

	private class EditProfileTask extends AsyncTask<Void, Void, Void> {
		Account editedAccount;
		Account newAccount;

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			ua = new UserApi(SettingActivity.this);
			newAccount = sm.getCurrentSession();
			newAccount.firstname = username;
			try {
				editedAccount = ua.editProfile(newAccount);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			if (editedAccount != null) {		
				sm.setSession(editedAccount);
				Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
				etUsername.setEnabled(false);
			} else {
				Toast.makeText(context, "Fail", Toast.LENGTH_SHORT).show();
			}
		}
	}
	
}
