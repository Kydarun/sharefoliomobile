package com.kydarun.sharefolio;

import com.kydarun.sharefolio.api.UserApi;
import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.util.Common;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class PhoneNumberActivity extends BaseActivity implements OnClickListener {
	EditText etPhoneNumber;
	Button btnSubmit;
	boolean isCreated = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.phone_number);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		TextView tvEnterPhoneNumber = (TextView)findViewById(R.id.tvEnterPhoneNumber);
		if (tvEnterPhoneNumber != null) {
			tvEnterPhoneNumber.setTypeface(Common.getFontRegular(context));
		}
		
		etPhoneNumber = (EditText)findViewById(R.id.etPhoneNumber);
		
		btnSubmit = (Button)findViewById(R.id.btnPhoneSubmit);
		if (btnSubmit != null) {
			btnSubmit.setOnClickListener(this);
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		if (isCreated) {
			finish();
		}
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btnPhoneSubmit) {
			if (etPhoneNumber.getText().toString().equals("")) {
				Toast.makeText(context, "Phone number cannot be empty!", Toast.LENGTH_SHORT).show();
			}
			else {
				new UpdatePhoneNumberAsyncTask().execute();
				progressDialog = ProgressDialog.show(context, context.getResources().getString(R.string.app_name), "Submitting...");
				isCreated = true;
			}
		}
	}
	
	class UpdatePhoneNumberAsyncTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			UserApi api = new UserApi(context);
			Account me = sm.getCurrentSession();
			me.phoneNumber = etPhoneNumber.getText().toString();
			try {
				api.updatePhoneNumber(me);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			
			progressDialog.dismiss();
			Intent intent = new Intent(context, VerifyPhoneActivity.class);
			Bundle b = new Bundle();
			b.putString("PhoneNumber", etPhoneNumber.getText().toString());
			intent.putExtras(b);
			startActivity(intent);
		}
		
	}
}
