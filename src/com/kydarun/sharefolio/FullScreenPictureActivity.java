package com.kydarun.sharefolio;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;

import com.koushikdutta.ion.Ion;

import android.os.Bundle;
import android.view.Window;
import android.widget.Toast;

public class FullScreenPictureActivity extends BaseActivity {

	ImageViewTouch ivFullScreenPicture;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.picture_fullscreen);
		
		ivFullScreenPicture = (ImageViewTouch)findViewById(R.id.ivFullScreenPicture);
		Bundle b = getIntent().getExtras();
		
		if (b.containsKey("url")) {
			Ion
			.with(context)
			.load(b.getString("url"))
			.withBitmap()
			.smartSize(true)
			.intoImageView(ivFullScreenPicture);
		}
		else {
			Toast.makeText(context, "Image not found", Toast.LENGTH_SHORT);
		}
	}
}
