package com.kydarun.sharefolio;

import java.util.ArrayList;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.costum.android.widget.PullAndLoadListView;
import com.costum.android.widget.PullAndLoadListView.OnLoadMoreListener;
import com.costum.android.widget.PullToRefreshListView.OnRefreshListener;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.kydarun.sharefolio.adapter.WallAdapter;
import com.kydarun.sharefolio.api.CompanyApi;
import com.kydarun.sharefolio.CompanyActivity;
import com.kydarun.sharefolio.chart.ChartOptions;
import com.kydarun.sharefolio.chart.ChartView;
import com.kydarun.sharefolio.chart.GenericChart;
import com.kydarun.sharefolio.obj.Company;
import com.kydarun.sharefolio.obj.History;
import com.kydarun.sharefolio.database.DBHelper;
import com.kydarun.sharefolio.util.Common;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Photo;
import com.sromku.simple.fb.listeners.OnPublishListener;


@SuppressLint("ClickableViewAccessibility")
public class ChartFullScreenActivity extends BaseActivity implements OnRefreshListener, OnLoadMoreListener, OnClickListener {
	//ChartView cvChart;
	ChartView cvChart1;
	TextView tvCompanyName;
	TextView tvCompanyPrice;
	TextView tvCompanyChange;
	ImageView myImage;
	TextView tvCompanyAbout;
	RelativeLayout rlForScreenShot;
	Button bShare;
	Button btnFollowToggle;
	PullAndLoadListView lvCompanyWall;
	WallAdapter adapter;
	SimpleFacebook mSimpleFacebook;
	Company company;
	Bitmap screenShot;
	boolean isFollowing;
	ValueAnimator mAnimator;
	ScaleAnimation scaleAnimation;
	int start;
	int chartnum = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		 getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                 WindowManager.LayoutParams.FLAG_FULLSCREEN);
			}
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chartfullscreen);
		
		Bundle b = this.getIntent().getExtras();
		company = (Company)b.getSerializable("company");
		
		
		start = 0;
		cvChart1 = (ChartView)findViewById(R.id.chartview);

		android.view.ViewGroup.LayoutParams params = cvChart1.getLayoutParams();
		params.width = pixelheight();
		params.height = pixelwidth();
		cvChart1.setLayoutParams(params);

	if(cvChart1 != null){
		ChartOptions options;
	String[] columnNames = new String[] { company.companyName };
		cvChart1.addGenericChart(columnNames, GenericChart.CHART_TYPE_SPARK_LINE, "chart_div", GenericChart.TITLE_TYPE_EMPTY);
		cvChart1.setOnTouchListener(new OnTouchListener() {

			@Override
		public boolean onTouch(View v, MotionEvent event) {
				
				finish();
				
				return (event.getAction() == MotionEvent.ACTION_MOVE);
				
			}
		});
		 
		options = cvChart1.getOptions("chart_div");
		options.setTitle("Sparkline");
		options.addColor("#ffffff");
		options.setAreaOpacity(0.0f);
		options.setBackgroundColor("#000000");
		options.setEnableInteractivity(false);	// Enable in future
	}
	
	
	new LoadCompanyAsyncTask().execute();
	
   
	
}


public int pixelheight() {
	DisplayMetrics dm = new DisplayMetrics();
	getWindowManager().getDefaultDisplay().getMetrics(dm);
	
	return (int) (dm.heightPixels);
}

public int pixelwidth() {
	DisplayMetrics dm = new DisplayMetrics();
	getWindowManager().getDefaultDisplay().getMetrics(dm);
	
	return (int) (dm.heightPixels *0.4);
}


	
	
class LoadCompanyAsyncTask extends AsyncTask<Void, Void, Void> {
	//History history;
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		
		
		for (History history: company.history) {
			cvChart1.addDatum("chart_div", company.companyName, Float.parseFloat(history.value));		
		}

		cvChart1.setBackgroundColor(Color.BLACK);
		cvChart1.drawCharts();
		
	}

	

	@Override
	protected Void doInBackground(Void... params) {
		CompanyApi api = new CompanyApi(context);
		try {
			company = api.getCompany(sm.getCurrentSession().ID, company.id, start, 15);
			company.history = api.getPriceChart(sm.getCurrentSession().ID, company.id);
			
		} catch (Exception e) {
			e.printStackTrace();
			company.history = new ArrayList<History>();
		}
		
		return null;
	}
}




@Override
public void onClick(View v) {
	// TODO Auto-generated method stub
	finish();
}


@Override
public void onLoadMore() {
	// TODO Auto-generated method stub
	
}


@Override
public void onRefresh() {
	// TODO Auto-generated method stub
	
}

}	
		