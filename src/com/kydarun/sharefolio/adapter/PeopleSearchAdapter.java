package com.kydarun.sharefolio.adapter;

import java.util.List;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.api.GroupApi;
import com.kydarun.sharefolio.api.UserApi;
import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.obj.Group;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("InflateParams")
public class PeopleSearchAdapter extends AdapterBase implements OnClickListener {
	public List<Account> peoples;
	public Group group;
	
	public PeopleSearchAdapter(Context context, List<Account> peoples) {
		super(context);
		this.peoples = peoples;
		this.group = null;
	}
	
	public PeopleSearchAdapter(Context context, List<Account> peoples, Group group) {
		this(context, peoples);
		this.group = group;
	}

	@Override
	public int getCount() {
		return peoples.size();
	}

	@Override
	public Object getItem(int position) {
		return peoples.get(position);
	}

	@Override
	public long getItemId(int position) {
		return peoples.get(position).ID;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.search_item, null);
			
			holder.ivSearchPicture = (ImageView)convertView.findViewById(R.id.ivSearchPicture);
			holder.tvSearchName = (TextView)convertView.findViewById(R.id.tvSearchName);
			holder.btnSearchAction = (Button)convertView.findViewById(R.id.btnSearchAction);
			holder.tvSearchStatus = (TextView)convertView.findViewById(R.id.tvSearchStatus);
			
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder)convertView.getTag();
		}
		Account people = peoples.get(position);
		
		UrlImageViewHelper.setUrlDrawable(holder.ivSearchPicture, context.getResources().getString(R.string.url_avatar)
				+ people.ID + people.avatar, R.drawable.ic_launcher);
		holder.tvSearchName.setText(people.firstname);
		holder.btnSearchAction.setTag(people);
		if (group == null) {
			if (people.isFriend) {
				holder.btnSearchAction.setVisibility(View.GONE);
				holder.tvSearchStatus.setText("Friend");
				holder.tvSearchStatus.setVisibility(View.VISIBLE);
			}
			else if (people.requestPending) {
				holder.btnSearchAction.setVisibility(View.GONE);
				holder.tvSearchStatus.setText("Pending Approval");
				holder.tvSearchStatus.setVisibility(View.VISIBLE);
			}
			else if (people.requestSent) {
				holder.btnSearchAction.setVisibility(View.GONE);
				holder.tvSearchStatus.setText("Request Sent");
				holder.tvSearchStatus.setVisibility(View.VISIBLE);
			}
			else {
				holder.btnSearchAction.setText("Add Friend");
				holder.btnSearchAction.setOnClickListener(this);
			}
		}
		else {
			boolean isFound = false;
			for (int i = 0; i < group.members.size(); i++) {
				Account member = group.members.get(i);
				if (member.ID.equals(people.ID)) {
					// Already in the member list
					holder.btnSearchAction.setVisibility(View.GONE);
					holder.tvSearchStatus.setText("Already a member");
					holder.tvSearchStatus.setVisibility(View.VISIBLE);
					isFound = true;
				}
			}
			if (!isFound) {
				holder.btnSearchAction.setText("Add to Group");
				holder.btnSearchAction.setOnClickListener(this);
			}
		}
		
		return convertView;
	}

	static class ViewHolder {
		ImageView ivSearchPicture;
		TextView tvSearchName;
		Button btnSearchAction;
		TextView tvSearchStatus;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btnSearchAction) {
			new PeopleActionAsyncTask((Button)v).execute();
		}
	}
	
	class PeopleActionAsyncTask extends AsyncTask<Void, Void, Boolean> {
		Button btnAction;
		
		public PeopleActionAsyncTask(Button btnAction) {
			this.btnAction = btnAction;
		}
		
		@Override
		protected Boolean doInBackground(Void... params) {
			Account people = (Account)btnAction.getTag();
			if (group == null) {
				UserApi api = new UserApi(context);
				try {
					return api.sendFriendRequest(sm.getCurrentSession().ID, people);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else {
				GroupApi api = new GroupApi(context);
				try {
					api.addMember(sm.getCurrentSession().ID, group.groupid, people.ID);
					return true;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			
			if (result) {
				ViewHolder holder = (ViewHolder)((RelativeLayout)btnAction.getParent()).getTag();
				if (group == null) {
					holder.tvSearchStatus.setText("Friend Request Sent");
				}
				else {
					holder.tvSearchStatus.setText("Added to Group");
				}
				holder.tvSearchStatus.setVisibility(View.VISIBLE);
				holder.btnSearchAction.setVisibility(View.GONE);
			}
			else {
				if (group == null) {
					Toast.makeText(context, "Failed to send request", Toast.LENGTH_SHORT).show();
				}
				else {
					Toast.makeText(context, "Failed to add member", Toast.LENGTH_SHORT).show();
				}
			}
		}
	}
}
