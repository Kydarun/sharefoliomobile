package com.kydarun.sharefolio.adapter;

import java.util.List;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.api.UserApi;
import com.kydarun.sharefolio.obj.Account;

public class SyncContactAdapter extends AdapterBase implements OnClickListener {
	List<Account> user;
	
	public SyncContactAdapter(Context context, List<Account> user) {
		super(context);
		this.user = user;
	}
	
	@Override
	public int getCount() {
		return user.size();
	}

	@Override
	public Object getItem(int position) {
		return user.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.sync_contact_item, null);
			holder = new ViewHolder();
			holder.ivSyncUserPhoto = (ImageView)convertView.findViewById(R.id.ivSyncUserPhoto);
			holder.tvSyncUserName = (TextView)convertView.findViewById(R.id.tvSyncUserName);
			holder.btnSyncUserAddFriend = (Button)convertView.findViewById(R.id.btnSyncUserAddFriend);
			
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		Account account = user.get(position);
		
		UrlImageViewHelper.setUrlDrawable(holder.ivSyncUserPhoto, 
				context.getResources().getString(R.string.url_avatar) + account.ID + account.avatar);
		holder.tvSyncUserName.setText(account.firstname);
		holder.btnSyncUserAddFriend.setTag(account);
		holder.btnSyncUserAddFriend.setOnClickListener(this);
		
		return convertView;
	}

	static class ViewHolder {
		ImageView ivSyncUserPhoto;
		TextView tvSyncUserName;
		Button btnSyncUserAddFriend;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btnSyncUserAddFriend) {
			Account account = (Account)v.getTag();
			new AddFriendAsyncTask((Button)v).execute(account);
		}
	}
	
	class AddFriendAsyncTask extends AsyncTask<Account, Void, Boolean> {
		Button btn;
		
		public AddFriendAsyncTask(Button btn) {
			this.btn = btn;
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			
			if (result) {
				btn.setText("Friend request sent");
			}
			else {
				Toast.makeText(context, "Failed to send friend request", Toast.LENGTH_SHORT).show();
			}
		}

		@Override
		protected Boolean doInBackground(Account... params) {
			UserApi api = new UserApi(context);
			try {
				return api.sendFriendRequest(sm.getCurrentSession().ID, params[0]);
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		
	}
}
