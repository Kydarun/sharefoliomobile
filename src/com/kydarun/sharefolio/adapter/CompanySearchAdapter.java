package com.kydarun.sharefolio.adapter;

import java.util.List;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.api.CompanyApi;
import com.kydarun.sharefolio.api.GroupApi;
import com.kydarun.sharefolio.obj.Company;
import com.kydarun.sharefolio.obj.Group;
import com.kydarun.sharefolio.database.DBHelper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("InflateParams")
public class CompanySearchAdapter extends AdapterBase implements OnClickListener {
	public List<Company> companies;
	public Group group;
	
	public CompanySearchAdapter(Context context, List<Company> companies) {
		super(context);
		this.companies = companies;
		this.group = null;
	}
	
	public CompanySearchAdapter(Context context, List<Company> companies, Group group) {
		this(context, companies);
		this.group = group;
	}

	@Override
	public int getCount() {
		return companies.size();
	}

	@Override
	public Object getItem(int position) {
		return companies.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 1;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.search_item, null);
			
			holder.ivSearchPicture = (ImageView)convertView.findViewById(R.id.ivSearchPicture);
			holder.tvSearchName = (TextView)convertView.findViewById(R.id.tvSearchName);
			holder.btnSearchAction = (Button)convertView.findViewById(R.id.btnSearchAction);
			holder.tvSearchStatus = (TextView)convertView.findViewById(R.id.tvSearchStatus);
			
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder)convertView.getTag();
		}
		Company company = companies.get(position);
		
		UrlImageViewHelper.setUrlDrawable(holder.ivSearchPicture, 
				context.getResources().getString(R.string.url_avatar) + "company/" + company.id + company.avatar,
				R.drawable.ic_launcher);
		holder.tvSearchName.setText(company.id + " " + company.companyCode);
		holder.btnSearchAction.setTag(company);
		holder.btnSearchAction.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_rating_important, 0, 0, 0);
		if (company.isFollowing) {
			holder.btnSearchAction.setVisibility(View.GONE);
			holder.tvSearchStatus.setText("Following");
			holder.tvSearchStatus.setVisibility(View.VISIBLE);
		}
		else {
			holder.btnSearchAction.setOnClickListener(this);
			holder.btnSearchAction.setText("Follow");
		}
		
		return convertView;
	}

	static class ViewHolder {
		ImageView ivSearchPicture;
		TextView tvSearchName;
		Button btnSearchAction;
		TextView tvSearchStatus;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btnSearchAction) {
			new FollowCompanyAsyncTask((Button)v).execute();
		}
	}
	
	class FollowCompanyAsyncTask extends AsyncTask<Void, Void, Boolean> {
		Button btnAction;
		Company company;
		
		public FollowCompanyAsyncTask(Button btnAction) {
			this.btnAction = btnAction;
		}
		
		@Override
		protected Boolean doInBackground(Void... arg0) {
			company = (Company)btnAction.getTag();
			if (group != null) {
				GroupApi api = new GroupApi(context);
				try {
					return api.followCompany(sm.getCurrentSession().ID, group.groupid, company.id);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else {
				CompanyApi api = new CompanyApi(context);
				try {
					return api.followCompany(sm.getCurrentSession().ID, company.id);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			
			if (result) {
				ViewHolder holder = (ViewHolder)((RelativeLayout)btnAction.getParent()).getTag();
				DBHelper.get(context).addCompany(company);
				holder.btnSearchAction.setVisibility(View.GONE);
				holder.tvSearchStatus.setText("Following");
				holder.tvSearchStatus.setVisibility(View.VISIBLE);
			}
			else {
				Toast.makeText(context, "Failed to follow company", Toast.LENGTH_SHORT);
			}
		}
		
	}
}
