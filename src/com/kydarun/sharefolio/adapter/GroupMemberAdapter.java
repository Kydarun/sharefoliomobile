package com.kydarun.sharefolio.adapter;

import java.util.List;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.api.GroupApi;
import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.obj.Group;
import com.kydarun.sharefolio.obj.GroupRequest;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

@SuppressLint("InflateParams")
public class GroupMemberAdapter extends AdapterBase implements OnClickListener {
	public List<Account> members;
	public List<GroupRequest> requests;
	boolean isMember;				// Flag to determine Group Request or Group Members
	Group group;
	
	public GroupMemberAdapter(Context context, List<Account> members, boolean isMember, Group group) {
		super(context);
		this.members = members;
		this.isMember = isMember;
		this.group = group;
	}
	
	public GroupMemberAdapter(Context context, boolean isMember, Group group, List<GroupRequest> requests) {
		super(context);
		this.requests = requests;
		this.isMember = isMember;
		this.group = group;
	}
	
	@Override
	public int getCount() {
		return isMember ? members.size() : requests.size();
	}

	@Override
	public Object getItem(int position) {
		return isMember ? members.get(position) : requests.get(position);
	}

	@Override
	public long getItemId(int position) {
		return isMember ? members.get(position).ID : 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.group_member_item, null);
			holder.ivGroupMemberPhoto = (ImageView)convertView.findViewById(R.id.ivGroupMemberPhoto);
			holder.tvGroupMemberName = (TextView)convertView.findViewById(R.id.tvGroupMemberName);
			holder.btnGroupMemberAction = (Button)convertView.findViewById(R.id.btnGroupMemberAction);
			holder.tvGroupMemberActionStatus = (TextView)convertView.findViewById(R.id.tvGroupMemberActionStatus);
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder)convertView.getTag();
		}
		if (!group.createdby.ID.equals(sm.getCurrentSession().ID)) {
			holder.btnGroupMemberAction.setVisibility(View.GONE);
		}
		if (isMember) {
			Account member = members.get(position);
			UrlImageViewHelper.setUrlDrawable(holder.ivGroupMemberPhoto, 
					context.getResources().getString(R.string.url_avatar) + member.ID + member.avatar);
			holder.tvGroupMemberName.setText(member.firstname);
			holder.btnGroupMemberAction.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_remove, 0, 0, 0);
			holder.btnGroupMemberAction.setText("Remove");
			holder.btnGroupMemberAction.setTag(member);
			holder.btnGroupMemberAction.setOnClickListener(this);
		}
		else {
			GroupRequest request = requests.get(position);
			UrlImageViewHelper.setUrlDrawable(holder.ivGroupMemberPhoto, 
					context.getResources().getString(R.string.url_avatar) + request.from.ID + request.from.avatar);
			holder.tvGroupMemberName.setText(request.from.firstname);
			holder.btnGroupMemberAction.setTag(request.from);
			holder.btnGroupMemberAction.setOnClickListener(this);
			holder.btnGroupMemberAction.setText("Approve");
		}
		
		return convertView;
	}

	static class ViewHolder {
		ImageView ivGroupMemberPhoto;
		TextView tvGroupMemberName;
		Button btnGroupMemberAction;
		TextView tvGroupMemberActionStatus;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btnGroupMemberAction:
				Account member = (Account)v.getTag();
				new MemberActionAsyncTask(group.groupid, (Button)v).execute(member.ID);
				break;
		}
	}
	
	class MemberActionAsyncTask extends AsyncTask<Long, Void, Boolean> {
		long groupid;
		Button btnAction;
		
		public MemberActionAsyncTask(long groupid, Button btnAction) {
			this.groupid = groupid;
			this.btnAction = btnAction;
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {
				ViewHolder holder = (ViewHolder)((RelativeLayout)btnAction.getParent()).getTag();
				btnAction.setVisibility(View.GONE);
				if (isMember) {
					holder.tvGroupMemberActionStatus.setText("Member removed");
				}
				else {
					holder.tvGroupMemberActionStatus.setText("Member added");
				}
				holder.tvGroupMemberActionStatus.setVisibility(View.VISIBLE);
			}
		}

		@Override
		protected Boolean doInBackground(Long... arg0) {
			GroupApi api = new GroupApi(context);
			long accountID = arg0[0];
			if (isMember) {
				try {
					return api.removeMember(sm.getCurrentSession().ID, groupid, accountID);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else {
				try {
					return api.processRequest(sm.getCurrentSession().ID, groupid, accountID, "A");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return false;
		}
		
	}
}
