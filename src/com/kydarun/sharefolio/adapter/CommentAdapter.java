package com.kydarun.sharefolio.adapter;

import java.util.ArrayList;
import java.util.List;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.kydarun.sharefolio.LikerFromCommentActivity;
import com.kydarun.sharefolio.LoadUserWallActivity;
import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.api.WallApi;
import com.kydarun.sharefolio.fragment.LikerFromCommentFragment;
import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.obj.Comment;
import com.kydarun.sharefolio.obj.Company;
import com.kydarun.sharefolio.util.Common;
import com.kydarun.sharefolio.util.SharefolioSpannableHandler;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CommentAdapter extends AdapterBase {
	private ArrayList<Comment> comments;
	private WallApi wa;
	private long accountID = sm.getCurrentSession().ID;
	private CommentAdapter adapter = this;

	public CommentAdapter(Context context, ArrayList<Comment> comments) {
		super(context);
		this.comments = comments;
	}

	@Override
	public int getCount() {
		return comments.size();
	}

	@Override
	public Object getItem(int arg0) {
		return comments.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.comment_item_view, parent, false);
			holder.imageView = (ImageView) convertView.findViewById(R.id.userCommentAvatar_image);
			holder.userNameAandCommentText = (TextView) convertView.findViewById(R.id.tvUserNameAndComment);
			holder.like = (Button) convertView.findViewById(R.id.btnLike);
			holder.unlike = (Button) convertView.findViewById(R.id.btnUnlike);
			holder.thumbUp = (ImageView) convertView.findViewById(R.id.ivThumbUp);
			holder.numberOfLike = (TextView) convertView.findViewById(R.id.tvShowNumberOfLike);
			holder.gap = (TextView) convertView.findViewById(R.id.tvGap);
			holder.llThumbUp = (LinearLayout) convertView.findViewById(R.id.llThumbUp);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		final Comment currentComment = comments.get(position);
		final long commentID = currentComment.commentID;

		String posterAvatarLink = "http://sharefolio.net/avatar/" + ((Account)currentComment.user).ID + ((Account)currentComment.user).avatar;
		UrlImageViewHelper.setUrlDrawable(holder.imageView, posterAvatarLink);

		String username = ((Account)currentComment.user).firstname;
		String postComment = currentComment.message;
		String nameAndComment = username + " " + postComment;
		
		nameAndComment = Html.fromHtml(nameAndComment).toString();
		
		List<Account> personTagged = currentComment.personTagged;
		List<Company> companyTagged = currentComment.companyTagged;
		
		SpannableStringBuilder ssb = new SpannableStringBuilder(nameAndComment);
		
		Log.e("personTagged", personTagged.toString());
		Log.e("companyTagged", companyTagged.toString());
		
		if (personTagged.size() != 0 || companyTagged.size() != 0) {
			ssb = SharefolioSpannableHandler.getSpannableStringBuilder(context, nameAndComment, personTagged, companyTagged);
		}
		
		int usernameStartIndex = nameAndComment.indexOf(username);
		int usernameEndIndex = username.length();
		ssb.setSpan(new StyleSpan(Typeface.BOLD), usernameStartIndex, usernameEndIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		ssb.setSpan(new ClickableSpan() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 Intent intent = new Intent(context, LoadUserWallActivity.class);
				 intent.putExtra(LoadUserWallActivity.TAG, ((Account)currentComment.user));
				 context.startActivity(intent);
			}

			@Override
			public void updateDrawState(TextPaint ds) {
				// TODO Auto-generated method stub
				ds.setUnderlineText(false);
			}
			
			
		}, usernameStartIndex, usernameEndIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		
		holder.userNameAandCommentText.setMovementMethod(LinkMovementMethod.getInstance());
		holder.userNameAandCommentText.setText(ssb);
		
		holder.like.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_rating_good, 0, 0, 0);
		holder.like.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				wa = new WallApi(context);
				try {
					new LikeCommentTask().execute(commentID);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		holder.unlike.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_rated_good, 0, 0, 0);
		holder.unlike.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				wa = new WallApi(context);
				try {
					new UnlikeCommentTask().execute(commentID);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		if (currentComment.liked) {
			holder.like.setVisibility(TextView.GONE);
			holder.unlike.setVisibility(TextView.VISIBLE);
		} else {
			holder.like.setVisibility(TextView.VISIBLE);
			holder.unlike.setVisibility(TextView.GONE);
		}
		
		if (currentComment.like.size() != 0) {
			holder.llThumbUp.setVisibility(LinearLayout.VISIBLE);
			holder.numberOfLike.setText(String.valueOf(currentComment.like.size()));
		} else {
			holder.llThumbUp.setVisibility(LinearLayout.GONE);
		}
		
		holder.llThumbUp.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(context, LikerFromCommentActivity.class);
				i.putExtra(LikerFromCommentFragment.KEY_LIKER_FROM_COMMENT, currentComment);
				context.startActivity(i);
			}
		});
		
		String gap = Common.formatDateDiff(currentComment.gap);
		holder.gap.setText(gap);
		
		return convertView;
	}
	
	private class LikeCommentTask extends AsyncTask<Long, Void, Void> {
		boolean isLikeCommentSuccess = false;
		long cID;
		Comment insideComment;
		
		@Override
		protected Void doInBackground(Long... params) {
			// TODO Auto-generated method stub
			long commentID = params[0];
			try {
				insideComment = wa.likeComment(accountID, commentID);
				isLikeCommentSuccess = wa.likeComment(accountID, commentID).liked;
				cID = wa.likeComment(accountID, commentID).commentID;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			if (isLikeCommentSuccess) {				
				for (int i = 0; i < comments.size(); i++) {
					Comment c = comments.get(i);
					if (c.commentID.equals(cID)) {
						comments.set(i, insideComment);
						break;
					}
				}
			}
			adapter.notifyDataSetChanged();
		}
	}
	
	private class UnlikeCommentTask extends AsyncTask<Long, Void, Void> {
		Comment insideComment;
		boolean isUnlikeCommentSuccess = true;
		long cID;

		@Override
		protected Void doInBackground(Long... params) {
			// TODO Auto-generated method stub
			long commentID = params[0];
			try {
				insideComment = wa.unlikeComment(accountID, commentID);
				isUnlikeCommentSuccess = wa.unlikeComment(accountID, commentID).liked;
				cID = wa.unlikeComment(accountID, commentID).commentID;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			if (isUnlikeCommentSuccess == false) {
				for (int i = 0 ; i < comments.size() ; i++) {
					Comment c = comments.get(i);
					if (c.commentID.equals(cID)) {
						comments.set(i, insideComment);
						break;
					}
				}
			}
			adapter.notifyDataSetChanged();
		}
	}
	
	static class ViewHolder {
		ImageView imageView;
		TextView userNameAandCommentText;
		Button like;
		Button unlike;
		TextView numberOfLike;
		ImageView thumbUp;
		TextView gap;
		LinearLayout llThumbUp;
	}
}
