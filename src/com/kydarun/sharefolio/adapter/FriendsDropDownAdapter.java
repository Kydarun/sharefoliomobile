package com.kydarun.sharefolio.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.obj.Account;

public class FriendsDropDownAdapter extends AdapterBase {
	List<Account> friends;
	
	public FriendsDropDownAdapter(Context context, List<Account> friends) {
		super(context);
		this.friends = friends;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return friends.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return friends.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.drop_down_list_item, parent, false);
			holder.friendAvatar = (ImageView) convertView.findViewById(R.id.ivAvatar);
			holder.friendName = (TextView) convertView.findViewById(R.id.tvName);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		Account currentFriend = friends.get(position);
		String friendAvatarLink = "http://sharefolio.net/avatar/" + currentFriend.ID + currentFriend.avatar;
		UrlImageViewHelper.setUrlDrawable(holder.friendAvatar, friendAvatarLink);
		
		holder.friendName.setText(currentFriend.firstname);
		
		return convertView;
	}
	
	static class ViewHolder {
		ImageView friendAvatar;
		TextView friendName;
	}



}
