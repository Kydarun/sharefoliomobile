package com.kydarun.sharefolio.adapter;

import com.kydarun.sharefolio.util.SessionManager;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;

public abstract class AdapterBase extends BaseAdapter {
	protected Context context;
	protected LayoutInflater inflater;
	protected SessionManager sm;

	public AdapterBase(Context context) {
		this.context = context;
		inflater = LayoutInflater.from(context);
		sm = new SessionManager(context);
	}
}
