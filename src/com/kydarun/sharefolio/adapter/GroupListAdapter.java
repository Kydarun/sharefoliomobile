package com.kydarun.sharefolio.adapter;

import java.util.List;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.obj.Group;
import com.kydarun.sharefolio.util.Common;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

@SuppressLint("InflateParams")
public class GroupListAdapter extends AdapterBase {
	public List<Group> groups;
	
	public GroupListAdapter(Context context, List<Group> groups) {
		super(context);
		this.groups = groups;
	}

	@Override
	public int getCount() {
		return this.groups.size();
	}

	@Override
	public Object getItem(int position) {
		return this.groups.get(position);
	}

	@Override
	public long getItemId(int position) {
		return this.groups.get(position).groupid;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.group_list_item, null);
			holder.ivGroupAvatar = (ImageView)convertView.findViewById(R.id.ivGroupAvatar);
			holder.tvGroupName = (TextView)convertView.findViewById(R.id.tvGroupName);
			holder.tvLastMessage = (TextView)convertView.findViewById(R.id.tvLastMessage);
			
			holder.tvGroupName.setTypeface(Common.getFontBold(context));
			holder.tvLastMessage.setTypeface(Common.getFontLight(context));
			
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder)convertView.getTag();
		}
		
		Group group = groups.get(position);
		UrlImageViewHelper.setUrlDrawable(holder.ivGroupAvatar, 
				context.getResources().getString(R.string.url_avatar) + "group/" + group.groupid + group.createdby.avatar,
				R.drawable.ic_launcher);
		holder.tvGroupName.setText(group.groupname);
		holder.tvLastMessage.setText(group.messages.size() == 0 ? "No recent messages." : group.messages.get(0).fromUser.firstname + " : " + group.messages.get(0).message);
		
		return convertView;
	}
	
	static class ViewHolder {
		ImageView ivGroupAvatar;
		TextView tvGroupName;
		TextView tvLastMessage;
	}
}
