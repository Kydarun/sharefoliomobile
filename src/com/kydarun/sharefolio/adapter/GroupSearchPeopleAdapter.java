package com.kydarun.sharefolio.adapter;

import java.util.ArrayList;
import java.util.List;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.api.GroupApi;
import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.obj.Group;
import com.kydarun.sharefolio.util.Common;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class GroupSearchPeopleAdapter extends AdapterBase implements OnClickListener {
	public List<Account> accounts;
	public List<Account> groupMembers;
	public long groupID;
	
	public GroupSearchPeopleAdapter(Context context, List<Account> accounts, long groupID) {
		super(context);
		this.accounts = accounts;
		this.groupMembers = new ArrayList<Account>();
		this.groupID = groupID;
	}

	@Override
	public int getCount() {
		return accounts.size();
	}

	@Override
	public Object getItem(int position) {
		return accounts.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.group_member_item, null);
			holder.ivGroupMemberPhoto = (ImageView)convertView.findViewById(R.id.ivGroupMemberPhoto);
			holder.tvGroupMemberName = (TextView)convertView.findViewById(R.id.tvGroupMemberName);
			holder.btnGroupMemberAction = (Button)convertView.findViewById(R.id.btnGroupMemberAction);
			holder.tvGroupMemberActionStatus = (TextView)convertView.findViewById(R.id.tvGroupMemberActionStatus);
			holder.tvGroupMemberName.setTypeface(Common.getFontRegular(context));
			holder.tvGroupMemberActionStatus.setTypeface(Common.getFontRegular(context));
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder)convertView.getTag();
		}
		
		Account account = accounts.get(position);
		UrlImageViewHelper.setUrlDrawable(holder.ivGroupMemberPhoto, 
				context.getResources().getString(R.string.url_avatar) + account.ID + account.avatar);
		holder.tvGroupMemberName.setText(account.firstname);
		
		boolean isMember = false;
		
		for (int i = 0; i < groupMembers.size(); i++) {
			Account member = groupMembers.get(i);
			if (account.ID == member.ID) {
				// Already a member
				isMember = true;
				holder.btnGroupMemberAction.setVisibility(View.INVISIBLE);
				holder.tvGroupMemberActionStatus.setVisibility(View.VISIBLE);
				holder.tvGroupMemberActionStatus.setText("Already a member");
				break;
			}
		}
		if (!isMember) {
			holder.tvGroupMemberActionStatus.setVisibility(View.INVISIBLE);
			holder.btnGroupMemberAction.setVisibility(View.VISIBLE);
			holder.btnGroupMemberAction.setText("Add to Group");
			holder.btnGroupMemberAction.setTag(account);
			holder.btnGroupMemberAction.setOnClickListener(this);
		}
		
		return convertView;
	}

	static class ViewHolder {
		ImageView ivGroupMemberPhoto;
		TextView tvGroupMemberName;
		Button btnGroupMemberAction;
		TextView tvGroupMemberActionStatus;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btnGroupMemberAction) {
			Account account = (Account)v.getTag();
			new AddMemberAsyncTask(account, (Button)v).execute();
		}
	}
	
	class AddMemberAsyncTask extends AsyncTask<Void, Void, Void> {
		Account account;
		Button btnAddMember;
		Group responseGroup;
		
		public AddMemberAsyncTask(Account account, Button btnAddMember) {
			this.account = account;
			this.btnAddMember = btnAddMember;
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			GroupApi api = new GroupApi(context);
			try {
				responseGroup = api.addMember(sm.getCurrentSession().ID, groupID, account.ID);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			
			for (int i = 0; i < responseGroup.members.size(); i++) {
				Account member = responseGroup.members.get(i);
				if (member.ID == account.ID) {
					// Added successfully
					ViewHolder holder = (ViewHolder)((RelativeLayout)btnAddMember.getParent()).getTag();
					holder.btnGroupMemberAction.setVisibility(View.INVISIBLE);
					holder.tvGroupMemberActionStatus.setVisibility(View.VISIBLE);
					holder.tvGroupMemberActionStatus.setText("Member added");
					groupMembers.add(member);
					
					return;
				}
			}
			Toast.makeText(context, "Failed to add member", Toast.LENGTH_SHORT).show();
		}
	}
}
