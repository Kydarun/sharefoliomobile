package com.kydarun.sharefolio.adapter;

import java.util.List;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.api.GroupApi;
import com.kydarun.sharefolio.obj.Group;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("InflateParams")
public class GroupSearchAdapter extends AdapterBase implements OnClickListener {
	public List<Group> groups;
	
	public GroupSearchAdapter(Context context, List<Group> groups) {
		super(context);
		this.groups = groups;
	}

	@Override
	public int getCount() {
		return groups.size();
	}

	@Override
	public Object getItem(int position) {
		return groups.get(position);
	}

	@Override
	public long getItemId(int position) {
		return groups.get(position).groupid;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.search_item, null);
			
			holder.ivSearchPicture = (ImageView)convertView.findViewById(R.id.ivSearchPicture);
			holder.tvSearchName = (TextView)convertView.findViewById(R.id.tvSearchName);
			holder.btnSearchAction = (Button)convertView.findViewById(R.id.btnSearchAction);
			holder.tvSearchStatus = (TextView)convertView.findViewById(R.id.tvSearchStatus);
			
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder)convertView.getTag();
		}
		Group group = groups.get(position);
		
		UrlImageViewHelper.setUrlDrawable(holder.ivSearchPicture, 
				context.getResources().getString(R.string.url_avatar) + "group/" + group.createdby.ID + group.createdby.avatar,
				R.drawable.ic_launcher);	// Temporarily put picture of the creator
		holder.tvSearchName.setText(group.groupname);
		
		holder.btnSearchAction.setTag(group);
		
		boolean isFound = false;
		
		for (int i = 0; i < group.members.size(); i++) {
			if (sm.getCurrentSession().ID.equals(group.members.get(i).ID)) {
				// Already in the group
				holder.btnSearchAction.setVisibility(View.GONE);
				holder.tvSearchStatus.setText("Already a member");
				holder.tvSearchStatus.setVisibility(View.VISIBLE);
				isFound = true;
				break;
			}
		}
		
		if (!isFound) {
			holder.btnSearchAction.setText("Join Group");
			holder.btnSearchAction.setOnClickListener(this);
		}
		
		return convertView;
	}

	static class ViewHolder {
		ImageView ivSearchPicture;
		TextView tvSearchName;
		Button btnSearchAction;
		TextView tvSearchStatus;
	}
	
	class RequestJoinGroupAsyncTask extends AsyncTask<Void, Void, Boolean> {
		Button btnAction;
		
		public RequestJoinGroupAsyncTask(Button btnAction) {
			this.btnAction = btnAction;
		}
		
		@Override
		protected Boolean doInBackground(Void... params) {
			GroupApi api = new GroupApi(context);
			Group group = (Group)btnAction.getTag();
			try {
				return api.requestJoinGroup(sm.getCurrentSession().ID, group.groupid);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			
			if (result) {
				ViewHolder holder = (ViewHolder)((RelativeLayout)btnAction.getParent()).getTag();
				holder.tvSearchStatus.setText("Request Sent");
				holder.tvSearchStatus.setVisibility(View.VISIBLE);
				holder.btnSearchAction.setVisibility(View.GONE);
			}
			else {
				Toast.makeText(context, "Failed to send request", Toast.LENGTH_SHORT).show();
			}
		}
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btnSearchAction) {
			new RequestJoinGroupAsyncTask((Button)v).execute();
		}
	}
}
