package com.kydarun.sharefolio.adapter;

import java.util.List;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.obj.Account;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class LikerAdapter extends AdapterBase {
	private List<Account> likers;

	public LikerAdapter(Context context, List<Account> likers) {
		super(context);
		this.likers = likers;
	}

	@Override
	public int getCount() {
		return likers.size();
	}

	@Override
	public Object getItem(int arg0) {
		return likers.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.like_item_view, parent, false);
			holder.likerAvatar = (ImageView) convertView.findViewById(R.id.ivUserLikeAvatar);
			holder.likerUsername = (TextView) convertView.findViewById(R.id.tvUserNameLike);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		Account currentLikers = likers.get(position);
		
		String likersAvatarLink = "http://sharefolio.net/avatar/" + currentLikers.ID + currentLikers.avatar;
		UrlImageViewHelper.setUrlDrawable(holder.likerAvatar, likersAvatarLink);
		
		holder.likerUsername.setText(currentLikers.firstname);
		
		return convertView;
	}
	
	static class ViewHolder {
		ImageView likerAvatar;
		TextView likerUsername;
	}

}
