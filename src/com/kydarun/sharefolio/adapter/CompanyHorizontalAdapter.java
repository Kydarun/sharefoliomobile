package com.kydarun.sharefolio.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.obj.Company;

@SuppressLint("InflateParams")
public class CompanyHorizontalAdapter extends AdapterBase {
	List<Company> companies;
	
	public CompanyHorizontalAdapter(Context context, List<Company> companies) {
		super(context);
		this.companies = companies;
	}
	
	@Override
	public int getCount() {
		return companies.size();
	}

	@Override
	public Object getItem(int position) {
		return companies.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.horizontal_item, null);
			holder.btnLink = (Button)convertView.findViewById(R.id.btnLink);
		}
		else {
			holder = (ViewHolder)convertView.getTag();
		}
		holder.btnLink.setText(companies.get(position).companyCode);
		
		return convertView;
	}

	static class ViewHolder {
		Button btnLink;
	}
}
