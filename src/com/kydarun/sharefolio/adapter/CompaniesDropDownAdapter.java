package com.kydarun.sharefolio.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.obj.Company;

public class CompaniesDropDownAdapter extends AdapterBase {
	List<Company> companies;
	
	public CompaniesDropDownAdapter(Context context, List<Company> companies) {
		super(context);
		this.companies = companies;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return companies.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return companies.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.drop_down_list_item, parent, false);
			holder.companyAvatar = (ImageView) convertView.findViewById(R.id.ivAvatar);
			holder.companyName = (TextView) convertView.findViewById(R.id.tvName);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		Company currentCompany = companies.get(position);
		
		String companyAvatartLink = "http://sharefolio.net/avatar/company/" + currentCompany.id + currentCompany.avatar;
		UrlImageViewHelper.setUrlDrawable(holder.companyAvatar, companyAvatartLink);
		
		holder.companyName.setText(currentCompany.id + "." + currentCompany.companyCode);
		return convertView;
	}
	
	static class ViewHolder {
		ImageView companyAvatar;
		TextView companyName;
	}
	
	

}
