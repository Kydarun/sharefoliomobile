 package com.kydarun.sharefolio.adapter;

import java.util.ArrayList;
import java.util.List;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.kydarun.sharefolio.FullScreenPictureActivity;
import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.obj.GroupMessage;
import com.kydarun.sharefolio.util.Common;
import com.kydarun.sharefolio.util.SharefolioSpannableHandler;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

@SuppressLint("InflateParams")
public class ChatAdapter extends AdapterBase {
	public List<GroupMessage> chats;
	
	public ChatAdapter(Context context, List<GroupMessage> chats) {
		super(context);
		this.chats = chats;
	}

	@Override
	public int getCount() {
		return chats.size();
	}

	@Override
	public Object getItem(int position) {
		return chats.get(position);
	}

	@Override
	public long getItemId(int position) {
		return chats.get(position).groupchatid;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.group_chat_item, null);
			holder.ivFromAvatar = (ImageView)convertView.findViewById(R.id.ivFromAvatar);
			holder.rlFrom = (RelativeLayout)convertView.findViewById(R.id.rlFrom);
			holder.rlTo = (RelativeLayout)convertView.findViewById(R.id.rlTo);
			holder.tvFromMessage = (TextView)convertView.findViewById(R.id.tvFromMessage);
			holder.ivFromPicture = (ImageView)convertView.findViewById(R.id.ivFromPicture);
			holder.flFromPicture = (FrameLayout)convertView.findViewById(R.id.flFromPicture);
			holder.pbFromPicture = (ProgressBar)convertView.findViewById(R.id.pbFromPicture);
			holder.tvFromName = (TextView)convertView.findViewById(R.id.tvFromName);
			holder.tvFromTimestamp = (TextView)convertView.findViewById(R.id.tvFromTimestamp);
			holder.tvMeMessage = (TextView)convertView.findViewById(R.id.tvMeMessage);
			holder.ivMePicture = (ImageView)convertView.findViewById(R.id.ivMePicture);
			holder.flMePicture = (FrameLayout)convertView.findViewById(R.id.flMePicture);
			holder.pbMePicture = (ProgressBar)convertView.findViewById(R.id.pbMePicture);
			holder.tvMeTimestamp = (TextView)convertView.findViewById(R.id.tvMeTimestamp);
			
			holder.tvFromMessage.setTypeface(Common.getFontRegular(context));
			holder.tvMeMessage.setTypeface(Common.getFontRegular(context));
			holder.tvFromName.setTypeface(Common.getFontBold(context));
			holder.tvFromTimestamp.setTypeface(Common.getFontLight(context));
			holder.tvMeTimestamp.setTypeface(Common.getFontLight(context));
			
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder)convertView.getTag();
		}
		
		final GroupMessage chat = chats.get(position);
		if (chat.fromUser.ID.equals(sm.getCurrentSession().ID)) {
			// Sender is me
			holder.rlFrom.setVisibility(View.GONE);
			if (chat.groupchatid == 0) {
				holder.tvMeMessage.setTypeface(Common.getFontRegular(context), Typeface.ITALIC);
				holder.tvMeMessage.setTextColor(Color.RED);
			} else {
				holder.tvMeMessage.setTypeface(Common.getFontRegular(context), Typeface.NORMAL);
				holder.tvMeMessage.setTextColor(Color.BLACK);
			}
			holder.tvMeTimestamp.setText(Common.getChatStamp(chat.gap));
			SpannableStringBuilder ssb = SharefolioSpannableHandler.getSpannableStringBuilder(context, chat.message, new ArrayList<Account>(), chat.companyTagged);
			holder.tvMeMessage.setMovementMethod(LinkMovementMethod.getInstance());
			holder.tvMeMessage.setText(ssb);
			holder.tvMeMessage.setText(chat.message);
			if (chat.attachment == null || chat.attachment.equals("")) {
				holder.ivMePicture.setVisibility(View.GONE);
				holder.flMePicture.setVisibility(View.GONE);
				holder.tvMeMessage.setVisibility(View.VISIBLE);
			}
			else {
				holder.tvMeMessage.setVisibility(View.GONE);
				holder.flMePicture.setVisibility(View.VISIBLE);
				holder.ivMePicture.setVisibility(View.VISIBLE);
				
				if (!chat.groupchatid.equals(0L)) {
					Ion.with(context)
					.load(context.getResources().getString(R.string.url_attachment) + "groupchat/" + chat.groupchatid + "/" + chat.attachment)
					.progressBar(holder.pbMePicture)
					.withBitmap()
					.intoImageView(holder.ivMePicture)
					.setCallback(new CustomImageViewFutureCallback(holder.flMePicture));
				}
				else {
					holder.flMePicture.setVisibility(View.VISIBLE);
					holder.ivMePicture.setVisibility(View.GONE);
				}
				// UrlImageViewHelper.setUrlDrawable(holder.ivMePicture, context.getResources().getString(R.string.url_attachment) + "groupchat/" + chat.groupchatid + "/" + chat.attachment);
			}
			holder.rlTo.setVisibility(View.VISIBLE);
			
			holder.ivMePicture.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(context, FullScreenPictureActivity.class);
					Bundle b = new Bundle();
					b.putString("url", context.getResources().getString(R.string.url_attachment) + "groupchat/" + chat.groupchatid + "/" + chat.attachment);
					intent.putExtras(b);
					context.startActivity(intent);
				}
			});
		}
		else {
			// Sender is someone else
			holder.rlTo.setVisibility(View.GONE);
			holder.rlFrom.setVisibility(View.VISIBLE);
			UrlImageViewHelper.setUrlDrawable(holder.ivFromAvatar, 
					context.getResources().getString(R.string.url_avatar) + chat.fromUser.ID + chat.fromUser.avatar, 
					R.drawable.ic_launcher);
			holder.tvFromName.setText(chat.fromUser.firstname);
			SpannableStringBuilder ssb = SharefolioSpannableHandler.getSpannableStringBuilder(context, chat.message, new ArrayList<Account>(), chat.companyTagged);
			holder.tvFromMessage.setMovementMethod(LinkMovementMethod.getInstance());
			holder.tvFromMessage.setText(ssb);
			if (chat.attachment == null || chat.attachment.equals("")) {
				holder.ivFromPicture.setVisibility(View.GONE);
				holder.flFromPicture.setVisibility(View.GONE);
				holder.tvFromMessage.setVisibility(View.VISIBLE);
			}
			else {
				holder.tvFromMessage.setVisibility(View.GONE);
				holder.flFromPicture.setVisibility(View.VISIBLE);
				holder.ivFromPicture.setVisibility(View.VISIBLE);
				Ion.with(context)
					.load(context.getResources().getString(R.string.url_attachment) + "groupchat/" + chat.groupchatid + "/" + chat.attachment)
					.progressBar(holder.pbFromPicture)
					.withBitmap()
					.smartSize(true)
					.intoImageView(holder.ivFromPicture)
					.setCallback(new CustomImageViewFutureCallback(holder.flFromPicture));
					
				// UrlImageViewHelper.setUrlDrawable(holder.ivFromPicture, context.getResources().getString(R.string.url_attachment) + "groupchat/" + chat.groupchatid + "/" + chat.attachment);
			}
			holder.tvFromTimestamp.setText(Common.getChatStamp(chat.gap));
			
			holder.ivFromPicture.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(context, FullScreenPictureActivity.class);
					Bundle b = new Bundle();
					b.putString("url", context.getResources().getString(R.string.url_attachment) + "groupchat/" + chat.groupchatid + "/" + chat.attachment);
					intent.putExtras(b);
					context.startActivity(intent);
				}
			});
		}
		
		return convertView;
	}
	
	static class ViewHolder {
		RelativeLayout rlFrom;
		RelativeLayout rlTo;
		ImageView ivFromAvatar;
		TextView tvFromName;
		TextView tvFromMessage;
		FrameLayout flFromPicture;
		ProgressBar pbFromPicture;
		ImageView ivFromPicture;
		TextView tvFromTimestamp;
		TextView tvMeMessage;
		TextView tvMeTimestamp;
		ImageView ivMePicture;
		FrameLayout flMePicture;
		ProgressBar pbMePicture;
	}
	
	class CustomImageViewFutureCallback implements FutureCallback<ImageView> {
		FrameLayout flProgress;
		
		public CustomImageViewFutureCallback(FrameLayout flProgress) {
			this.flProgress = flProgress;
		}
		
		@Override
		public void onCompleted(Exception e, ImageView result) {
			flProgress.setVisibility(View.GONE);
		}
	}
}
