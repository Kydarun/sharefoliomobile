package com.kydarun.sharefolio.adapter;

import java.util.List;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.api.UserApi;
import com.kydarun.sharefolio.database.DBHelper;
import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.obj.Request;

public class FriendRequestAdapter extends AdapterBase {
	List<Request> requesters;
	
	public FriendRequestAdapter(Context context, List<Request> requesters){
		super(context);
		this.requesters = requesters;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return requesters.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return requesters.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.friend_request_item, parent, false);
			holder.ivProfilePicture = (ImageView) convertView.findViewById(R.id.ivProfilePicture);
			holder.tvUserName = (TextView) convertView.findViewById(R.id.tvUserName);
			holder.bConfirmFriendRequest = (Button) convertView.findViewById(R.id.bConfirmFriendRequest);
			holder.bCancelFriendRequest = (Button) convertView.findViewById(R.id.bCancelFriendRequest);
			holder.llBeforeProcessFriendRequest = (LinearLayout) convertView.findViewById(R.id.llBeforeProcessFriendRequest);
			holder.llAcceptFriendRequest = (LinearLayout) convertView.findViewById(R.id.llAcceptFriendRequest);
			holder.llRejectFriendRequest = (LinearLayout) convertView.findViewById(R.id.llRejectFriendRequest);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		Request requester = requesters.get(position);
		
		final Long requestID = requester.id;
		final Account requesterAccount = requester.from;
		
		String profilePictureLink = "http://sharefolio.net/avatar/" + requesterAccount.ID + requesterAccount.avatar;
		UrlImageViewHelper.setUrlDrawable(holder.ivProfilePicture, profilePictureLink);
		
		holder.tvUserName.setText(requesterAccount.firstname);
		
		holder.bConfirmFriendRequest.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				boolean isAccept = true;
				new ProcessFriendRequestTask(requesterAccount, holder, isAccept).execute(requestID);
			}
		});
		
		holder.bCancelFriendRequest.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				boolean isAccept = false;
				new ProcessFriendRequestTask(holder, isAccept).execute(requestID);
			}
		});
		
		return convertView;
	}
	
	private class ProcessFriendRequestTask extends AsyncTask<Long, Void, Boolean> {
		Account requesterAccount;
		ViewHolder holder;
		boolean isAccept;
		boolean isSuccess;
		
		public ProcessFriendRequestTask(Account requesterAccount, ViewHolder holder, boolean isAccept) {
			this.requesterAccount = requesterAccount;
			this.holder = holder;
			this.isAccept = isAccept;
		}
		
		public ProcessFriendRequestTask(ViewHolder holder, boolean isAccept) {
			this.holder = holder;
			this.isAccept = isAccept;
		}

		@Override
		protected Boolean doInBackground(Long... params) {
			// TODO Auto-generated method stub
			UserApi ua = new UserApi(context);
			try {
				if (isAccept) {
					isSuccess = ua.processRequest(sm.getCurrentSession().ID, params[0], isAccept);
				} else {
					isSuccess = ua.processRequest(sm.getCurrentSession().ID, params[0], isAccept);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return isSuccess;
		}

		@Override
		protected void onPostExecute(Boolean isSuccess) {
			// TODO Auto-generated method stub
			if (isSuccess) {
				if (isAccept) {
					Toast.makeText(context, "Accept Success", Toast.LENGTH_SHORT).show();
					holder.llBeforeProcessFriendRequest.setVisibility(LinearLayout.GONE);
					holder.llAcceptFriendRequest.setVisibility(LinearLayout.VISIBLE);
					DBHelper.get(context).addFriend(requesterAccount);
				} else {
					Toast.makeText(context, " Reject Success", Toast.LENGTH_SHORT).show();
					holder.llBeforeProcessFriendRequest.setVisibility(LinearLayout.GONE);
					holder.llRejectFriendRequest.setVisibility(LinearLayout.VISIBLE);
				}
			} else {
				Toast.makeText(context, "Fail", Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	static class ViewHolder {
		ImageView ivProfilePicture;
		TextView tvUserName;
		Button bConfirmFriendRequest;
		Button bCancelFriendRequest;
		LinearLayout llBeforeProcessFriendRequest;
		LinearLayout llAcceptFriendRequest;
		LinearLayout llRejectFriendRequest;
	}

}
