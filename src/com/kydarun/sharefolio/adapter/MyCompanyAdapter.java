package com.kydarun.sharefolio.adapter;

import java.util.List;

import com.kydarun.sharefolio.obj.Company;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.util.Common;

import android.widget.TextView;

@SuppressLint("InflateParams")
public class MyCompanyAdapter extends AdapterBase {
	public List<Company> companies;

	public MyCompanyAdapter(Context context, List<Company> companies) {
		super(context);
		this.companies = companies;
	}

	@Override
	public int getCount() {
		return companies.size();
	}

	@Override
	public Object getItem(int position) {
		return companies.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.my_company_item, null);
			holder.tvCompanyCode = (TextView)convertView.findViewById(R.id.tvCompanyCode);
			holder.tvCompanyID = (TextView)convertView.findViewById(R.id.tvCompanyID);
			holder.tvGap = (TextView)convertView.findViewById(R.id.tvGap);
			holder.tvPrice = (TextView)convertView.findViewById(R.id.tvPrice);
			holder.tvPriceChange = (TextView)convertView.findViewById(R.id.tvPriceChange);
			holder.tvWallContent = (TextView)convertView.findViewById(R.id.tvWallContent);
			
			holder.tvCompanyCode.setTypeface(Common.getFontLight(context));
			holder.tvCompanyID.setTypeface(Common.getFontLight(context));
			holder.tvGap.setTypeface(Common.getFontLight(context));
			holder.tvPrice.setTypeface(Common.getFontLight(context));
			holder.tvPriceChange.setTypeface(Common.getFontLight(context));
			holder.tvWallContent.setTypeface(Common.getFontLight(context));
			
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder)convertView.getTag();
		}

		Company company = companies.get(position);
		
		holder.tvCompanyCode.setText(company.companyCode);
		holder.tvCompanyID.setText(company.id);
		holder.tvGap.setText(Common.formatDateDiff(company.wallpost.get(0).gap));

		String priceChange = company.price.stockChange.equals("0.00") ? "(0.00%)" : 
			Common.percentageChange(Double.parseDouble(company.price.ref), Double.parseDouble(company.price.last));
		holder.tvPrice.setText(company.price.last);
		if (priceChange.contains("+")) {
			holder.tvPrice.setTextColor(context.getResources().getColor(R.color.green));
		}
		else if (priceChange.contains("-")) {
			holder.tvPrice.setTextColor(context.getResources().getColor(R.color.red));
		}
		
		holder.tvPriceChange.setText((company.price.stockChange.equals("0.00") ? "0.000" : company.price.stockChange) + " " + priceChange);
		if (priceChange.contains("+")) {
			holder.tvPriceChange.setTextColor(context.getResources().getColor(R.color.green));
		}
		else if (priceChange.contains("-")) {
			holder.tvPriceChange.setTextColor(context.getResources().getColor(R.color.red));
		}
		
		String wallContent = company.wallpost.get(0).wallContent == null 
				|| company.wallpost.get(0).wallContent.equals("") 
				|| company.wallpost.get(0).wallContent.equals("null") ? 
						company.wallpost.get(0).announcementTitle 
						: company.wallpost.get(0).wallContent;
		holder.tvWallContent.setText(Html.fromHtml(Common.trimText(wallContent.trim(), 50)));

		return convertView;
	}

	static class ViewHolder {
		TextView tvCompanyCode;
		TextView tvCompanyID;
		TextView tvPrice;
		TextView tvPriceChange;
		TextView tvWallContent;
		TextView tvGap;
	}
}
