package com.kydarun.sharefolio.adapter;

import java.util.List;

import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.obj.Wall;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

@SuppressLint("InflateParams")
public class CompanyWallAdapter extends AdapterBase {
	public List<Wall> wallposts;
	
	public CompanyWallAdapter(Context context, List<Wall> wallposts) {
		super(context);
		this.wallposts = wallposts;
	}
	
	@Override
	public int getCount() {
		return wallposts.size();
	}

	@Override
	public Object getItem(int position) {
		return wallposts.get(position);
	}

	@Override
	public long getItemId(int position) {
		return wallposts.get(position).wallID;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.company_wall_item, null);
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		return convertView;
	}

	static class ViewHolder {
		
	}
}
