package com.kydarun.sharefolio.adapter;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.kydarun.sharefolio.CommentActivity;
import com.kydarun.sharefolio.CompanyActivity;
import com.kydarun.sharefolio.LoadUserWallActivity;
import com.kydarun.sharefolio.LikerFromWallActivity;
import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.api.WallApi;
import com.kydarun.sharefolio.fragment.CommentFragment;
import com.kydarun.sharefolio.fragment.LikerFromWallFragment;
import com.kydarun.sharefolio.obj.Comment;
import com.kydarun.sharefolio.obj.Wall;
import com.kydarun.sharefolio.util.Common;
import com.kydarun.sharefolio.util.ExpendableTextView;
import com.kydarun.sharefolio.util.SharefolioSpannableHandler;

public class WallAdapter extends AdapterBase {
	public List<Wall> walls;
	private WallApi wa;

	public WallAdapter(Context context, List<Wall> walls) {
		super(context);
		this.walls = walls;
	}

	@Override
	public int getCount() {
		return walls.size();
	}

	@Override
	public Object getItem(int arg0) {
		return walls.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.homepage_item_view, parent, false);
			holder.rlForAvatar = (RelativeLayout) convertView.findViewById(R.id.rlForAvatar);
			holder.imageView = (ImageView) convertView.findViewById(R.id.item_ivProfilePicture);
			holder.wallPoster = (TextView) convertView.findViewById(R.id.item_tvUserName);
			holder.wallPost = (ExpendableTextView) convertView.findViewById(R.id.item_tvPost);
			holder.wv = (WebView) convertView.findViewById(R.id.item_webview);
			holder.posterCompanyAnnouncementTitle = (TextView) convertView.findViewById(R.id.item_tvAnnouncementTitle);
			holder.posterCompanyAnnouncementLink = (TextView) convertView.findViewById(R.id.item_tvAnnouncementLink);
			holder.ll = (LinearLayout) convertView.findViewById(R.id.linear);
			holder.targetName = (TextView) convertView.findViewById(R.id.item_tvTarget);
			holder.like = (Button) convertView.findViewById(R.id.item_bLikeButton);
			holder.unlike = (Button) convertView.findViewById(R.id.item_bUnlikeButton);
			holder.comment = (Button) convertView.findViewById(R.id.item_bCommentButton);
			holder.gap = (TextView) convertView.findViewById(R.id.item_tvGap);
			holder.llLike = (LinearLayout) convertView.findViewById(R.id.item_llLike);
			holder.numberOfLike = (TextView) convertView.findViewById(R.id.numberOfLike);
			holder.tvLikeCount = (TextView)convertView.findViewById(R.id.tvLikeCount);
			holder.tvCommentCount = (TextView)convertView.findViewById(R.id.tvCommentCount);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		final Wall currentWall = walls.get(position);
		final long currentWallID = currentWall.wallID;

		if (currentWall.posterCompany != null) {
			holder.ll.setVisibility(LinearLayout.GONE);
			
			holder.rlForAvatar.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(context, CompanyActivity.class);
					Bundle bundle = new Bundle();
					bundle.putSerializable("company", currentWall.posterCompany);
					intent.putExtras(bundle);
					context.startActivity(intent);
				}
			});

			// Use Sharefolio default logo for the time being
			/* String posterCompanyAvatarLink = "http://sharefolio.net/avatar/company/"
					+ currentWall.posterCompany.id
					+ currentWall.posterCompany.avatar;
			UrlImageViewHelper.setUrlDrawable(holder.imageView, posterCompanyAvatarLink); */
			holder.imageView.setImageResource(R.drawable.ic_launcher);

			holder.wallPoster.setText(currentWall.posterCompany.id 
					+ "." 
					+ currentWall.posterCompany.companyCode);

			holder.wallPost.setVisibility(TextView.GONE);

			holder.wv.loadData(currentWall.wallContent, "text/html", "UTF-8");
			holder.wv.setVisibility(WebView.VISIBLE);

			holder.posterCompanyAnnouncementTitle.setVisibility(TextView.VISIBLE);
			holder.posterCompanyAnnouncementTitle.setText(currentWall.announcementTitle);

			holder.posterCompanyAnnouncementLink.setVisibility(TextView.VISIBLE);
			holder.posterCompanyAnnouncementLink.setText(currentWall.announcementLink);

		} else {
			holder.wv.setVisibility(WebView.GONE);
			holder.posterCompanyAnnouncementTitle.setVisibility(TextView.GONE);
			holder.posterCompanyAnnouncementLink.setVisibility(TextView.GONE);
			holder.wallPost.setVisibility(TextView.VISIBLE);
			holder.ll.setVisibility(LinearLayout.GONE);
			
			holder.rlForAvatar.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(context, LoadUserWallActivity.class);
					intent.putExtra(LoadUserWallActivity.TAG, currentWall.poster);
					context.startActivity(intent);
				}
			});

			String posterAvatarLink = "http://sharefolio.net/avatar/" 
					+ currentWall.poster.ID 
					+ currentWall.poster.avatar;
			UrlImageViewHelper.setUrlDrawable(holder.imageView, posterAvatarLink);

			holder.wallPoster.setText(currentWall.poster.firstname);
			
			String wallContent = Html.fromHtml(currentWall.wallContent).toString();
			holder.wallPost.setText(wallContent);
			
			holder.wallPost.setMovementMethod(LinkMovementMethod.getInstance());
			holder.wallPost.setText(SharefolioSpannableHandler.getSpannableStringBuilder(context, wallContent, currentWall.personTagged, currentWall.companyTagged));
			
			

			if (currentWall.targetCompany != null) {
				holder.targetName.setText(currentWall.targetCompany.id 
						+ "." 
						+ currentWall.targetCompany.companyCode);
				holder.ll.setVisibility(LinearLayout.VISIBLE);
			} else if (currentWall.target != null) {
				holder.targetName.setText(currentWall.target.firstname);
				holder.ll.setVisibility(LinearLayout.VISIBLE);
			}
		}
		
		holder.like.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_rating_good, 0, 0, 0);
		holder.like.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				wa = new WallApi(context);
				new LikePostTask().execute(currentWallID);
			}
		});

		holder.unlike.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_rated_good, 0, 0, 0);
		holder.unlike.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				wa = new WallApi(context);
				new UnlikePostTask().execute(currentWallID);
			}
		});

		holder.llLike.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i = new Intent(context, LikerFromWallActivity.class);
				i.putExtra(LikerFromWallFragment.EXTRA_CURRENT_WALL, currentWall);
				context.startActivity(i);
			}
		});

		holder.comment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, CommentActivity.class);
				intent.putExtra(CommentFragment.KEY_WALLID, currentWall.wallID);
				intent.putExtra(CommentFragment.KEY_ACCOUNTID, sm.getCurrentSession().ID);
				context.startActivity(intent);
			}
		});

		String gap = Common.formatDateDiff(currentWall.gap);
		holder.gap.setText(gap);

		if (currentWall.like.size() != 0) {
			holder.llLike.setVisibility(LinearLayout.VISIBLE);
			holder.numberOfLike.setText(String.valueOf(currentWall.like.size()));
			holder.tvLikeCount.setText(currentWall.like.size() + " likes");

		} else {
			holder.llLike.setVisibility(LinearLayout.GONE);
			holder.tvLikeCount.setText("");
		}
		
		if (currentWall.comments.size() != 0) {
			holder.tvCommentCount.setText(currentWall.comments.size() + " comments");
		}
		else {
			holder.tvCommentCount.setText("");
		}
		

		if (currentWall.liked) {
			holder.unlike.setVisibility(Button.VISIBLE);
			holder.like.setVisibility(Button.GONE);
		} else {
			holder.like.setVisibility(Button.VISIBLE);
			holder.unlike.setVisibility(Button.GONE);
		}

		return convertView;
	}
	
	private class LikePostTask extends AsyncTask<Long, Void, Void> {
		long wID;
		Wall insideWall;
		boolean isLikePostSuccess;

		@Override
		protected Void doInBackground(Long... params) {
			// TODO Auto-generated method stub
			wID = params[0];
			try {
				insideWall = wa.likePost(sm.getCurrentSession().ID, wID);
				isLikePostSuccess = insideWall.liked;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			if (isLikePostSuccess) {
				//WallBank.get(context).updateWall(insideWall, wID);
				updateWall(insideWall, wID);
			}
			//adapter.notifyDataSetChanged();
		}
	}
	
	private class UnlikePostTask extends AsyncTask<Long, Void, Void> {
		long wID;
		Wall insideWall;
		boolean isLikePostSuccess;

		@Override
		protected Void doInBackground(Long... params) {
			// TODO Auto-generated method stub
			wID = params[0];
			try {
				insideWall = wa.unlikePost(sm.getCurrentSession().ID, wID);
				isLikePostSuccess = insideWall.liked;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			if (isLikePostSuccess == false) {
				updateWall(insideWall, wID);
			}
		}
	}
	
	public void updateWall(Wall wall, long wallID) {
		for (int i = 0 ; i < walls.size() ; i++) {
			if (walls.get(i).wallID.equals(wallID)) {
				walls.set(i, wall);
				break;
			}
		}
		this.notifyDataSetChanged();
	}

	static class ViewHolder {
		RelativeLayout rlForAvatar;
		ImageView imageView;
		TextView wallPoster;
		ExpendableTextView wallPost;
		WebView wv;
		TextView posterCompanyAnnouncementTitle;
		TextView posterCompanyAnnouncementLink;
		TextView targetName;
		ImageButton likeAndCommentButton;
		Button like;
		Button unlike;
		Button comment;
		LinearLayout llLike;
		TextView numberOfLike;
		TextView tvLikeCount;
		TextView tvCommentCount;
		TextView gap;
		List<Comment> commentList;
		LinearLayout ll;
	}

}
