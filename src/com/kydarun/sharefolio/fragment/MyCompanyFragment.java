package com.kydarun.sharefolio.fragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.FrameLayout;

import com.costum.android.widget.PullToRefreshListView;
import com.costum.android.widget.PullToRefreshListView.OnRefreshListener;
import com.kydarun.sharefolio.CompanyActivity;
import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.SearchActivity;
import com.kydarun.sharefolio.SettingActivity;
import com.kydarun.sharefolio.adapter.CompanySearchAdapter;
import com.kydarun.sharefolio.adapter.MyCompanyAdapter;
import com.kydarun.sharefolio.api.CompanyApi;
import com.kydarun.sharefolio.constant.EnvironmentalConstant;
import com.kydarun.sharefolio.constant.PageNameConstant;
import com.kydarun.sharefolio.database.DBHelper;
import com.kydarun.sharefolio.obj.Company;
import com.kydarun.sharefolio.util.Common;

@SuppressLint("InflateParams")
public class MyCompanyFragment extends BaseFragment implements OnRefreshListener, OnItemClickListener, OnClickListener {

	PullToRefreshListView lvMyCompany;
	MyCompanyAdapter adapter;
	CompanySearchAdapter companySearchAdapter;
	Button btnMyCompanyAdd;
	
	List<Company> companySuggestions = new ArrayList<Company>();
	
	public MyCompanyFragment() {
		super();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		View v = inflater.inflate(R.layout.my_company, container, false);
		
		flProgress = (FrameLayout)v.findViewById(R.id.flProgress);

		lvMyCompany = (PullToRefreshListView)v.findViewById(R.id.lvMyCompany);
		if (lvMyCompany != null) {
			lvMyCompany.setOnRefreshListener(this);
			lvMyCompany.setOnItemClickListener(this);
		}
		
		btnMyCompanyAdd = (Button)v.findViewById(R.id.btnMyCompanyAdd);
		if (btnMyCompanyAdd != null) {
			btnMyCompanyAdd.setOnClickListener(this);
		}
		
		return v;
	}

	@Override
	public void onResume() {
		super.onResume();
		flProgress.setVisibility(View.VISIBLE);
		lvMyCompany.setAdapter(adapter);
		btnMyCompanyAdd.setVisibility(Button.VISIBLE);
		new LoadMyCompanyAsyncTask().execute();
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		inflater.inflate(R.menu.setting, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.menu_item_search:
			Intent i = new Intent(context, SearchActivity.class);
			Bundle bundle = new Bundle();
			bundle.putString("PageName", PageNameConstant.MY_COMPANY);
			i.putExtras(bundle);
			context.startActivity(i);
			return true;
		case R.id.menu_item_setting:
			Intent intent = new Intent(context, SettingActivity.class);
			context.startActivity(intent);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
		
	}

	class LoadMyCompanyAsyncTask extends AsyncTask<Void, Void, List<Company>> {
		DBHelper db = DBHelper.get(context);
		
		@Override
		protected List<Company> doInBackground(Void... arg0) {
			CompanyApi api = new CompanyApi(context);
			
			try {
				if (db.getAllCompanies().size() == 0 || 
						Common.getSetting(context, "last_updated", 0L) == 0L || 
						Common.getSetting(context, "last_updated", 0L) + EnvironmentalConstant.FETCH_PRICE_INTERVAL > Calendar.getInstance().getTimeInMillis()) {
					return api.getMyCompanyList(sm.getCurrentSession().ID);
				}
				else {
					return db.getAllCompanies();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return null;
		}

		@Override
		protected void onPostExecute(List<Company> result) {
			super.onPostExecute(result);
			if (adapter == null) {
				adapter = new MyCompanyAdapter(context, result == null ? new ArrayList<Company>() : result);
				lvMyCompany.setAdapter(adapter);
			}
			else {
				adapter.companies = result;
				adapter.notifyDataSetChanged();
				lvMyCompany.onRefreshComplete();
			}
			
			if (result.size() == 0) {
				companySearchAdapter = new CompanySearchAdapter(context, companySuggestions);
				lvMyCompany.setAdapter(companySearchAdapter);
				new LoadCompanySuggestionTask().execute();
			} else {
				flProgress.setVisibility(View.GONE);
			}
			
			if (db.getAllCompanies().size() == 0 || 
					Common.getSetting(context, "last_updated", 0L) == 0L || 
					Common.getSetting(context, "last_updated", 0L) + EnvironmentalConstant.FETCH_PRICE_INTERVAL > Calendar.getInstance().getTimeInMillis()) {
				Common.setSetting(context, "last_updated", Calendar.getInstance().getTimeInMillis());
				new UpdateCompanyCacheAsyncTask(result).execute();
			}
		}
	}
	
	class UpdateCompanyCacheAsyncTask extends AsyncTask<Void, Void, Void> {
		List<Company> companies;

		public UpdateCompanyCacheAsyncTask(List<Company> companies) {
			this.companies = companies;
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			DBHelper db = DBHelper.get(context);
			for (Company c: companies) {
				db.deleteCompany(c);
				db.addCompany(c);
			}
			return null;
		}		
	}
	
	class LoadCompanySuggestionTask extends AsyncTask<Void, Void, List<Company>> {

		@Override
		protected List<Company> doInBackground(Void... params) {
			// TODO Auto-generated method stub
			CompanyApi ca = new CompanyApi(context);
			companySuggestions.clear();
			try {
				return ca.getCompanySuggestion(sm.getCurrentSession().ID);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}

		@Override
		protected void onPostExecute(List<Company> result) {
			// TODO Auto-generated method stub
			btnMyCompanyAdd.setVisibility(Button.GONE);
			flProgress.setVisibility(View.GONE);
			companySuggestions.addAll(result);
			companySearchAdapter.notifyDataSetChanged();
		}
	}

	@Override
	public void onRefresh() {
		flProgress.setVisibility(View.VISIBLE);
		Common.setSetting(context, "last_updated", 0L);
		new LoadMyCompanyAsyncTask().execute();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		if (id == 0) {
			Company company = adapter.companies.get(position - 1);
			Intent intent = new Intent(getActivity(), CompanyActivity.class);
			Bundle b = new Bundle();
			b.putSerializable("company", company);
			intent.putExtras(b);
			
			getActivity().startActivity(intent);
		} else if (id == 1){
			Company company = companySearchAdapter.companies.get(position - 1);
			Intent intent = new Intent(getActivity(), CompanyActivity.class);
			Bundle b = new Bundle();
			b.putSerializable("company", company);
			intent.putExtras(b);
			
			getActivity().startActivity(intent);
		}
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btnMyCompanyAdd) {
			Intent i = new Intent(context, SearchActivity.class);
			Bundle b = new Bundle();
			b.putString("PageName", PageNameConstant.MY_COMPANY);
			i.putExtras(b);
			startActivity(i);
		}
	}
}
