package com.kydarun.sharefolio.fragment;

import java.util.ArrayList;
import java.util.List;

import com.costum.android.widget.PullToRefreshListView;
import com.costum.android.widget.PullToRefreshListView.OnRefreshListener;
import com.kydarun.sharefolio.ChatActivity;
import com.kydarun.sharefolio.CreateGroupActivity;
import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.SearchActivity;
import com.kydarun.sharefolio.SettingActivity;
import com.kydarun.sharefolio.adapter.GroupListAdapter;
import com.kydarun.sharefolio.api.GroupApi;
import com.kydarun.sharefolio.constant.PageNameConstant;
import com.kydarun.sharefolio.obj.Group;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;

@SuppressLint("InflateParams")
public class GroupListFragment extends BaseFragment implements OnRefreshListener, OnItemClickListener, OnClickListener {
	
	PullToRefreshListView lvGroupList;
	GroupListAdapter adapter;
	Button btnCreateGroup;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		View v = inflater.inflate(R.layout.group_list, container, false);
		lvGroupList = (PullToRefreshListView)v.findViewById(R.id.lvGroupList);
		if (lvGroupList != null) {
			lvGroupList.setEmptyView(v.findViewById(R.id.vEmpty));
			lvGroupList.setOnRefreshListener(this);
			lvGroupList.setOnItemClickListener(this);
		}
		
		btnCreateGroup = (Button)v.findViewById(R.id.btnCreateGroup);
		if (btnCreateGroup != null) {
			btnCreateGroup.setOnClickListener(this);
		}
		
		flProgress = (FrameLayout)v.findViewById(R.id.flProgress);
		
		return v;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		inflater.inflate(R.menu.setting, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.menu_item_search:
			Intent i = new Intent(context, SearchActivity.class);
			Bundle bundle = new Bundle();
			bundle.putString("PageName", PageNameConstant.MY_CHAT);
			i.putExtras(bundle);
			context.startActivity(i);
			return true;
		case R.id.menu_item_setting:
			Intent intent = new Intent(context, SettingActivity.class);
			context.startActivity(intent);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
		
	}

	@Override
	public void onResume() {
		super.onResume();
		flProgress.setVisibility(View.VISIBLE);
		new GroupListAsyncTask().execute();
	}

	class GroupListAsyncTask extends AsyncTask<Void, Void, List<Group>> {

		@Override
		protected void onPostExecute(List<Group> result) {
			super.onPostExecute(result);
			
			if (adapter == null) {
				adapter = new GroupListAdapter(context, result == null ? new ArrayList<Group>() : result);
				lvGroupList.setAdapter(adapter);
			}
			else {
				adapter.groups = result;
				adapter.notifyDataSetChanged();
				lvGroupList.onRefreshComplete();
			}
			
			flProgress.setVisibility(View.GONE);
		}

		@Override
		protected List<Group> doInBackground(Void... params) {
			try {
				GroupApi api = new GroupApi(context);
				return api.getMyGroup(sm.getCurrentSession().ID);
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
			return null;
		}
	}

	@Override
	public void onRefresh() {
		new GroupListAsyncTask().execute();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Group group = adapter.groups.get(position - 1);
		
		Intent intent = new Intent(getActivity(), ChatActivity.class);
		Bundle b = new Bundle();
		b.putSerializable("group", group);
		intent.putExtras(b);
		
		getActivity().startActivity(intent);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btnCreateGroup) {
			Intent intent = new Intent(context, CreateGroupActivity.class);
			startActivity(intent);
		}
	}
}
