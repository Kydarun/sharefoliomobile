package com.kydarun.sharefolio.fragment;

import java.io.Serializable;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

import com.costum.android.widget.PullAndLoadListView;
import com.costum.android.widget.PullAndLoadListView.OnLoadMoreListener;
import com.costum.android.widget.PullToRefreshListView.OnRefreshListener;
import com.kydarun.sharefolio.PostWallActivity;
import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.SearchActivity;
import com.kydarun.sharefolio.SettingActivity;
import com.kydarun.sharefolio.adapter.WallAdapter;
import com.kydarun.sharefolio.api.WallApi;
import com.kydarun.sharefolio.constant.PageNameConstant;
import com.kydarun.sharefolio.obj.Wall;

@SuppressLint("InflateParams")
public class HomeFragment extends BaseFragment implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6861590608150793881L;
	
	WallAdapter adapter;
	ArrayList<Wall> walls = new ArrayList<Wall>();
	PullAndLoadListView homePageListView;
	Button btnNewPost;
	long accountID;
	int starting = 0;
	final int NUMBER_OF_RESULT = 10;
	WallApi wa;
	FrameLayout flNewPost;
	
	float startY;

	public HomeFragment() {
		super();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		wa = new WallApi(context);
		accountID = sm.getCurrentSession().ID;
		
		View v = inflater.inflate(R.layout.homepage, null);
		
		flNewPost = (FrameLayout) v.findViewById(R.id.flNewPost);
		
		adapter = new WallAdapter(getActivity(), walls);
		homePageListView = (PullAndLoadListView) v.findViewById(R.id.lvHomePage);
		homePageListView.setAdapter(adapter);
		
		homePageListView.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				int mDelta;
				
				// TODO Auto-generated method stub
				float y = event.getY();
				float x = event.getX();
				
				int action = event.getAction();
				
				switch(action) {
				case MotionEvent.ACTION_DOWN:
					startY = y;
					break;
				case MotionEvent.ACTION_MOVE:
					mDelta = (int)(y - startY);
					if (mDelta < 0) {
						Log.d("scrollUp", "scroll up");
						if (flNewPost.getVisibility() == FrameLayout.VISIBLE) {
							flNewPost.setVisibility(FrameLayout.GONE);
						}
					} else {
						if (flNewPost.getVisibility() == FrameLayout.GONE) {
							flNewPost.setVisibility(FrameLayout.VISIBLE);
						}
						Log.d("scrollDown", "scroll down");
					}
					break;
				case MotionEvent.ACTION_UP:
					break;
				}	
				return false;
			}
		});
		
		
		flProgress = (FrameLayout)v.findViewById(R.id.flProgress);
		
		homePageListView.setOnRefreshListener(new OnRefreshListener() {
			// Do work to refresh the list here.
			public void onRefresh() { 
				starting = 0;
				flProgress.setVisibility(View.VISIBLE);
				new RefreshWallTask().execute();
				//new RefreshWallTask(true).execute();
			}
		});
	 
		homePageListView.setOnLoadMoreListener(new OnLoadMoreListener() {
			// Do the work to load more items at the end of list here.
			 public void onLoadMore() { 
				 starting += NUMBER_OF_RESULT;
				 new LoadMoreWallsTask().execute();
			 }
		});
		
		btnNewPost = (Button)v.findViewById(R.id.btnNewPost);
		btnNewPost.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, PostWallActivity.class);
				startActivity(intent);
			}
		});

		return v;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		inflater.inflate(R.menu.setting, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.menu_item_search:
			Intent i = new Intent(context, SearchActivity.class);
			Bundle bundle = new Bundle();
			bundle.putString("PageName", PageNameConstant.HOME_PAGE);
			i.putExtras(bundle);
			context.startActivity(i);
			return true;
		case R.id.menu_item_setting:
			Intent intent = new Intent(context, SettingActivity.class);
			context.startActivity(intent);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
		
	}

	@Override
	public void onResume() {
		super.onResume();
		flProgress.setVisibility(View.VISIBLE);
		starting = 0;
		new RefreshWallTask().execute();
		//new RefreshWallTask(false).execute();
	}

	private class LoadMoreWallsTask extends AsyncTask<Void, Void, Void> {
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub	
			if (isCancelled()) {
				return null;
			}
			try {
				//Wall wallpost;
				walls.addAll(wa.loadHome(accountID, starting, NUMBER_OF_RESULT));
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			adapter.notifyDataSetChanged();
			homePageListView.onLoadMoreComplete();
			
			flProgress.setVisibility(View.GONE);
		}
		
		protected void onCancelled() {
			// Notify the loading more operation has finished
			homePageListView.onLoadMoreComplete();
		}
	}
	
	private class RefreshWallTask extends AsyncTask<Void, Void, Void> {
	//private class RefreshWallTask extends AsyncTask<Void, Void, List<Wall>> {
		
	//	boolean updateCache;
		//DBHelper db = DBHelper.get(context);
		
		//public RefreshWallTask (boolean updateCache) {
		//	this.updateCache = updateCache;
		//}
		
		@Override
		protected Void doInBackground(Void... params) {
		//protected List<Wall> doInBackground(Void... params) {
			// TODO Auto-generated method stub
			if (isCancelled()) {
				return null;
			}
			walls.clear();
			try {
				walls.addAll(wa.loadHome(accountID, starting, NUMBER_OF_RESULT));
				//if (db.getAllWallPosts().size() == 0 || updateCache) {
					
					//if(walls.addAll(wa.loadHome(accountID, starting, NUMBER_OF_RESULT)))
					//{
				
						//return walls;
					//}
			//	}
				//else {
					//return db.getAllWallPosts();
				//}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
		//protected void onPostExecute(List<Wall> result) {
			// TODO Auto-generated method stub
			//adapter.walls = result;
			adapter.notifyDataSetChanged();
			homePageListView.onRefreshComplete();
			flProgress.setVisibility(View.GONE);
			
			//if (db.getAllWallPosts().size() == 0 || updateCache) {
				//new UpdateWallPostCacheAsyncTask(result).execute();
			//}
		}
		
		protected void onCancelled() {
			// Notify the loading more operation has finished
			homePageListView.onLoadMoreComplete();
		}
	}
	
	//class UpdateWallPostCacheAsyncTask extends AsyncTask<Void, Void, Void> {
	//	List<Wall> wallposts;

	//	public UpdateWallPostCacheAsyncTask(List<Wall> result) {
		//	this.wallposts = result;
		//}
		
	//	@Override
	//	protected Void doInBackground(Void... params) {
		//	DBHelper db = DBHelper.get(context);
			//for (Wall wa: wallposts) {
				//db.deleteCompany(c);
			//	db.addWallpost(wa);
			//}
		//	return null;
		//}		
	//}

}
