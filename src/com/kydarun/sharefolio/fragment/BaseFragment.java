package com.kydarun.sharefolio.fragment;

import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.util.SessionManager;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.TextView;

public abstract class BaseFragment extends Fragment {
	protected Context context;
	protected SessionManager sm;
	protected Account account;
	protected FrameLayout flProgress;
	
	protected TextView tvTitle;

	public BaseFragment() {
		super();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.context = getActivity();
		this.sm = new SessionManager(context);
	}
}
