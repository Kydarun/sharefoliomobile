package com.kydarun.sharefolio.fragment;

import java.util.Calendar;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.kydarun.sharefolio.BaseActivity;
import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.SearchActivity;
import com.kydarun.sharefolio.api.CompanyApi;
import com.kydarun.sharefolio.constant.EnvironmentalConstant;
import com.kydarun.sharefolio.constant.PageNameConstant;
import com.kydarun.sharefolio.database.DBHelper;
import com.kydarun.sharefolio.obj.Company;
import com.kydarun.sharefolio.util.Common;

@SuppressLint("InflateParams")
public class HeaderFragment extends BaseFragment implements OnClickListener {
	protected Calendar lastUpdated;
	List<Company> companies;
	TextView tvPriceTicker;
	public LoadPriceAsyncTask task;
	BaseActivity callingActivity;
	ImageButton btnAddCompany;
	
	public String pageName;
	
	ImageButton ibHome;
	public ImageButton ibSetting;
	public ImageButton ibSearch;
	
	public HeaderFragment(BaseActivity callingActivity) {
		super();
		this.callingActivity = callingActivity;
	}
	
	public HeaderFragment() {
		super();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.header, null);
		
		tvPriceTicker = (TextView)v.findViewById(R.id.tvPriceTicker);
		if (tvPriceTicker != null) {
			tvPriceTicker.setTypeface(Common.getFontBold(context));
			tvPriceTicker.setText("Loading...");
			tvPriceTicker.setSelected(true);
		}
		
		task = new LoadPriceAsyncTask();
		task.execute();
		
		btnAddCompany = (ImageButton)v.findViewById(R.id.btnAddCompany);
		if (btnAddCompany != null) {
			btnAddCompany.setOnClickListener(this);
		}
		if (ibHome != null && ibSetting != null && ibSearch != null) {
			ibHome.setOnClickListener(this);
			if (pageName == null) {
				ibSearch.setVisibility(View.GONE);
				ibSetting.setVisibility(View.GONE);
			}
			else if (pageName.equals(PageNameConstant.MY_COMPANY)
					|| pageName.equals(PageNameConstant.HOME_PAGE)
					|| pageName.equals(PageNameConstant.MY_CHAT)) {
				ibSearch.setOnClickListener(this);
				ibSetting.setOnClickListener(this);
			}
			else {
				ibSearch.setVisibility(View.GONE);
				ibSetting.setVisibility(View.GONE);
			}
		}
		
		return v;
	}
	
	/* Re-fetch the latest price after a certain interval (Defined in EnvironmentalConstant) */
	public void getLatestPrice() {
		task = new LoadPriceAsyncTask();
		task.execute();
	}
	
	/* Is an async task running? */
	public boolean isTaskFinished() {
		return task.isFinished;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		task.isFinished = true;
		task.cancel(true);
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btnAddCompany:
				Intent i = new Intent(context, SearchActivity.class);
				Bundle b = new Bundle();
				b.putString("PageName", PageNameConstant.MY_COMPANY);
				i.putExtras(b);
				startActivity(i);
				break;
		}
	}

	class LoadPriceAsyncTask extends AsyncTask<Void, Void, Void> {
		boolean isFinished;
		DBHelper db;
		CompanyApi api;

		public LoadPriceAsyncTask() {
			this.isFinished = false;
			api = new CompanyApi(context);
			db = DBHelper.get(context);
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			try {
				if (db.getAllCompanies().size() == 0 || 
						Common.getSetting(callingActivity, "last_updated", 0L) == 0L || 
						Common.getSetting(callingActivity, "last_updated", 0L) + EnvironmentalConstant.FETCH_PRICE_INTERVAL > Calendar.getInstance().getTimeInMillis()) {
					companies = api.getMyCompanyList(sm.getCurrentSession().ID);
				}
				else {
					companies = db.getAllCompanies();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			
			String tickerString = "";
			
			for (int i = 0; i < companies.size(); i++) {
				Company co = companies.get(i);
				String priceText = co.companyCode + " " + co.price.last + " (" + co.price.stockChange + ")";
				if (co.price.stockChange.contains("+")) {
					tickerString += "<font color=\"#006600\">" + priceText + "</font> ";
				}
				else if (co.price.stockChange.contains("-")) {
					tickerString += "<font color=\"#FF0000\">" + priceText + "</font> ";
				}
				else {
					tickerString += "<font color=\"#000000\">" + priceText + "</font> ";
				}
			}
			Spanned fromHtml = Html.fromHtml(tickerString);
			if (!tvPriceTicker.getText().toString().equals(fromHtml.toString())) {
				tvPriceTicker.setText(Html.fromHtml(tickerString));
			}
			
			isFinished = true;
			lastUpdated = Calendar.getInstance();
			
			if (db.getAllCompanies().size() == 0 || 
					Common.getSetting(callingActivity, "last_updated", 0L) == 0L || 
					Common.getSetting(callingActivity, "last_updated", 0L) + EnvironmentalConstant.FETCH_PRICE_INTERVAL > Calendar.getInstance().getTimeInMillis()) {
				Common.setSetting(callingActivity, "last_updated", Calendar.getInstance().getTimeInMillis());
				new CacheCompanyAsyncTask().execute();
			}
		}
	}
	
	class CacheCompanyAsyncTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			DBHelper db = DBHelper.get(context);
			for (Company c: companies) {
				db.deleteCompany(c);
				db.addCompany(c);
			}
			return null;
		}
	}
}
