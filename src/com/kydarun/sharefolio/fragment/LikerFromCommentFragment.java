package com.kydarun.sharefolio.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.adapter.LikerAdapter;
import com.kydarun.sharefolio.obj.Comment;

public class LikerFromCommentFragment extends Fragment {
	Comment currentComment;
	ListView likersListView;
	public static final String KEY_LIKER_FROM_COMMENT = "com.kydarun.sharefolio.LikerFromCommentFragment.currentComment";
	
	public static LikerFromCommentFragment newInstance(Comment comment) {
		Bundle args = new Bundle();
		args.putSerializable(KEY_LIKER_FROM_COMMENT, comment);
		
		LikerFromCommentFragment fragment = new LikerFromCommentFragment();
		fragment.setArguments(args);
		
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		currentComment = (Comment) getArguments().getSerializable(KEY_LIKER_FROM_COMMENT);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.like_list, container, false);
		
		getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
		
		likersListView = (ListView) v.findViewById(R.id.lvLiker);
		LikerAdapter adapter = new LikerAdapter(getActivity(), currentComment.like);
		likersListView.setAdapter(adapter);
		
		return v;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId()) {
		case android.R.id.home:
			getActivity().onBackPressed();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
		
	}

}
