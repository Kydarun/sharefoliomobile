package com.kydarun.sharefolio.fragment;

import java.util.ArrayList;
import java.util.List;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListPopupWindow;
import android.widget.ListView;
import android.widget.Toast;

import com.costum.android.widget.PullAndLoadListView;
import com.costum.android.widget.PullToRefreshListView.OnRefreshListener;
import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.adapter.CommentAdapter;
import com.kydarun.sharefolio.api.WallApi;
import com.kydarun.sharefolio.database.DBHelper;
import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.obj.Comment;
import com.kydarun.sharefolio.obj.CommentBank;
import com.kydarun.sharefolio.obj.Company;
import com.kydarun.sharefolio.util.SuggestionList;

public class CommentFragment extends Fragment {
	public static final String KEY_WALLID = "com.kydarun.sharefolio.wallID";
	public static final String KEY_ACCOUNTID = "com.kydarun.sharefolio.accountID";
	
	List<Comment> commentList;
	List<Comment> currentList = new ArrayList<Comment>();
	List<Comment> checkLatestComment = new ArrayList<Comment>();
	
	CommentAdapter adapter;
	Comment comment;
	PullAndLoadListView commentListView;
	ImageButton postCommentButton;
	EditText postCommentText;
	ListPopupWindow popupWindow;
	String commentPosted;
	String commentBeforeConvertID;
	long wallID;
	long accountID;
	WallApi waComment;
	CommentBank cb;
	
	FrameLayout flProgress;

	List<Account> friends = new ArrayList<Account>();
	List<Company> companies = new ArrayList<Company>();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		wallID = getArguments().getLong(KEY_WALLID);
		accountID = getArguments().getLong(KEY_ACCOUNTID);
		
		friends = DBHelper.get(getActivity()).getAllFriends();
		companies = DBHelper.get(getActivity()).getAllCompanies();	
	}

	public static CommentFragment newInstance(long wallID, long accountID) { 
		Bundle args = new Bundle();
		args.putLong(KEY_WALLID, wallID);
		args.putLong(KEY_ACCOUNTID, accountID);
		
		CommentFragment fragment = new CommentFragment();
		fragment.setArguments(args);
		
		return fragment;
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.comment, container, false);
		
		getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);

		flProgress = (FrameLayout)v.findViewById(R.id.flProgress);
		cb = new CommentBank(getActivity(), wallID, flProgress);
		
		adapter = new CommentAdapter(getActivity(), cb.getComments());
		commentListView = (PullAndLoadListView) v.findViewById(R.id.lvComment);
		commentListView.setAdapter(adapter);
		commentListView.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);
		
		cb.setupCommentBank(accountID, adapter, commentListView);
		
		commentListView.setOnRefreshListener(new OnRefreshListener() {
			
			public void onRefresh() {
				// Do work to refresh the list here.
				cb.onLoadMoreComment();
			}
		});
		
		postCommentText = (EditText) v.findViewById(R.id.etComment);
		
		popupWindow = new ListPopupWindow(getActivity());
		popupWindow.setAnchorView(postCommentText);
		popupWindow.setInputMethodMode(ListPopupWindow.INPUT_METHOD_NEEDED);
		
		postCommentText.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				SuggestionList.loadSuggestionList(getActivity(), popupWindow, s, friends, companies, postCommentText);
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		postCommentButton = (ImageButton) v.findViewById(R.id.bPostComment);
		postCommentButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub		
				commentPosted = postCommentText.getText().toString();
				commentBeforeConvertID = commentPosted;
				
				if (!isEmpty(commentPosted)) {
					postCommentText.setEnabled(false);
					postCommentButton.setEnabled(false);
					
					comment = new Comment();
					comment.user = new Account();
					((Account)comment.user).ID = accountID;
					
					comment.personTagged = SuggestionList.loadFinalFriendsTag(commentPosted);
					comment.companyTagged = SuggestionList.loadFinalCompaniesTag(commentPosted);
					
					if (comment.personTagged.size() != 0) {
						for (int i = 0 ; i < comment.personTagged.size() ; i++) {
							if (commentPosted.contains(comment.personTagged.get(i).firstname)) {
								commentPosted = commentPosted.replace(comment.personTagged.get(i).firstname, comment.personTagged.get(i).ID.toString());
							}
						}
					}
					
					if (comment.companyTagged.size() != 0) {
						for (int i = 0 ; i < comment.companyTagged.size() ; i++) {
							if (commentPosted.contains(comment.companyTagged.get(i).companyCode)) {
								commentPosted = commentPosted.replace(comment.companyTagged.get(i).companyCode, comment.companyTagged.get(i).id);
							}
						}
					}
					
					comment.wallID = wallID;
					comment.message = commentPosted;		
					new SubmitCommentTask().execute();
				} else {
					Toast.makeText(getActivity(), "Please enter your comment...", Toast.LENGTH_SHORT).show();
				}
			}
		});	
		
		View vEmpty = v.findViewById(R.id.vEmpty);
		commentListView.setEmptyView(vEmpty);
		vEmpty.setVisibility(View.GONE);

		return v;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId()) {
		case android.R.id.home:
			if (NavUtils.getParentActivityName(getActivity()) != null) {
				NavUtils.navigateUpFromSameTask(getActivity());
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public static boolean isEmpty(String commentPosted) {
		if (commentPosted.trim().length() != 0) {
			return false;
		} else {
			return true;
		}
	}
	
	public class SubmitCommentTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			checkLatestComment.clear();
			waComment = new WallApi(getActivity());
			try {		
				checkLatestComment.addAll(waComment.submitComment(comment));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			Comment latestComment = checkLatestComment.get(0);
			if (isCommentSubmitted(latestComment)) {
				cb.getComments().add(latestComment);
			}
			
			cb.starting = 0;
			adapter.notifyDataSetChanged();
			commentListView.setSelection(adapter.getCount() - 1);
			postCommentButton.setEnabled(true);
			postCommentText.setEnabled(true);
			postCommentText.setText("");
			
		}

		private boolean isCommentSubmitted(Comment latestComment) {
			// TODO Auto-generated method stub			
			if (((Account)latestComment.user).ID.equals(((Account)comment.user).ID) && latestComment.message.equals(commentBeforeConvertID) ) {
				return true;
			} else {
				return false;
			}
		}
	}

}
