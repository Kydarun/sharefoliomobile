package com.kydarun.sharefolio.fragment;

import java.util.List;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.kydarun.sharefolio.R;
import com.kydarun.sharefolio.adapter.LikerAdapter;
import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.obj.Wall;

public class LikerFromWallFragment extends Fragment {
	Wall currentWall;
	long wallID;
	List<Account> likers;
	public static final String EXTRA_CURRENT_WALL = "com.kydarun.sharefolio.LikerFromWallFragment.currentWall";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		currentWall = (Wall) getArguments().getSerializable(EXTRA_CURRENT_WALL);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.like_list, container, false);
		
		getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
		
		likers = currentWall.like;
		ListView likersListView = (ListView) v.findViewById(R.id.lvLiker);
		LikerAdapter adapter = new LikerAdapter(getActivity(), likers);
		likersListView.setAdapter(adapter);
		return v;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId()) {
		case android.R.id.home:
			getActivity().onBackPressed();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public static LikerFromWallFragment newInstance(Wall wall) {
		Bundle args = new Bundle();
		args.putSerializable(EXTRA_CURRENT_WALL, wall);
		
		LikerFromWallFragment fragment = new LikerFromWallFragment();
		fragment.setArguments(args);
		
		return fragment;
	}
	

}
