package com.kydarun.sharefolio;

import java.io.IOException;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.amazonaws.services.sns.model.CreatePlatformEndpointResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.kydarun.sharefolio.api.CompanyApi;
import com.kydarun.sharefolio.api.UserApi;
import com.kydarun.sharefolio.database.DBHelper;
import com.kydarun.sharefolio.notification.NotificationReceiverService;
import com.kydarun.sharefolio.obj.Account;
import com.kydarun.sharefolio.obj.Company;
import com.kydarun.sharefolio.util.Common;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.Permission.Type;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnProfileListener;

public class LoginActivity extends BaseActivity implements OnClickListener {
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	
	Button btnLogin;
	Button btnFBLogin;
	EditText etEmail;
	EditText etPassword;
	String email;
	String password;
	
	SimpleFacebook fb;
	
	GoogleCloudMessaging gcm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		
		gcm = GoogleCloudMessaging.getInstance(this);
		
		btnLogin = (Button)findViewById(R.id.btnLogin);
		if (btnLogin != null) {
			btnLogin.setOnClickListener(this);
		}
		
		btnFBLogin = (Button)findViewById(R.id.btnFBLogin);
		if (btnFBLogin != null) {
			btnFBLogin.setBackgroundResource(R.drawable.com_facebook_button_blue);
			btnFBLogin.setTypeface(Typeface.DEFAULT_BOLD);
			btnFBLogin.setTextColor(getResources().getColor(R.color.com_facebook_loginview_text_color));
			btnFBLogin.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                    getResources().getDimension(R.dimen.com_facebook_loginview_text_size));
			btnFBLogin.setCompoundDrawablesWithIntrinsicBounds(R.drawable.com_facebook_inverse_icon, 0, 0, 0);
			btnFBLogin.setCompoundDrawablePadding(
                    getResources().getDimensionPixelSize(R.dimen.com_facebook_loginview_compound_drawable_padding));
			btnFBLogin.setPadding(getResources().getDimensionPixelSize(R.dimen.com_facebook_loginview_padding_left),
                    getResources().getDimensionPixelSize(R.dimen.com_facebook_loginview_padding_top),
                    getResources().getDimensionPixelSize(R.dimen.com_facebook_loginview_padding_right),
                    getResources().getDimensionPixelSize(R.dimen.com_facebook_loginview_padding_bottom));
			btnFBLogin.setOnClickListener(this);
		}
		
		Button btnSignUp = (Button)findViewById(R.id.btnSignUp);
		if (btnSignUp != null) {
			btnSignUp.setOnClickListener(this);
		}
		
		etEmail = (EditText)findViewById(R.id.etEmail);
		if (etEmail != null) {
			
		}
		
		etPassword = (EditText)findViewById(R.id.etPassword);
		if (etPassword != null) {
			
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		fb.onActivityResult(this, requestCode, resultCode, data);
		super.onActivityResult(requestCode, resultCode, data);
		progressDialog = ProgressDialog.show(context, context.getResources().getString(R.string.app_name), "Signing in");
	}

	@Override
	protected void onResume() {
		super.onResume();
		fb = SimpleFacebook.getInstance(this);
		
		if (sm.isSessionExists()) {
			finish();
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btnLogin:
				email = etEmail.getText().toString();
				password = etPassword.getText().toString();
				
				if (email.equals("") && password.equals("")) {
					Toast.makeText(context, "Please enter your email and password", Toast.LENGTH_SHORT).show();
				} else {
					Account account = new Account();
					account.email = email;
					account.password = password;
					new LoginAsyncTask(this, account).execute();
					progressDialog = ProgressDialog.show(context, context.getResources().getString(R.string.app_name), "Signing in");
				}
				break;
			case R.id.btnFBLogin:
				Permission[] permissions = new Permission[]{
					Permission.PUBLIC_PROFILE,
					Permission.EMAIL,
					Permission.PUBLISH_ACTION
				};
				SimpleFacebookConfiguration config = new SimpleFacebookConfiguration.Builder()
									.setAppId(this.getResources().getString(R.string.app_id))
									.setNamespace("com.kydarun.sharefolio")
									.setPermissions(permissions)
									.build();
				SimpleFacebook.setConfiguration(config);
				OnLoginListener login = new OnLoginListener() {
					@Override
					public void onThinking() {
						
					}

					@Override
					public void onException(Throwable throwable) {
						
					}

					@Override
					public void onFail(String reason) {
						progressDialog.dismiss();
					}

					@Override
					public void onLogin() {
						OnProfileListener listener = new OnProfileListener() {
							@Override
							public void onComplete(Profile response) {
								super.onComplete(response);
								
								new LoginAsyncTask(context, response).execute();
							}
						};
						fb.getProfile(listener);
					}

					@Override
					public void onNotAcceptingPermissions(Type type) {
						
					}
				};
				fb.login(login);
				break;
			case R.id.btnSignUp:
				Intent i = new Intent(context, RegisterActivity.class);
				startActivity(i);
				break;
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	class LoginAsyncTask extends AsyncTask<Void, Void, Account> {
		Account account;
		Context context;
		Profile profile;
		
		public LoginAsyncTask(Context context, Account account) {
			this.context = context;
			this.account = account;
		}
		
		public LoginAsyncTask(Context context, Profile profile) {
			this.context = context;
			this.profile = profile;
		}
		
		@Override
		protected Account doInBackground(Void... arg0) {
			UserApi api = new UserApi(context);
			try {
				if (profile != null) {
					return (Account)api.fbLogin(profile.getId(), profile.getFirstName(), profile.getLastName(), profile.getEmail(), profile.getPicture());
				}
				else {
					return (Account)api.login(account);
				}
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Account result) {
			super.onPostExecute(result);
			
			if (result != null) {
				sm.login(result);
				int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
				if (resultCode != ConnectionResult.SUCCESS) {
					if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
						GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity)context, PLAY_SERVICES_RESOLUTION_REQUEST).show();
					}
					else {
						Toast.makeText(context, "This device does not support Google Play.", Toast.LENGTH_SHORT).show();
					}
				}
				else {
					new RegisterNotificationAsyncTask().execute();
					new DatabaseAsyncTask().execute();
				}
			}
			else {
				Toast.makeText(context, "Login failed", Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	class RegisterNotificationAsyncTask extends AsyncTask<Void, Void, Void> {
		CreatePlatformEndpointResult res;
		String token = null;
		boolean alreadyRegistered = false;
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			
			if (token != null & res != null && res.getEndpointArn() != null) {
				startService(new Intent(context, NotificationReceiverService.class));
				Toast.makeText(context, "Registered successfully", Toast.LENGTH_SHORT).show();
			}
			else if (alreadyRegistered) {
				Toast.makeText(context, "Already registered.", Toast.LENGTH_SHORT).show();
			}
			else {
				Toast.makeText(context, "Failed to register", Toast.LENGTH_SHORT).show();
			}
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				if (sm.getCurrentSession().platformEndpointArn.equals("")) {
					token = gcm.register(context.getResources().getString(R.string.project_number));
					res = Common.registerPushNotification(context, gcm, token);
					alreadyRegistered = false;
				}
				else {
					alreadyRegistered = true;
					
					// Delete the endpoint & re-register it
					token = gcm.register(context.getResources().getString(R.string.project_number));
					res = Common.reRegisterEndpoint(context, gcm, sm.getCurrentSession(), token);
				}
				
				UserApi api = new UserApi(context);
				try {
					api.updateRegistrationID(sm.getCurrentSession().ID, token, res.getEndpointArn());
					Account a = sm.getCurrentSession();
					a.registration_id = token;
					a.platformEndpointArn = res.getEndpointArn();
					sm.setSession(a);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (NotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			return null;
		}	
	}
	
	class DatabaseAsyncTask extends AsyncTask<Void, Void, Void> {
		List<Account> friendList;
		List<Company> companyList;

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			UserApi ua = new UserApi(context);
			CompanyApi ca = new CompanyApi(context);
			try {
				friendList = ua.getFriendsList(sm.getCurrentSession().ID, 0, 1000);
				companyList = ca.getMyCompanyList(sm.getCurrentSession().ID);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			DBHelper db = DBHelper.get(context);
					
			for (Account friend : friendList) {
				db.addFriend(friend);
			}
			
			for (Company company : companyList) {
				db.addCompany(company);
			}
			
			Log.d("myID", "" + sm.getCurrentSession().ID);
			Log.d("FriendList:", friendList.toString());
			Log.d("CompanyList:", companyList.toString());
			Log.d("Testing", "testing");
			
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
			
			/* if (Common.isPhoneNumberVerified(sm)) {
				Intent intent = new Intent(LoginActivity.this, MainActivity.class);
				startActivity(intent);
			}
			else {
				Intent intent = new Intent(LoginActivity.this, PhoneNumberActivity.class);
				startActivity(intent);
			} */
			Intent intent = new Intent(LoginActivity.this, MainActivity.class);
			startActivity(intent);
		}
		
	}
}
